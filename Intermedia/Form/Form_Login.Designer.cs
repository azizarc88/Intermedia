﻿namespace Intermedia.Form
{
    partial class Form_Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GBUserName = new System.Windows.Forms.GroupBox();
            this.TBNim = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.GBPass = new System.Windows.Forms.GroupBox();
            this.TBPass = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.Label1 = new System.Windows.Forms.Label();
            this.LinkLabel1 = new System.Windows.Forms.LinkLabel();
            this.BLogin = new System.Windows.Forms.Button();
            this.BTutup = new System.Windows.Forms.Button();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.GBUserName.SuspendLayout();
            this.GBPass.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // GBUserName
            // 
            this.GBUserName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GBUserName.Controls.Add(this.TBNim);
            this.GBUserName.ForeColor = System.Drawing.Color.Black;
            this.GBUserName.Location = new System.Drawing.Point(16, 9);
            this.GBUserName.Name = "GBUserName";
            this.GBUserName.Size = new System.Drawing.Size(182, 46);
            this.GBUserName.TabIndex = 8;
            this.GBUserName.TabStop = false;
            this.GBUserName.Text = "NIM Anggota";
            // 
            // TBNim
            // 
            this.TBNim.AntiAlias = true;
            this.TBNim.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBNim.BackgroundStyle.Class = "TextBoxBorder";
            this.TBNim.BackgroundStyle.CornerDiameter = 3;
            this.TBNim.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBNim.ForeColor = System.Drawing.Color.Black;
            this.TBNim.HidePromptOnLeave = true;
            this.highlighter1.SetHighlightOnFocus(this.TBNim, true);
            this.TBNim.Location = new System.Drawing.Point(5, 19);
            this.TBNim.Mask = "00.00.0000";
            this.TBNim.Name = "TBNim";
            this.TBNim.Size = new System.Drawing.Size(170, 20);
            this.TBNim.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TBNim.TabIndex = 0;
            this.TBNim.Text = "";
            this.TBNim.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // GBPass
            // 
            this.GBPass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GBPass.Controls.Add(this.TBPass);
            this.GBPass.ForeColor = System.Drawing.Color.Black;
            this.GBPass.Location = new System.Drawing.Point(16, 60);
            this.GBPass.Name = "GBPass";
            this.GBPass.Size = new System.Drawing.Size(182, 46);
            this.GBPass.TabIndex = 9;
            this.GBPass.TabStop = false;
            this.GBPass.Text = "Kata Sandi";
            // 
            // TBPass
            // 
            this.TBPass.AutoSelectAll = true;
            this.TBPass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBPass.Border.Class = "TextBoxBorder";
            this.TBPass.Border.CornerDiameter = 3;
            this.TBPass.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBPass.FocusHighlightColor = System.Drawing.Color.DeepSkyBlue;
            this.TBPass.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBPass, true);
            this.TBPass.Location = new System.Drawing.Point(5, 19);
            this.TBPass.MaxLength = 16;
            this.TBPass.Name = "TBPass";
            this.TBPass.PasswordChar = '●';
            this.TBPass.Size = new System.Drawing.Size(168, 22);
            this.TBPass.TabIndex = 0;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.Label1.ForeColor = System.Drawing.Color.Black;
            this.Label1.Location = new System.Drawing.Point(392, 56);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(63, 13);
            this.Label1.TabIndex = 14;
            this.Label1.Text = "Contact Us:";
            // 
            // LinkLabel1
            // 
            this.LinkLabel1.AutoSize = true;
            this.LinkLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LinkLabel1.ForeColor = System.Drawing.Color.Black;
            this.LinkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.LinkLabel1.Location = new System.Drawing.Point(396, 75);
            this.LinkLabel1.Name = "LinkLabel1";
            this.LinkLabel1.Size = new System.Drawing.Size(55, 13);
            this.LinkLabel1.TabIndex = 13;
            this.LinkLabel1.TabStop = true;
            this.LinkLabel1.Tag = "https://www.facebook.com/groups/intermedia.amikompwt";
            this.LinkLabel1.Text = "Facebook";
            // 
            // BLogin
            // 
            this.BLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.BLogin.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BLogin.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.BLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkTurquoise;
            this.BLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue;
            this.BLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BLogin.ForeColor = System.Drawing.Color.Black;
            this.BLogin.Location = new System.Drawing.Point(121, 117);
            this.BLogin.Name = "BLogin";
            this.BLogin.Size = new System.Drawing.Size(75, 23);
            this.BLogin.TabIndex = 11;
            this.BLogin.Text = "Login";
            this.BLogin.UseVisualStyleBackColor = false;
            this.BLogin.Click += new System.EventHandler(this.BLogin_Click);
            // 
            // BTutup
            // 
            this.BTutup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.BTutup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkTurquoise;
            this.BTutup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue;
            this.BTutup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTutup.ForeColor = System.Drawing.Color.Black;
            this.BTutup.Location = new System.Drawing.Point(16, 116);
            this.BTutup.Name = "BTutup";
            this.BTutup.Size = new System.Drawing.Size(75, 23);
            this.BTutup.TabIndex = 10;
            this.BTutup.Text = "Tutup";
            this.BTutup.UseVisualStyleBackColor = false;
            this.BTutup.Click += new System.EventHandler(this.BTutup_Click);
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242))))), System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204))))));
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // PictureBox1
            // 
            this.PictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.PictureBox1.ForeColor = System.Drawing.Color.Black;
            this.PictureBox1.Image = global::Intermedia.Properties.Resources.Logo_Intermedia_mini;
            this.PictureBox1.Location = new System.Drawing.Point(215, 4);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(136, 136);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox1.TabIndex = 12;
            this.PictureBox1.TabStop = false;
            // 
            // Form_Login
            // 
            this.AcceptButton = this.BLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BTutup;
            this.ClientSize = new System.Drawing.Size(367, 155);
            this.ControlBox = false;
            this.Controls.Add(this.GBUserName);
            this.Controls.Add(this.GBPass);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.LinkLabel1);
            this.Controls.Add(this.BLogin);
            this.Controls.Add(this.BTutup);
            this.Controls.Add(this.PictureBox1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form_Login";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.GBUserName.ResumeLayout(false);
            this.GBPass.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox GBUserName;
        internal DevComponents.DotNetBar.Controls.MaskedTextBoxAdv TBNim;
        internal System.Windows.Forms.GroupBox GBPass;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBPass;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.LinkLabel LinkLabel1;
        internal System.Windows.Forms.Button BLogin;
        internal System.Windows.Forms.Button BTutup;
        internal System.Windows.Forms.PictureBox PictureBox1;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
    }
}