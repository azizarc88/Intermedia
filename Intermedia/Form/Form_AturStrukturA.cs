﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Intermedia.Class;
using DevComponents.DotNetBar.Metro;

namespace Intermedia.Form
{
    public partial class Form_AturStrukturA : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();

        public delegate void DataGridSegarkanHandler(object sender, DataGridSegarkanEventArgs e);
        public event DataGridSegarkanHandler DataGridSegarkan;

        public List<string> DataUbah = new List<string>();
        public List<string> DataInfo = new List<string>();

        public Form_AturStrukturA()
        {
            InitializeComponent();
        }

        internal void SetTampilan()
        {
            LId_Jabatan.Text = DataUbah[0];
            TBNim.Text = DataUbah[1];
            CBJabatan.Text = DataUbah[2];
            DTIJabatanAwal.Text = DataUbah[3];
            DTIJabatanAkhir.Text = DataUbah[4];

            LNama.Text = DataInfo[1];
            LKelas.Text = DataInfo[2];
            LAlamat.Text = DataInfo[3];
            LNoTelp.Text = DataInfo[5];
        }

        private bool CekKelengkapan()
        {
            if (DTIJabatanAwal.Text.Length == 0)
            {
                ToastNotification.Show(this, "Tanggal jabatan berawal harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                DTIJabatanAwal.Focus();
                return false;
            }
            else if (DTIJabatanAkhir.Text.Length == 0)
            {
                ToastNotification.Show(this, "Tanggal jabatan berakhir harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                DTIJabatanAkhir.Focus();
                return false;
            }
            return true;
        }

        private void Form_AturStrukturA_Shown(object sender, EventArgs e)
        {
            CBJabatan.Focus();
        }

        private void BSimpan_Click(object sender, EventArgs e)
        {
            MessageBoxEx.UseSystemLocalizedString = true;
            DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Struktur Anggota");

            if (CekKelengkapan() == false)
            {
                return;
            }

            if (CBJabatan.SelectedItem.ToString() != "Anggota")
            {

                string jabatan = CBJabatan.SelectedItem.ToString();
                if (MDA.DataAda("nama_jabatan", "struktur_anggota", "nama_jabatan = '" + jabatan + "'"))
                {
                    string nama, nim, id;
                    nama = MDA.AmbilNilai("nama_anggota", "v_struktur", "nama_jabatan = '" + jabatan + "'");
                    nim = MDA.AmbilNilai("nim", "v_struktur", "nama_jabatan = '" + jabatan + "'");
                    id = MDA.AmbilNilai("id_jabatan", "struktur_anggota", "nama_jabatan = '" + jabatan + "'");
                    if (MessageBoxEx.Show(this, "Jabatan " + CBJabatan.SelectedItem.ToString() + " sudah dimiliki oleh " + nama + "\n\nApakah Anda ingin mengganti jabatan " + jabatan + " ke " + LNama.Text + "?", "Intermedia", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        string n, nC, n2, nC2;
                        // Dirubah ke Anggota
                        n = "nim = '" + nim + "', nama_jabatan = 'Anggota', jabatan_mulai = '" + MDA.AturJabatanMulai() + "', jabatan_akhir = '" + MDA.AturJabatanAkhir() + "'";
                        nC = "(" + id + ", " + nim + ", Anggota, " + MDA.AturJabatanMulai() + ", " + MDA.AturJabatanAkhir() + ")";
                        MDA.UbahData("struktur_anggota", n, "nim = " + nim);
                        MDA.Catat("Ubah Data Struktur Anggota", nC);

                        // Dirubah ke Keinginan
                        n2 = "nim = '" + TBNim.Text + "', nama_jabatan = '" + CBJabatan.SelectedItem.ToString() + "', jabatan_mulai = '" + MDA.AturTanggal(DTIJabatanAwal.Value) + "', jabatan_akhir = '" + MDA.AturTanggal(DTIJabatanAkhir.Value) + "'";
                        nC2 = "(" + LId_Jabatan.Text + ", " + TBNim.Text + ", " + CBJabatan.Text + ", " + DTIJabatanAwal.Text + ", " + DTIJabatanAkhir + ")";
                        MDA.UbahData("struktur_anggota", n2, "nim = " + TBNim.Text);
                        MDA.Catat("Ubah Data Struktur Anggota", nC2);

                        DataGridSegarkan(this, args);

                        this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                        return;
                    }
                    else
                    {
                        CBJabatan.Focus();
                        return;
                    }
                }
            }

            string nilai, nilaiC;
            nilai = "nim = '" + TBNim.Text + "', nama_jabatan = '" + CBJabatan.SelectedItem.ToString() + "', jabatan_mulai = '" + MDA.AturTanggal(DTIJabatanAwal.Value) + "', jabatan_akhir = '" + MDA.AturTanggal(DTIJabatanAkhir.Value) + "'";
            nilaiC = "(" + LId_Jabatan.Text + ", " + TBNim.Text + ", " + CBJabatan.Text + ", " + DTIJabatanAwal.Text + ", " + DTIJabatanAkhir + ")";
            MDA.UbahData("struktur_anggota", nilai, "nim = " + TBNim.Text);
            MDA.Catat("Ubah Data Struktur Anggota", nilaiC);

            DataGridSegarkan(this, args);

            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }
    }
}
