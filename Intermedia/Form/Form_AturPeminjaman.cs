﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_AturPeminjaman : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();

        public string nim, nama, jabatan;

        public Form_AturPeminjaman()
        {
            InitializeComponent();
        }

        private void CekKetersediaan()
        {
            foreach (DataGridViewRow baris in DGPeminjaman.Rows)
            {
                foreach (DataGridViewCell cell in baris.Cells)
                {
                    if (cell.Value.ToString() == "Belum Kembali")
                    {
                        cell.Style.BackColor = Color.SteelBlue;
                    }
                    else if (cell.Value.ToString() == "Sudah Kembali")
                    {
                        cell.Style.BackColor = Color.LightSteelBlue;
                    }
                }
            }
        }

        private List<string> AmbilBelumKembali()
        {
            List<string> d = new List<string>();
            foreach (DataGridViewRow baris in DGPeminjaman.Rows)
            {
                if (baris.Cells[4].Value.ToString() != "")
                {
                    d.Add(baris.Cells[0].Value.ToString());
                }
            }
            return d;
        }

        private void Form_AturPeminjaman_Load(object sender, EventArgs e)
        {
            DGPeminjaman.DataSource = MDA.IsiDG("select * from v_peminjaman");
            CekKetersediaan();
        }

        private void Form_AturPeminjaman_Tampil(object sender, EventArgs e)
        {
            if (jabatan != "Administrator" && jabatan != "Ketua" && jabatan != "Sekretaris")
            {
                BPinjam.Enabled = false;
                BKembali.Enabled = false;
            }
            else
            {
                BPinjam.Enabled = true;
                BKembali.Enabled = true;
            }
        }

        private void BPinjam_Click(object sender, EventArgs e)
        {
            Form_AturPeminjaman_Pinjam FrmPinjam = new Form_AturPeminjaman_Pinjam();
            FrmPinjam.UpdatePeminjaman += new Form_AturPeminjaman_Pinjam.UpdatePeminjamanHandler(UpdatePeminjaman);
            FrmPinjam.IDBarang = MDA.ID(DGPeminjaman.Rows);
            FrmPinjam.ShowDialog();
        }

        private void BKembali_Click(object sender, EventArgs e)
        {
            Form_AturPeminjaman_Kembalikan FrmKembali = new Form_AturPeminjaman_Kembalikan();
            FrmKembali.UpdatePeminjaman += new Form_AturPeminjaman_Kembalikan.UpdatePeminjamanHandler(UpdatePeminjaman);
            FrmKembali.ID = AmbilBelumKembali();
            FrmKembali.ShowDialog();
        }

        private void UpdatePeminjaman(object sender, UpdatePeminjamanEventArgs e)
        {
            DGPeminjaman.DataSource = MDA.IsiDG("select * from v_peminjaman");
            CekKetersediaan();
        }

        private void DGPeminjaman_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int y = DGPeminjaman.CurrentCellAddress.Y;
            LNama.Text = MDA.AmbilNilai("nama_anggota", "anggota", "nim = '" + DGPeminjaman[2, y].Value.ToString() + "'") + " meminjam " + DGPeminjaman[1, y].Value.ToString() + " dan " + DGPeminjaman[5, y].Value.ToString();
        }
    }
}
