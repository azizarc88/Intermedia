﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_Atur_Pilih : MetroForm
    {
        ModKoneksi DB = new ModKoneksi();
        ManageDataAnggota MDA = new ManageDataAnggota();

        public delegate void ItemTerpilihHandler(object sender, ItemTerpilihEventArgs e);
        public event ItemTerpilihHandler ItemTerpilih;

        public List<string> ID = new List<string>();
        public bool TidakDibalik = true;
        public string Query;

        public Form_Atur_Pilih()
        {
            InitializeComponent();
        }

        private void CekKetersediaan()
        {
            foreach (DataGridViewRow baris in DGAnggota.Rows)
            {
                foreach (string nim in ID)
                {
                    if (baris.Cells[0].Value.ToString().Contains(nim) == TidakDibalik)
                    {
                        foreach (DataGridViewCell cell in baris.Cells)
                        {
                            cell.Style.BackColor = Color.SteelBlue;
                        }
                    }
                }
            }
        }

        private void Cari(string TeksCari)
        {
            foreach (DataGridViewRow baris in DGAnggota.Rows)
            {
                foreach (DataGridViewCell cell in baris.Cells)
                {
                    cell.Style.BackColor = Color.WhiteSmoke;
                }   
            }

            if (TeksCari != "")
            {
                foreach (DataGridViewRow baris in DGAnggota.Rows)
                {
                    foreach (DataGridViewCell cell in baris.Cells)
                    {
                        if (cell.Value.ToString().ToLower().Contains(TeksCari.ToString().ToLower()))
                        {
                            cell.Style.BackColor = Color.LightBlue;
                            DGAnggota.CurrentCell = cell;
                        }
                    }
                }
            }
        }

        private void Form_AturPiket_PilihNim_Load(object sender, EventArgs e)
        {
            DGAnggota.DataSource = DB.IsiDG(Query);
            CekKetersediaan();
        }

        private void Form_Atur_Pilih_Tampil(object sender, EventArgs e)
        {
            TBCari.Focus();
        }

        private void TextBoxX1_TextChanged(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Cari(TBCari.Text);
            timer1.Enabled = false;
        }

        private void BPilih_Click(object sender, EventArgs e)
        {
            List<string> Daftar = new List<string>();

            foreach (DataGridViewRow baris in DGAnggota.SelectedRows)
            {
                foreach (DataGridViewCell cell in baris.Cells)
                {
                    Daftar.Add(cell.Value.ToString());
                }
            }

            if (ID.Contains(LCNimDA.Text) == TidakDibalik)
            {
                if (TidakDibalik == false)
                {
                    ToastNotification.Show(this, "Item terpilih tidak memiliki data", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                    return;
                }
                else
                {
                    ToastNotification.Show(this, "Item terpilih sudah memiliki data", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                    return;
                }
            }

            ItemTerpilihEventArgs args = new ItemTerpilihEventArgs(Daftar);
            ItemTerpilih(this, args);

            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }

        private void DGAnggota_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int y = DGAnggota.CurrentCellAddress.Y;
            LCNimDA.Text = DGAnggota[0, y].Value.ToString();
            LContentDA.Text = DGAnggota.CurrentCell.Value.ToString();
        }
    }
}
