﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_AturAnggota : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();
        public List<string> DataUbah = new List<string>();

        public int sem { set; get; }
        public int max { set; get; }
        public List<string> Nim = new List<string>();

        public delegate void DataGridSegarkanHandler(object sender, DataGridSegarkanEventArgs e);
        public event DataGridSegarkanHandler DataGridSegarkan;

        Boolean tbrepasterfokus = false;

        public Form_AturAnggota()
        {
            InitializeComponent();
        }

        private void Bersihkan()
        {
            TBNim.Text = "";
            TBNama.Text = "";
            TBKelas.Text = "";
            TBAlamat.Text = "";
            CBGender.Text = "";
            TBNoTelp.Text = "";
            TBPass.Text = "";
        }

        private bool CekKelengkapan()
        {
            ToastNotification.Close(this);
            if (TBNim.Text == "")
            {
                ToastNotification.Show(this, "Nim harus diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBNim.Focus();
                return false;
            }
            else if (TBNama.Text == "")
            {
                ToastNotification.Show(this, "Nama harus diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBNama.Focus();
                return false;
            }
            else if (TBKelas.Text == "")
            {
                ToastNotification.Show(this, "Kelas harus diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBKelas.Focus();
                return false;
            }
            else if (TBAlamat.Text == "")
            {
                ToastNotification.Show(this, "Alamat harus diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBAlamat.Focus();
                return false;
            }
            else if (CBGender.SelectedItem.ToString() == "" || (CBGender.SelectedItem.ToString() != "L" && CBGender.SelectedItem.ToString() != "P"))
            {
                ToastNotification.Show(this, "Jenis Kelamin harus diisi \"L\" atau \"P\"", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                CBGender.Focus();
                return false;
            }
            else if (TBNoTelp.Text == "")
            {
                ToastNotification.Show(this, "No Handphone harus diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBNoTelp.Focus();
                return false;
            }
            else if (TBPass.Text == "")
            {
                ToastNotification.Show(this, "Password harus diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBPass.Focus();
                return false;
            }
            else if (TBRePass.Text == "" && tbrepasterfokus == true)
            {
                ToastNotification.Show(this, "Silakan ulangi memasukan password", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBRePass.Focus();
                return false;
            }
            if (TBRePass.BackColor == Color.RosyBrown)
            {
                ToastNotification.Show(this, "Silakan ulangi password dengan benar", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBRePass.Focus();
                return false;
            }
            return true;
        }

        internal void SetTampilan(string UbahATAUTambah, string jabatan)
        {
            if (UbahATAUTambah == "ubah" && (jabatan != "Administrator" && jabatan != "Ketua"))
            {
                BSimpan.Text = "Ubah";
                BPrev.Enabled = false;
                BNext.Enabled = false;
                TBNim.ReadOnly = true;
                TBNim.ButtonClear.Enabled = false;
                TBNim.BackColor = Color.LightSteelBlue;
                LabelX7.Text = "Password Baru";
                this.Text = "Ubah Data Anggota";

                TBNim.Text = DataUbah[0];
                TBNama.Text = DataUbah[1];
                TBKelas.Text = DataUbah[2];
                TBAlamat.Text = DataUbah[3];
                CBGender.Text = DataUbah[4];
                TBNoTelp.Text = DataUbah[5];
                TBPassLama.Text = DataUbah[6];
            }
            else if (UbahATAUTambah == "ubah")
            {
                char a = TBNama.PasswordChar;
                BSimpan.Text = "Ubah";
                BPrev.Enabled = true;
                BNext.Enabled = true;
                TBNim.ReadOnly = true;
                TBNim.ButtonClear.Enabled = false;
                TBPassLama.PasswordChar = a;
                TBNim.BackColor = Color.LightSteelBlue;
                LabelX7.Text = "Password Baru";
                this.Text = "Ubah Data Anggota";

                TBNim.Text = DataUbah[0];
                TBNama.Text = DataUbah[1];
                TBKelas.Text = DataUbah[2];
                TBAlamat.Text = DataUbah[3];
                CBGender.Text = DataUbah[4];
                TBNoTelp.Text = DataUbah[5];
                TBPassLama.Text = DataUbah[6];
            }
            else
            {
                BSimpan.Text = "Tambah";
                BNext.Enabled = false;
                BPrev.Enabled = false;
                TBNim.ReadOnly = false;
                TBNim.ButtonClear.Enabled = true;
                TBNim.BackColor = Color.FromArgb(239, 239, 242);
                TBNama.Focus();
                this.Text = "Tambah Data Anggota";
                LabelX6.Visible = false;
                TBPassLama.Visible = false;
                LabelX7.Location = new System.Drawing.Point(338, 67);
                TBPass.Location = new System.Drawing.Point(436, 68);
                TBRePass.Location = new System.Drawing.Point(436, 96);
                LabelX8.Location = new System.Drawing.Point(338, 97);
            }
        }

        private void Ubah_Anggota_Tampil(object sender, EventArgs e)
        {
            if (BSimpan.Text == "Ubah")
            {
                TBNama.Focus();
            }
            else
            {
                TBNim.Focus();
            }
        }

        private void BSimpan_Click(object sender, EventArgs e)
        {
            if (BSimpan.Text == "Ubah")
            {
                if (CekKelengkapan() == false)
                {
                    return;
                }

                string nilai, nilaiC;
                nilai = "nama_anggota = '" + TBNama.Text + "', kelas = '" + TBKelas.Text + "', alamat = '" + TBAlamat.Text + "', j_kel = '" + CBGender.SelectedItem.ToString() + "', no_hp = '" + TBNoTelp.Text + "', password = '" + TBPass.Text + "'";
                nilaiC = "(" + TBNim.Text + ", " + TBNama.Text + ", " + TBKelas.Text + ", " + TBAlamat.Text + ", " + CBGender.SelectedItem.ToString() + ", " + TBNoTelp.Text + ", " + TBPass.Text + ")";
                MDA.UbahData("anggota", nilai, "nim = " + TBNim.Text);
                MDA.Catat("Ubah Data Anggota", nilaiC);
            }
            if (BSimpan.Text == "Tambah")
            {
                if (CekKelengkapan() == false)
                {
                    return;
                }

                if (MDA.DataAda("nim", "anggota", "nim = '" + TBNim.Text + "'") == true)
                {
                    ToastNotification.Show(this, "Data dengan nim \"" + TBNim.Text + "\" sudah ada", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                    TBNim.Focus();
                    return;
                }

                string nilaiA, nilaiCA, nilaiS, nilaiCS;
                nilaiA = "'" + TBNim.Text + "', '" + TBNama.Text + "', '" + TBKelas.Text + "', '" + TBAlamat.Text + "', '" + CBGender.SelectedItem.ToString() + "', '" + TBNoTelp.Text + "', '" + TBPass.Text + "'";
                nilaiCA = "(" + TBNim.Text + ", " + TBNama.Text + ", " + TBKelas.Text + ", " + TBAlamat.Text + ", " + CBGender.SelectedItem.ToString() + ", " + TBNoTelp.Text + ", " + TBPass.Text + ")";
                nilaiS = "'" + MDA.TentID("SA", "struktur_anggota", "id_jabatan") + "','" + TBNim.Text + "', 'Anggota','" + MDA.AturJabatanMulai() + "','" + MDA.AturJabatanAkhir() + "'";
                nilaiCS = "(" + MDA.TentID("SA", "struktur_anggota", "id_jabatan") + ", " + TBNim.Text + ", Anggota, " + MDA.AturJabatanMulai() + ", " + MDA.AturJabatanAkhir() + ")";
                MDA.TambahData("anggota", nilaiA);
                MDA.TambahData("struktur_anggota", nilaiS);
                MDA.Catat("Tambah Data Anggota", nilaiCA);
                MDA.Catat("Tambah Data Struktur", nilaiCS);
            }

            DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Anggota");
            DataGridSegarkan(this, args);

            if (CBOtoClose.Checked == true)
            {
                this.Close();
                this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                return;
            }

            ToastNotification.Show(this, TBNama.Text + " telah ditambahkan ke keanggotaan Intermedia", eToastPosition.BottomCenter);
            Bersihkan();
            TBNim.Focus();
        }

        private void BPrev_Click(object sender, EventArgs e)
        {
            sem--;
            if (sem < 0)
            {
                sem = max;
            }
            TBNama.Focus();
            DataUbah = MDA.BacaDataAnggota(Nim[sem]);
            TBNim.Text = DataUbah[0];
            TBNama.Text = DataUbah[1];
            TBKelas.Text = DataUbah[2];
            TBAlamat.Text = DataUbah[3];
            CBGender.Text = DataUbah[4];
            TBNoTelp.Text = DataUbah[5];
            TBPassLama.Text = DataUbah[6];
        }

        private void BNext_Click(object sender, EventArgs e)
        {
            sem++;
            if (sem > max)
            {
                sem = 0;
            }
            TBNama.Focus();
            DataUbah = MDA.BacaDataAnggota(Nim[sem]);
            TBNim.Text = DataUbah[0];
            TBNama.Text = DataUbah[1];
            TBKelas.Text = DataUbah[2];
            TBAlamat.Text = DataUbah[3];
            CBGender.Text = DataUbah[4];
            TBNoTelp.Text = DataUbah[5];
            TBPassLama.Text = DataUbah[6];
        }

        private void TBRePass_TerFokus(object sender, EventArgs e)
        {
            tbrepasterfokus = true;
        }

        private void TBRePass_IsiBerubah(object sender, EventArgs e)
        {
            if (TBRePass.Text != TBPass.Text)
            {
                TBRePass.BackColor = Color.RosyBrown;
            }
            else
            {
                TBRePass.BackColor = Color.FromArgb(239, 239, 242);
            }
        }
    }
}
