﻿namespace Intermedia.Form
{
    partial class Form_AbsenRapat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.CBTampilSemua = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.CBWaktu = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.CBLogika = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.DGAbsenRapat = new System.Windows.Forms.DataGridView();
            this.nama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tanggal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.waktu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tanggal_absen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.waktu_absen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.CBTanggal = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.LWaktu = new DevComponents.DotNetBar.Controls.ReflectionLabel();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.GroupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            ((System.ComponentModel.ISupportInitialize)(this.DGAbsenRapat)).BeginInit();
            this.groupPanel2.SuspendLayout();
            this.GroupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // CBTampilSemua
            // 
            this.CBTampilSemua.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.CBTampilSemua.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.CBTampilSemua.EnableMarkup = false;
            this.CBTampilSemua.ForeColor = System.Drawing.Color.Black;
            this.CBTampilSemua.Location = new System.Drawing.Point(424, 3);
            this.CBTampilSemua.Name = "CBTampilSemua";
            this.CBTampilSemua.Size = new System.Drawing.Size(18, 23);
            this.CBTampilSemua.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBTampilSemua.TabIndex = 7;
            this.CBTampilSemua.CheckedChanged += new System.EventHandler(this.CBTampilSemua_CheckedChanged);
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(298, 7);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 6;
            this.labelX4.Text = "Waktu:";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(153, 7);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(95, 23);
            this.labelX3.TabIndex = 5;
            this.labelX3.Text = "Operator Logika:";
            // 
            // CBWaktu
            // 
            this.CBWaktu.DisplayMember = "Text";
            this.CBWaktu.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CBWaktu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBWaktu.ForeColor = System.Drawing.Color.Black;
            this.CBWaktu.FormattingEnabled = true;
            this.CBWaktu.ItemHeight = 14;
            this.CBWaktu.Items.AddRange(new object[] {
            this.comboItem3,
            this.comboItem4,
            this.comboItem5,
            this.comboItem6});
            this.CBWaktu.Location = new System.Drawing.Point(298, 35);
            this.CBWaktu.Name = "CBWaktu";
            this.CBWaktu.Size = new System.Drawing.Size(139, 20);
            this.CBWaktu.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBWaktu.TabIndex = 3;
            this.CBWaktu.SelectedIndexChanged += new System.EventHandler(this.CBWaktu_SelectedIndexChanged);
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "09:50:00";
            this.comboItem3.Value = "09:50:00";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "10:40:00";
            this.comboItem4.Value = "10:40:00";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "12:30:00";
            this.comboItem5.Value = "12:30:00";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "14:20:00";
            this.comboItem6.Value = "14:20:00";
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // CBLogika
            // 
            this.CBLogika.DisplayMember = "Text";
            this.CBLogika.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CBLogika.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBLogika.ForeColor = System.Drawing.Color.Black;
            this.CBLogika.FormattingEnabled = true;
            this.CBLogika.ItemHeight = 14;
            this.CBLogika.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2});
            this.CBLogika.Location = new System.Drawing.Point(153, 36);
            this.CBLogika.Name = "CBLogika";
            this.CBLogika.Size = new System.Drawing.Size(139, 20);
            this.CBLogika.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBLogika.TabIndex = 2;
            this.CBLogika.SelectedIndexChanged += new System.EventHandler(this.CBWaktu_SelectedIndexChanged);
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "DAN";
            this.comboItem1.Value = "AND";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "ATAU";
            this.comboItem2.Value = "OR";
            // 
            // DGAbsenRapat
            // 
            this.DGAbsenRapat.AllowUserToAddRows = false;
            this.DGAbsenRapat.AllowUserToDeleteRows = false;
            this.DGAbsenRapat.AllowUserToResizeColumns = false;
            this.DGAbsenRapat.AllowUserToResizeRows = false;
            this.DGAbsenRapat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGAbsenRapat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGAbsenRapat.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGAbsenRapat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGAbsenRapat.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGAbsenRapat.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGAbsenRapat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGAbsenRapat.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nama,
            this.tanggal,
            this.waktu,
            this.tanggal_absen,
            this.waktu_absen});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGAbsenRapat.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGAbsenRapat.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DGAbsenRapat.EnableHeadersVisualStyles = false;
            this.DGAbsenRapat.GridColor = System.Drawing.Color.LightGray;
            this.DGAbsenRapat.Location = new System.Drawing.Point(12, 105);
            this.DGAbsenRapat.Name = "DGAbsenRapat";
            this.DGAbsenRapat.ReadOnly = true;
            this.DGAbsenRapat.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGAbsenRapat.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGAbsenRapat.RowHeadersVisible = false;
            this.DGAbsenRapat.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DGAbsenRapat.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGAbsenRapat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGAbsenRapat.Size = new System.Drawing.Size(791, 350);
            this.DGAbsenRapat.TabIndex = 61;
            // 
            // nama
            // 
            this.nama.DataPropertyName = "nama_anggota";
            this.nama.HeaderText = "Nama";
            this.nama.Name = "nama";
            this.nama.ReadOnly = true;
            // 
            // tanggal
            // 
            this.tanggal.DataPropertyName = "tanggal";
            this.tanggal.FillWeight = 50F;
            this.tanggal.HeaderText = "Tanggal";
            this.tanggal.Name = "tanggal";
            this.tanggal.ReadOnly = true;
            // 
            // waktu
            // 
            this.waktu.DataPropertyName = "waktu";
            this.waktu.FillWeight = 50F;
            this.waktu.HeaderText = "Waktu";
            this.waktu.Name = "waktu";
            this.waktu.ReadOnly = true;
            // 
            // tanggal_absen
            // 
            this.tanggal_absen.DataPropertyName = "absen_tanggal";
            this.tanggal_absen.FillWeight = 80F;
            this.tanggal_absen.HeaderText = "Absen Tanggal";
            this.tanggal_absen.Name = "tanggal_absen";
            this.tanggal_absen.ReadOnly = true;
            // 
            // waktu_absen
            // 
            this.waktu_absen.DataPropertyName = "waktu_absen";
            this.waktu_absen.FillWeight = 50F;
            this.waktu_absen.HeaderText = "Absen Pada";
            this.waktu_absen.Name = "waktu_absen";
            this.waktu_absen.ReadOnly = true;
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(8, 7);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 1;
            this.labelX1.Text = "Tanggal:";
            // 
            // CBTanggal
            // 
            this.CBTanggal.DisplayMember = "Text";
            this.CBTanggal.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CBTanggal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBTanggal.ForeColor = System.Drawing.Color.Black;
            this.CBTanggal.FormattingEnabled = true;
            this.CBTanggal.ItemHeight = 14;
            this.CBTanggal.Location = new System.Drawing.Point(8, 36);
            this.CBTanggal.Name = "CBTanggal";
            this.CBTanggal.Size = new System.Drawing.Size(139, 20);
            this.CBTanggal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBTanggal.TabIndex = 0;
            this.CBTanggal.SelectedIndexChanged += new System.EventHandler(this.CBWaktu_SelectedIndexChanged);
            // 
            // LWaktu
            // 
            this.LWaktu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LWaktu.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LWaktu.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.LWaktu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LWaktu.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.LWaktu.ForeColor = System.Drawing.Color.Black;
            this.LWaktu.Location = new System.Drawing.Point(0, 0);
            this.LWaktu.Name = "LWaktu";
            this.LWaktu.Size = new System.Drawing.Size(326, 66);
            this.LWaktu.TabIndex = 0;
            this.LWaktu.Text = "07:46 Enjing";
            // 
            // groupPanel2
            // 
            this.groupPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.groupPanel2.Controls.Add(this.LWaktu);
            this.groupPanel2.Location = new System.Drawing.Point(471, 12);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(332, 87);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 60;
            this.groupPanel2.Text = "Waktu Hari Ini";
            this.groupPanel2.TitleImagePosition = DevComponents.DotNetBar.eTitleImagePosition.Center;
            // 
            // GroupPanel1
            // 
            this.GroupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GroupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.GroupPanel1.Controls.Add(this.CBTampilSemua);
            this.GroupPanel1.Controls.Add(this.labelX4);
            this.GroupPanel1.Controls.Add(this.labelX3);
            this.GroupPanel1.Controls.Add(this.CBWaktu);
            this.GroupPanel1.Controls.Add(this.CBLogika);
            this.GroupPanel1.Controls.Add(this.labelX1);
            this.GroupPanel1.Controls.Add(this.CBTanggal);
            this.GroupPanel1.Location = new System.Drawing.Point(12, 12);
            this.GroupPanel1.Name = "GroupPanel1";
            this.GroupPanel1.Size = new System.Drawing.Size(453, 87);
            // 
            // 
            // 
            this.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.GroupPanel1.Style.BackColorGradientAngle = 90;
            this.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground;
            this.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel1.Style.BorderBottomWidth = 1;
            this.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel1.Style.BorderLeftWidth = 1;
            this.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel1.Style.BorderRightWidth = 1;
            this.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel1.Style.BorderTopWidth = 1;
            this.GroupPanel1.Style.CornerDiameter = 4;
            this.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GroupPanel1.TabIndex = 59;
            this.GroupPanel1.Text = "Kondisi Seleksi";
            this.GroupPanel1.TitleImagePosition = DevComponents.DotNetBar.eTitleImagePosition.Center;
            // 
            // Form_AbsenRapat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 467);
            this.Controls.Add(this.DGAbsenRapat);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.GroupPanel1);
            this.DoubleBuffered = true;
            this.Name = "Form_AbsenRapat";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Presensi Rapat";
            this.Load += new System.EventHandler(this.Form_AbsenRapat_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGAbsenRapat)).EndInit();
            this.groupPanel2.ResumeLayout(false);
            this.GroupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer2;
        private DevComponents.DotNetBar.Controls.CheckBoxX CBTampilSemua;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CBWaktu;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private System.Windows.Forms.Timer timer1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CBLogika;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        internal System.Windows.Forms.DataGridView DGAbsenRapat;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CBTanggal;
        private DevComponents.DotNetBar.Controls.ReflectionLabel LWaktu;
        internal DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        internal DevComponents.DotNetBar.Controls.GroupPanel GroupPanel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nama;
        private System.Windows.Forms.DataGridViewTextBoxColumn tanggal;
        private System.Windows.Forms.DataGridViewTextBoxColumn waktu;
        private System.Windows.Forms.DataGridViewTextBoxColumn tanggal_absen;
        private System.Windows.Forms.DataGridViewTextBoxColumn waktu_absen;
    }
}