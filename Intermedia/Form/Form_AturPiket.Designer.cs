﻿namespace Intermedia.Form
{
    partial class Form_AturPiket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TBNim = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.LAlamat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.LNoTelp = new DevComponents.DotNetBar.LabelX();
            this.LKelas = new DevComponents.DotNetBar.LabelX();
            this.LNama = new DevComponents.DotNetBar.LabelX();
            this.LabelX7 = new DevComponents.DotNetBar.LabelX();
            this.LabelX6 = new DevComponents.DotNetBar.LabelX();
            this.LabelX5 = new DevComponents.DotNetBar.LabelX();
            this.LabelX4 = new DevComponents.DotNetBar.LabelX();
            this.GroupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.LNoID = new DevComponents.DotNetBar.LabelX();
            this.BPilih = new DevComponents.DotNetBar.ButtonX();
            this.BTutup = new DevComponents.DotNetBar.ButtonX();
            this.LabelX1 = new DevComponents.DotNetBar.LabelX();
            this.LNim = new DevComponents.DotNetBar.LabelX();
            this.CBOtoClose = new DevComponents.DotNetBar.CheckBoxItem();
            this.LId = new DevComponents.DotNetBar.LabelX();
            this.BSimpan = new DevComponents.DotNetBar.ButtonX();
            this.CBHari = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.CBJam = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.comboItem9 = new DevComponents.Editors.ComboItem();
            this.comboItem10 = new DevComponents.Editors.ComboItem();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.styleManagerAmbient1 = new DevComponents.DotNetBar.StyleManagerAmbient(this.components);
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.GroupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TBNim
            // 
            this.TBNim.AllowPromptAsInput = false;
            this.TBNim.AntiAlias = true;
            this.TBNim.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBNim.BackgroundStyle.Class = "TextBoxBorder";
            this.TBNim.BackgroundStyle.CornerDiameter = 2;
            this.TBNim.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBNim.ButtonClear.Visible = true;
            this.styleManagerAmbient1.SetEnableAmbientSettings(this.TBNim, DevComponents.DotNetBar.eAmbientSettings.None);
            this.TBNim.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBNim, true);
            this.TBNim.Location = new System.Drawing.Point(404, 44);
            this.TBNim.Mask = "00.00.0000";
            this.TBNim.Name = "TBNim";
            this.TBNim.Size = new System.Drawing.Size(132, 22);
            this.TBNim.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TBNim.TabIndex = 46;
            this.TBNim.Text = "";
            this.TBNim.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // LAlamat
            // 
            this.LAlamat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LAlamat.Border.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.LAlamat.Border.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LAlamat.Border.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.LAlamat.Border.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.LAlamat.Border.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.LAlamat.Border.Class = "TextBoxBorder";
            this.LAlamat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LAlamat.ForeColor = System.Drawing.Color.Black;
            this.LAlamat.Location = new System.Drawing.Point(100, 74);
            this.LAlamat.Multiline = true;
            this.LAlamat.Name = "LAlamat";
            this.LAlamat.ReadOnly = true;
            this.LAlamat.Size = new System.Drawing.Size(146, 46);
            this.LAlamat.TabIndex = 94;
            this.LAlamat.Text = "Belum ditentukan";
            // 
            // LNoTelp
            // 
            this.LNoTelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNoTelp.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNoTelp.ForeColor = System.Drawing.Color.Black;
            this.LNoTelp.Location = new System.Drawing.Point(102, 140);
            this.LNoTelp.Name = "LNoTelp";
            this.LNoTelp.Size = new System.Drawing.Size(150, 23);
            this.LNoTelp.TabIndex = 92;
            this.LNoTelp.Text = "Belum ditentukan";
            // 
            // LKelas
            // 
            this.LKelas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LKelas.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LKelas.ForeColor = System.Drawing.Color.Black;
            this.LKelas.Location = new System.Drawing.Point(102, 38);
            this.LKelas.Name = "LKelas";
            this.LKelas.Size = new System.Drawing.Size(150, 23);
            this.LKelas.TabIndex = 96;
            this.LKelas.Text = "Belum ditentukan";
            // 
            // LNama
            // 
            this.LNama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNama.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNama.ForeColor = System.Drawing.Color.Black;
            this.LNama.Location = new System.Drawing.Point(102, 6);
            this.LNama.Name = "LNama";
            this.LNama.Size = new System.Drawing.Size(150, 23);
            this.LNama.TabIndex = 98;
            this.LNama.Text = "Belum ditentukan";
            // 
            // LabelX7
            // 
            this.LabelX7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX7.ForeColor = System.Drawing.Color.Black;
            this.LabelX7.Location = new System.Drawing.Point(10, 140);
            this.LabelX7.Name = "LabelX7";
            this.LabelX7.Size = new System.Drawing.Size(100, 23);
            this.LabelX7.TabIndex = 93;
            this.LabelX7.Text = "No. Handphone";
            // 
            // LabelX6
            // 
            this.LabelX6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX6.ForeColor = System.Drawing.Color.Black;
            this.LabelX6.Location = new System.Drawing.Point(10, 71);
            this.LabelX6.Name = "LabelX6";
            this.LabelX6.Size = new System.Drawing.Size(100, 23);
            this.LabelX6.TabIndex = 95;
            this.LabelX6.Text = "Alamat";
            // 
            // LabelX5
            // 
            this.LabelX5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX5.ForeColor = System.Drawing.Color.Black;
            this.LabelX5.Location = new System.Drawing.Point(10, 38);
            this.LabelX5.Name = "LabelX5";
            this.LabelX5.Size = new System.Drawing.Size(100, 23);
            this.LabelX5.TabIndex = 97;
            this.LabelX5.Text = "Kelas";
            // 
            // LabelX4
            // 
            this.LabelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX4.ForeColor = System.Drawing.Color.Black;
            this.LabelX4.Location = new System.Drawing.Point(10, 6);
            this.LabelX4.Name = "LabelX4";
            this.LabelX4.Size = new System.Drawing.Size(100, 23);
            this.LabelX4.TabIndex = 99;
            this.LabelX4.Text = "Nama";
            // 
            // GroupPanel1
            // 
            this.GroupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GroupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.GroupPanel1.Controls.Add(this.LAlamat);
            this.GroupPanel1.Controls.Add(this.LNoTelp);
            this.GroupPanel1.Controls.Add(this.LKelas);
            this.GroupPanel1.Controls.Add(this.LNama);
            this.GroupPanel1.Controls.Add(this.LabelX7);
            this.GroupPanel1.Controls.Add(this.LabelX6);
            this.GroupPanel1.Controls.Add(this.LabelX5);
            this.GroupPanel1.Controls.Add(this.LabelX4);
            this.GroupPanel1.Location = new System.Drawing.Point(10, 6);
            this.GroupPanel1.Name = "GroupPanel1";
            this.GroupPanel1.Size = new System.Drawing.Size(268, 196);
            // 
            // 
            // 
            this.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.GroupPanel1.Style.BackColorGradientAngle = 90;
            this.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground;
            this.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel1.Style.BorderBottomWidth = 1;
            this.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel1.Style.BorderLeftWidth = 1;
            this.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel1.Style.BorderRightWidth = 1;
            this.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel1.Style.BorderTopWidth = 1;
            this.GroupPanel1.Style.CornerDiameter = 4;
            this.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GroupPanel1.TabIndex = 55;
            this.GroupPanel1.Text = "Info Anggota";
            this.GroupPanel1.TitleImagePosition = DevComponents.DotNetBar.eTitleImagePosition.Center;
            // 
            // LNoID
            // 
            this.LNoID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNoID.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNoID.ForeColor = System.Drawing.Color.Black;
            this.LNoID.Location = new System.Drawing.Point(304, 12);
            this.LNoID.Name = "LNoID";
            this.LNoID.Size = new System.Drawing.Size(100, 23);
            this.LNoID.TabIndex = 88;
            this.LNoID.Text = "ID";
            // 
            // BPilih
            // 
            this.BPilih.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPilih.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.highlighter1.SetHighlightOnFocus(this.BPilih, true);
            this.BPilih.Location = new System.Drawing.Point(542, 44);
            this.BPilih.Name = "BPilih";
            this.BPilih.Size = new System.Drawing.Size(68, 22);
            this.BPilih.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPilih.TabIndex = 47;
            this.BPilih.Text = "Pilih";
            this.BPilih.Click += new System.EventHandler(this.BPilih_Click);
            // 
            // BTutup
            // 
            this.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BTutup.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.Location = new System.Drawing.Point(304, 150);
            this.BTutup.Name = "BTutup";
            this.BTutup.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BTutup.Size = new System.Drawing.Size(142, 34);
            this.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BTutup.TabIndex = 50;
            this.BTutup.Text = "Batal";
            // 
            // LabelX1
            // 
            this.LabelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX1.ForeColor = System.Drawing.Color.Black;
            this.LabelX1.Location = new System.Drawing.Point(304, 71);
            this.LabelX1.Name = "LabelX1";
            this.LabelX1.Size = new System.Drawing.Size(100, 23);
            this.LabelX1.TabIndex = 86;
            this.LabelX1.Text = "Hari";
            // 
            // LNim
            // 
            this.LNim.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNim.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNim.ForeColor = System.Drawing.Color.Black;
            this.LNim.Location = new System.Drawing.Point(304, 42);
            this.LNim.Name = "LNim";
            this.LNim.Size = new System.Drawing.Size(100, 23);
            this.LNim.TabIndex = 87;
            this.LNim.Text = "Nim";
            // 
            // CBOtoClose
            // 
            this.CBOtoClose.Checked = true;
            this.CBOtoClose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBOtoClose.GlobalItem = false;
            this.CBOtoClose.Name = "CBOtoClose";
            this.CBOtoClose.Stretch = true;
            this.CBOtoClose.Text = "Tutup otomatis";
            // 
            // LId
            // 
            this.LId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LId.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LId.ForeColor = System.Drawing.Color.Black;
            this.LId.Location = new System.Drawing.Point(404, 12);
            this.LId.Name = "LId";
            this.LId.Size = new System.Drawing.Size(100, 23);
            this.LId.TabIndex = 84;
            this.LId.Text = "PK001";
            // 
            // BSimpan
            // 
            this.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BSimpan.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BSimpan.Location = new System.Drawing.Point(468, 150);
            this.BSimpan.Name = "BSimpan";
            this.BSimpan.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BSimpan.Size = new System.Drawing.Size(142, 34);
            this.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BSimpan.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.CBOtoClose});
            this.BSimpan.SubItemsExpandWidth = 18;
            this.BSimpan.TabIndex = 51;
            this.BSimpan.Text = "Tambah";
            this.BSimpan.Click += new System.EventHandler(this.BSimpan_Click);
            // 
            // CBHari
            // 
            this.CBHari.DisplayMember = "Text";
            this.CBHari.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CBHari.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBHari.ForeColor = System.Drawing.Color.Black;
            this.CBHari.FormattingEnabled = true;
            this.highlighter1.SetHighlightOnFocus(this.CBHari, true);
            this.CBHari.ItemHeight = 14;
            this.CBHari.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem3,
            this.comboItem4,
            this.comboItem5,
            this.comboItem6});
            this.CBHari.Location = new System.Drawing.Point(404, 74);
            this.CBHari.Name = "CBHari";
            this.CBHari.Size = new System.Drawing.Size(206, 20);
            this.CBHari.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBHari.TabIndex = 48;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "Senin";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Selasa";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "Rabu";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "Kamis";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "Jumat";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "Sabtu";
            // 
            // CBJam
            // 
            this.CBJam.DisplayMember = "Text";
            this.CBJam.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CBJam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBJam.ForeColor = System.Drawing.Color.Black;
            this.CBJam.FormattingEnabled = true;
            this.highlighter1.SetHighlightOnFocus(this.CBJam, true);
            this.CBJam.ItemHeight = 14;
            this.CBJam.Items.AddRange(new object[] {
            this.comboItem7,
            this.comboItem8,
            this.comboItem9,
            this.comboItem10});
            this.CBJam.Location = new System.Drawing.Point(404, 102);
            this.CBJam.Name = "CBJam";
            this.CBJam.Size = new System.Drawing.Size(206, 20);
            this.CBJam.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBJam.TabIndex = 49;
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "09:50:00";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "10:40:00";
            // 
            // comboItem9
            // 
            this.comboItem9.Text = "12:30:00";
            // 
            // comboItem10
            // 
            this.comboItem10.Text = "14:20:00";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(304, 99);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(100, 23);
            this.labelX2.TabIndex = 85;
            this.labelX2.Text = "Jam";
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // Form_AturPiket
            // 
            this.AcceptButton = this.BSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BTutup;
            this.ClientSize = new System.Drawing.Size(617, 210);
            this.ControlBox = false;
            this.Controls.Add(this.CBJam);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.CBHari);
            this.Controls.Add(this.GroupPanel1);
            this.Controls.Add(this.LNoID);
            this.Controls.Add(this.BPilih);
            this.Controls.Add(this.BTutup);
            this.Controls.Add(this.LabelX1);
            this.Controls.Add(this.LNim);
            this.Controls.Add(this.LId);
            this.Controls.Add(this.TBNim);
            this.Controls.Add(this.BSimpan);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form_AturPiket";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tambah Data Piket";
            this.Shown += new System.EventHandler(this.Form_AturPiket_Tampil);
            this.GroupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.Controls.MaskedTextBoxAdv TBNim;
        internal DevComponents.DotNetBar.Controls.TextBoxX LAlamat;
        internal DevComponents.DotNetBar.LabelX LNoTelp;
        internal DevComponents.DotNetBar.LabelX LKelas;
        internal DevComponents.DotNetBar.LabelX LNama;
        internal DevComponents.DotNetBar.LabelX LabelX7;
        internal DevComponents.DotNetBar.LabelX LabelX6;
        internal DevComponents.DotNetBar.LabelX LabelX5;
        internal DevComponents.DotNetBar.LabelX LabelX4;
        internal DevComponents.DotNetBar.Controls.GroupPanel GroupPanel1;
        internal DevComponents.DotNetBar.LabelX LNoID;
        internal DevComponents.DotNetBar.ButtonX BPilih;
        internal DevComponents.DotNetBar.ButtonX BTutup;
        internal DevComponents.DotNetBar.LabelX LabelX1;
        internal DevComponents.DotNetBar.LabelX LNim;
        internal DevComponents.DotNetBar.CheckBoxItem CBOtoClose;
        internal DevComponents.DotNetBar.LabelX LId;
        internal DevComponents.DotNetBar.ButtonX BSimpan;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CBHari;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CBJam;
        internal DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.StyleManagerAmbient styleManagerAmbient1;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.Editors.ComboItem comboItem9;
        private DevComponents.Editors.ComboItem comboItem10;
    }
}