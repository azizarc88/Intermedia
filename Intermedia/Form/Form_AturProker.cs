﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_AturProker : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();

        public delegate void DataGridSegarkanHandler(object sender, DataGridSegarkanEventArgs e);
        public event DataGridSegarkanHandler DataGridSegarkan;

        public List<string> DataUbah = new List<string>();

        public Form_AturProker()
        {
            InitializeComponent();
        }

        internal void SetTampilan(string UbahATAUTambah)
        {
            if (UbahATAUTambah == "ubah")
            {
                BSimpan.Text = "Ubah";
                this.Text = "Ubah Data Program Kerja";

                LId.Text = DataUbah[0];
                TBNama.Text = DataUbah[1];
                DTITanggal.Text = DataUbah[2];
                TBDana.Text = DataUbah[3];
                TBKeterangan.Text = DataUbah[4];
            }
            else
            {
                BSimpan.Text = "Tambah";
                LId.Text = MDA.TentID("PR", "program_kerja", "id_proker");
                this.Text = "Tambah Data Program Kerja";
            }
        }

        private bool CekKelengkapan()
        {
            if (TBNama.Text == "")
            {
                ToastNotification.Show(this, "Nama program kerja harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBNama.Focus();
                return false;
            }
            else if (DTITanggal.Text.Length == 0)
            {
                ToastNotification.Show(this, "Tanggal harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                DTITanggal.Focus();
                return false;
            }
            else if (TBDana.Text == "")
            {
                ToastNotification.Show(this, "Total dana harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBDana.Focus();
                return false;
            }
            return true;
        }

        private void Form_AturProker_Tampil(object sender, EventArgs e)
        {
            TBNama.Focus();
        }

         private void BSimpan_Click(object sender, EventArgs e)
        {
            if (BSimpan.Text == "Ubah")
            {
                if (CekKelengkapan() == false)
                {
                    return;
                }

                string nilai, nilaiC;
                nilai = "nama = '" + TBNama.Text + "', tanggal = '" + MDA.AturTanggal(DTITanggal.Value) + "', dana = '" + TBDana.Value + "', keterangan = '" + TBKeterangan.Text + "'";
                nilaiC = "(" + LId.Text + ", " + TBNama.Text + ", " + MDA.AturTanggal(DTITanggal.Value) + ", " + TBDana.Value + ", " + TBKeterangan.Text + ")";
                MDA.UbahData("program_kerja", nilai, "id_proker = '" + LId.Text + "'");
                MDA.Catat("Ubah Data Program Kerja", nilaiC);
            }
            else if (BSimpan.Text == "Tambah")
            {
                if (CekKelengkapan() == false)
                {
                    return;
                }

                string nilai, nilaiC;
                nilai = "'" + LId.Text + "', '" + TBNama.Text + "', '" + MDA.AturTanggal(DTITanggal.Value) + "', '" + TBDana.Value + "', '" + TBKeterangan.Text + "'";
                nilaiC = "(" + LId.Text + ", " + TBNama.Text + ", " + MDA.AturTanggal(DTITanggal.Value) + ", " + TBDana.Value + ", " + TBKeterangan.Text + ")";
                MDA.TambahData("program_kerja", nilai);
                MDA.Catat("Tambah Data Program Kerja", nilaiC);
            }

            DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Proker");
            DataGridSegarkan(this, args);

            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }
    }
}
