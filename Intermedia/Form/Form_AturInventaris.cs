﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_AturInventaris : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();

        public delegate void DataGridSegarkanHandler(object sender, DataGridSegarkanEventArgs e);
        public event DataGridSegarkanHandler DataGridSegarkan;

        public List<string> DataUbah = new List<string>();

        public Form_AturInventaris()
        {
            InitializeComponent();
        }

        internal void SetTampilan(string UbahATAUTambah)
        {
            if (UbahATAUTambah == "ubah")
            {
                BSimpan.Text = "Ubah";
                BPilih.Enabled = false;
                TBNim.ReadOnly = true;
                TBNim.ButtonClear.Enabled = false;
                TBNim.BackColor = Color.LightSteelBlue;
                this.Text = "Ubah Data Inventaris";

                LId.Text = DataUbah[0];
                TBNim.Text = DataUbah[1];
                TBNama.Text = DataUbah[2];
            }
            else
            {
                BSimpan.Text = "Tambah";
                BPilih.Enabled = true;
                TBNim.ReadOnly = false;
                TBNim.ButtonClear.Enabled = true;
                TBNim.BackColor = Color.FromArgb(239, 239, 242);
                LId.Text = MDA.TentID("BR", "inventaris", "id_barang");
                this.Text = "Tambah Inventaris";
            }
        }

        private bool CekKelengkapan()
        {
            if (TBNim.Text == "")
            {
                ToastNotification.Show(this, "Nim dari anggota harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBNim.Focus();
                return false;
            }
            else if (TBNama.Text == "")
            {
                ToastNotification.Show(this, "Nama barang harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBNama.Focus();
                return false;
            }
            return true;
        }

        private void Form_AturPiket_Tampil(object sender, EventArgs e)
        {
            if (BSimpan.Text == "Ubah")
            {
                TBNama.Focus();
            }
            else
            {
                TBNim.Focus();
            }
        }

        private void BSimpan_Click(object sender, EventArgs e)
        {
            if (BSimpan.Text == "Ubah")
            {
                if (CekKelengkapan() == false)
                {
                    return;
                }

                string nilai, nilaiC;
                nilai = "nim = '" + TBNim.Text + "', nama_barang = '" + TBNama.Text + "'";
                nilaiC = "(" + LId.Text + ", " + TBNim.Text + ", " + TBNama.Text + ")";
                MDA.UbahData("inventaris", nilai, "id_barang = '" + LId.Text + "'");
                MDA.Catat("Ubah Data Inventaris", nilaiC);
            }
            else if (BSimpan.Text == "Tambah")
            {
                if (CekKelengkapan() == false)
                {
                    return;
                }

                string nilai, nilaiC;
                nilai = "'" + LId.Text + "', '" + TBNim.Text + "', '" + TBNama.Text + "'";
                nilaiC = "(" + LId.Text + ", " + TBNim.Text + ", " + TBNama.Text + ")";
                MDA.TambahData("inventaris", nilai);
                MDA.Catat("Tambah Data Inventaris", nilaiC);
            }

            DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Inventaris");
            DataGridSegarkan(this, args);

            LId.Text = MDA.TentID("BR", "inventaris", "id_barang");

            if (CBOtoClose.Checked == true)
            {
                this.Close();
                this.DialogResult = System.Windows.Forms.DialogResult.Yes;
            }

            ToastNotification.Show(this, TBNama.Text + " telah ditambahkan ke database", eToastPosition.BottomCenter);
        }

        private void BPilih_Click(object sender, EventArgs e)
        {
            Form_Atur_Pilih FrmPilihItem = new Form_Atur_Pilih();
            FrmPilihItem.ItemTerpilih += new Form_Atur_Pilih.ItemTerpilihHandler(NimInvent_Terpilih);
            FrmPilihItem.Query = "select nim, nama_anggota, kelas, alamat, j_kel, no_hp from anggota where nim > '1000' order by nama_anggota asc";
            FrmPilihItem.ShowDialog();
        }

        private void NimInvent_Terpilih(object sender, ItemTerpilihEventArgs e)
        {
            TBNama.Focus();

            TBNim.Text = e.Daftar[0];
        }

        
    }
}
