﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_AturPeminjaman_Kembalikan : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();

        public delegate void UpdatePeminjamanHandler(object sender, UpdatePeminjamanEventArgs e);
        public event UpdatePeminjamanHandler UpdatePeminjaman;

        public List<string> ID = new List<string>();

        public Form_AturPeminjaman_Kembalikan()
        {
            InitializeComponent();
        }

        private bool CekKelengkapan()
        {
            if (TBNim.Text == "")
            {
                ToastNotification.Show(this, "Nim anggota harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBNim.Focus();
                return false;
            }
            return true;
        }

        private void BPilihNim_Click(object sender, EventArgs e)
        {
            Form_Atur_Pilih FrmPilihItem = new Form_Atur_Pilih();
            FrmPilihItem.ItemTerpilih += new Form_Atur_Pilih.ItemTerpilihHandler(ItemTerpilih);
            FrmPilihItem.ID = ID;
            FrmPilihItem.Query = "select * from v_peminjaman";
            FrmPilihItem.ShowDialog();
        }

        private void ItemTerpilih(object sender, ItemTerpilihEventArgs e)
        {
            TBNim.Text = e.Daftar[2];
            LId.Text = e.Daftar[0];
            LTanggal.Text = DateTime.Now.ToLongDateString();
        }

        private void BSimpan_Click(object sender, EventArgs e)
        {
            if (CekKelengkapan() == false)
            {
                return;
            }

            if (MDA.DataAda("nim", "tabel_peminjaman", "nim = '" + TBNim.Text + "'") == false)
            {
                ToastNotification.Show(this, "Data dengan nim " + TBNim.Text + " tidak ada", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBNim.Focus();
                return;
            }

            string nilai, nilaiC;
            nilai = "tanggal_kembali = '" + MDA.AturTanggal(DateTime.Now) + "', status = 'Sudah Kembali'";
            nilaiC = "(" + MDA.AturTanggal(DateTime.Now) + ")";
            MDA.UbahData("tabel_peminjaman", nilai, "id_barang = '" + LId.Text + "'");
            MDA.Catat("Ubah Data Peminjaman", nilaiC);

            UpdatePeminjamanEventArgs args = new UpdatePeminjamanEventArgs(TBNim.Text);
            UpdatePeminjaman(this, args);

            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }
    }
}
