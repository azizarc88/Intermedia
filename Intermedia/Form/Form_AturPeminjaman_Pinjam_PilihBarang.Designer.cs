﻿namespace Intermedia.Form
{
    partial class Form_AturPeminjaman_Pinjam_PilihBarang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.BBatal = new DevComponents.DotNetBar.ButtonX();
            this.TBCari = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.LContentDA = new System.Windows.Forms.Label();
            this.LCNimDA = new System.Windows.Forms.Label();
            this.BPilih = new DevComponents.DotNetBar.ButtonX();
            this.DGBarang = new System.Windows.Forms.DataGridView();
            this.id_barang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nama_anggota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kelas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nama_barang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DGBarang)).BeginInit();
            this.SuspendLayout();
            // 
            // BBatal
            // 
            this.BBatal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BBatal.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BBatal.Location = new System.Drawing.Point(402, 366);
            this.BBatal.Name = "BBatal";
            this.BBatal.Size = new System.Drawing.Size(101, 29);
            this.BBatal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BBatal.TabIndex = 25;
            this.BBatal.Text = "Batal";
            // 
            // TBCari
            // 
            this.TBCari.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBCari.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBCari.Border.Class = "TextBoxBorder";
            this.TBCari.Border.CornerDiameter = 3;
            this.TBCari.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBCari.ForeColor = System.Drawing.Color.Black;
            this.TBCari.Location = new System.Drawing.Point(8, 8);
            this.TBCari.Name = "TBCari";
            this.TBCari.Size = new System.Drawing.Size(605, 22);
            this.TBCari.TabIndex = 24;
            this.TBCari.WatermarkColor = System.Drawing.SystemColors.Desktop;
            this.TBCari.WatermarkText = "Cari.....";
            this.TBCari.TextChanged += new System.EventHandler(this.TBCari_TextChanged);
            // 
            // LContentDA
            // 
            this.LContentDA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LContentDA.AutoSize = true;
            this.LContentDA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LContentDA.ForeColor = System.Drawing.Color.Black;
            this.LContentDA.Location = new System.Drawing.Point(86, 372);
            this.LContentDA.Name = "LContentDA";
            this.LContentDA.Size = new System.Drawing.Size(0, 13);
            this.LContentDA.TabIndex = 23;
            // 
            // LCNimDA
            // 
            this.LCNimDA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LCNimDA.AutoSize = true;
            this.LCNimDA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LCNimDA.ForeColor = System.Drawing.Color.Black;
            this.LCNimDA.Location = new System.Drawing.Point(16, 372);
            this.LCNimDA.Name = "LCNimDA";
            this.LCNimDA.Size = new System.Drawing.Size(0, 13);
            this.LCNimDA.TabIndex = 22;
            // 
            // BPilih
            // 
            this.BPilih.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPilih.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BPilih.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BPilih.Location = new System.Drawing.Point(512, 366);
            this.BPilih.Name = "BPilih";
            this.BPilih.Size = new System.Drawing.Size(101, 29);
            this.BPilih.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPilih.TabIndex = 26;
            this.BPilih.Text = "Pilih";
            this.BPilih.Click += new System.EventHandler(this.BPilih_Click);
            // 
            // DGBarang
            // 
            this.DGBarang.AllowUserToAddRows = false;
            this.DGBarang.AllowUserToDeleteRows = false;
            this.DGBarang.AllowUserToResizeColumns = false;
            this.DGBarang.AllowUserToResizeRows = false;
            this.DGBarang.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGBarang.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGBarang.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGBarang.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGBarang.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGBarang.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGBarang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGBarang.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_barang,
            this.nama_anggota,
            this.kelas,
            this.nama_barang});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGBarang.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGBarang.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DGBarang.EnableHeadersVisualStyles = false;
            this.DGBarang.GridColor = System.Drawing.Color.LightGray;
            this.DGBarang.Location = new System.Drawing.Point(6, 40);
            this.DGBarang.MultiSelect = false;
            this.DGBarang.Name = "DGBarang";
            this.DGBarang.ReadOnly = true;
            this.DGBarang.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGBarang.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGBarang.RowHeadersVisible = false;
            this.DGBarang.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DGBarang.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGBarang.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGBarang.Size = new System.Drawing.Size(608, 316);
            this.DGBarang.TabIndex = 21;
            this.DGBarang.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGBarang_CellContentClick);
            // 
            // id_barang
            // 
            this.id_barang.DataPropertyName = "id_barang";
            this.id_barang.FillWeight = 50F;
            this.id_barang.HeaderText = "ID Barang";
            this.id_barang.Name = "id_barang";
            this.id_barang.ReadOnly = true;
            // 
            // nama_anggota
            // 
            this.nama_anggota.DataPropertyName = "nama_anggota";
            this.nama_anggota.HeaderText = "Disumbangkan Oleh";
            this.nama_anggota.Name = "nama_anggota";
            this.nama_anggota.ReadOnly = true;
            // 
            // kelas
            // 
            this.kelas.DataPropertyName = "kelas";
            this.kelas.FillWeight = 50F;
            this.kelas.HeaderText = "Kelas";
            this.kelas.Name = "kelas";
            this.kelas.ReadOnly = true;
            // 
            // nama_barang
            // 
            this.nama_barang.DataPropertyName = "nama_barang";
            this.nama_barang.HeaderText = "Nama Barang";
            this.nama_barang.Name = "nama_barang";
            this.nama_barang.ReadOnly = true;
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form_AturPeminjaman_Pinjam_PilihBarang
            // 
            this.AcceptButton = this.BPilih;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BBatal;
            this.ClientSize = new System.Drawing.Size(620, 403);
            this.Controls.Add(this.BBatal);
            this.Controls.Add(this.TBCari);
            this.Controls.Add(this.LContentDA);
            this.Controls.Add(this.LCNimDA);
            this.Controls.Add(this.BPilih);
            this.Controls.Add(this.DGBarang);
            this.DoubleBuffered = true;
            this.MinimizeBox = false;
            this.Name = "Form_AturPeminjaman_Pinjam_PilihBarang";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pilih Barang";
            this.Load += new System.EventHandler(this.Form_AturPeminjaman_Pinjam_PilihBarang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGBarang)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevComponents.DotNetBar.ButtonX BBatal;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBCari;
        internal System.Windows.Forms.Label LContentDA;
        internal System.Windows.Forms.Label LCNimDA;
        internal DevComponents.DotNetBar.ButtonX BPilih;
        internal System.Windows.Forms.DataGridView DGBarang;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_barang;
        private System.Windows.Forms.DataGridViewTextBoxColumn nama_anggota;
        private System.Windows.Forms.DataGridViewTextBoxColumn kelas;
        private System.Windows.Forms.DataGridViewTextBoxColumn nama_barang;
        private System.Windows.Forms.Timer timer1;
    }
}