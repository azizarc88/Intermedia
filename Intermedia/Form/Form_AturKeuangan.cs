﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_AturKeuangan : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();

        public delegate void DataGridSegarkanHandler(object sender, DataGridSegarkanEventArgs e);
        public event DataGridSegarkanHandler DataGridSegarkan;

        public List<string> DataUbah = new List<string>();
        public int total, nominal;

        public Form_AturKeuangan()
        {
            InitializeComponent();
        }

        internal void SetTampilan(string UbahATAUTambah)
        {
            if (UbahATAUTambah == "ubah")
            {
                BSimpan.Text = "Ubah";
                BPilih.Enabled = false;
                TBNim.ReadOnly = true;
                TBNim.ButtonClear.Enabled = false;
                TBNim.BackColor = Color.LightSteelBlue;
                TBNominal.IsInputReadOnly = false;
                TBNominal.BackColor = Color.FromArgb(239, 239, 242);
                this.Text = "Ubah Data Keuangan";

                LId.Text = DataUbah[0];
                TBNim.Text = DataUbah[1];
                CBTipe.Text = DataUbah[2];
                TBKeterangan.Text = DataUbah[3];
                TBNominal.Text = DataUbah[4];
                nominal = int.Parse(DataUbah[4]);
                DTITanggal.Text = DataUbah[5];
            }
            else if (UbahATAUTambah == "ubahl")
            {
                BSimpan.Text = "Ubah";
                BPilih.Enabled = false;
                TBNim.ReadOnly = true;
                TBNim.ButtonClear.Enabled = false;
                TBNim.BackColor = Color.LightSteelBlue;
                TBNominal.IsInputReadOnly = true;
                TBNominal.BackColor = Color.LightSteelBlue;
                this.Text = "Ubah Data Keuangan";

                LId.Text = DataUbah[0];
                TBNim.Text = DataUbah[1];
                CBTipe.Text = DataUbah[2];
                TBKeterangan.Text = DataUbah[3];
                TBNominal.Text = DataUbah[4];
                nominal = int.Parse(DataUbah[4]);
                DTITanggal.Text = DataUbah[5];
            }
            else
            {
                BSimpan.Text = "Tambah";
                BPilih.Enabled = true;
                TBNim.ReadOnly = false;
                TBNim.ButtonClear.Enabled = true;
                TBNim.BackColor = Color.FromArgb(239, 239, 242);
                LId.Text = MDA.TentID("KS", "keuangan", "id_keuangan");
                TBNominal.IsInputReadOnly = false;
                TBNominal.BackColor = Color.FromArgb(239, 239, 242);
                this.Text = "Tambah Data Keuangan";
            }
        }

        private bool CekKelengkapan()
        {
            if (TBNim.Text == "")
            {
                ToastNotification.Show(this, "Nim dari anggota harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBNim.Focus();
                return false;
            }
            else if (CBTipe.SelectedItem.ToString() == "")
            {
                ToastNotification.Show(this, "Tipe dana harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                CBTipe.Focus();
                return false;
            }
            else if (TBKeterangan.Text == "")
            {
                ToastNotification.Show(this, "Keterangan uang harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBKeterangan.Focus();
                return false;
            }
            else if (TBNominal.Text == "")
            {
                ToastNotification.Show(this, "Jumlah uang harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBNominal.Focus();
                return false;
            }
            else if (DTITanggal.Text.Length == 0)
            {
                ToastNotification.Show(this, "Tanggal harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                DTITanggal.Focus();
                return false;
            }
            return true;
        }

        private void Form_AturKeuangan_Tampil(object sender, EventArgs e)
        {

        }

        private void BSimpan_Click(object sender, EventArgs e)
        {
            if (BSimpan.Text == "Ubah")
            {
                if (CekKelengkapan() == false)
                {
                    return;
                }

                string nilai, nilaiC, nilai2, nilai2C;
                nilai = "tipes = '" + CBTipe.SelectedItem.ToString() + "', keterangan = '" + TBKeterangan.Text + "', nominal = '" + TBNominal.Value + "', tanggal = '" + MDA.AturWaktuUang(DTITanggal.Value) + "'";
                nilaiC = "(" + CBTipe.SelectedItem.ToString() + ", " + TBKeterangan.Text + ", " + TBNominal.Value + ", " + MDA.AturWaktuUang(DTITanggal.Value) + ")";
                nilai2 = "total = '" + (total + (TBNominal.Value - nominal)).ToString() + "'";
                nilai2C = "(" + (total + (TBNominal.Value - nominal)).ToString() + ")";
                MDA.UbahData("keuangan", nilai, "id_keuangan = '" + LId.Text + "'");
                MDA.Catat("Ubah Data Keuangan", nilaiC);
                MDA.UbahData("totalkeuangan", nilai2, "id_keuangan = '" + LId.Text + "'");
                MDA.Catat("Ubah Data Total Keuangan", nilai2C);
            }
            else if (BSimpan.Text == "Tambah")
            {
                if (CekKelengkapan() == false)
                {
                    return;
                }

                if (CBTipe.SelectedItem.ToString() == "Masuk")
                {
                    total += TBNominal.Value;
                }
                else
                {
                    total -= TBNominal.Value;
                }

                string nilai, nilaiC, nilai2, nilai2C;
                nilai = "'" + LId.Text + "', '" + TBNim.Text + "', '" + CBTipe.SelectedItem.ToString() + "', '" + TBKeterangan.Text + "', '" + TBNominal.Value + "', '" + MDA.AturWaktuUang(DTITanggal.Value) + "'";
                nilaiC = "(" + LId.Text + ", " + TBNim.Text + ", " + CBTipe.SelectedItem.ToString() + ", " + TBKeterangan.Text + ", " + TBNominal.Value + ", " + MDA.AturWaktuUang(DTITanggal.Value) + ")";
                nilai2 = "'" + LId.Text + "', '" + TBNim.Text + "', '" + total + "'";
                nilai2C = "(" + LId.Text + ", " + TBNim.Text + ", " + total + ")";
                MDA.TambahData("keuangan", nilai);
                MDA.Catat("Tambah Data Keuangan", nilaiC);
                MDA.TambahData("totalkeuangan", nilai2);
                MDA.Catat("Tambah Data Total Keuangan", nilai2C);
            }

            DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Keuangan");
            DataGridSegarkan(this, args);

            LId.Text = MDA.TentID("KS", "keuangan", "id_keuangan");

            if (CBOtoClose.Checked == true)
            {
                this.Close();
                this.DialogResult = System.Windows.Forms.DialogResult.Yes;
            }

            ToastNotification.Show(this,"Data telah ditambahkan", eToastPosition.BottomCenter);
        }

        private void BPilih_Click(object sender, EventArgs e)
        {
            Form_Atur_Pilih FrmPilihItem = new Form_Atur_Pilih();
            FrmPilihItem.ItemTerpilih += new Form_Atur_Pilih.ItemTerpilihHandler(ItemTerpilih);
            FrmPilihItem.Query = "select nim, nama_anggota, kelas, alamat, j_kel, no_hp from anggota where nim > '1000' order by nama_anggota asc";
            FrmPilihItem.ShowDialog();
        }

        private void ItemTerpilih(object sender, ItemTerpilihEventArgs e)
        {
            CBTipe.Focus();

            TBNim.Text = e.Daftar[0];
        }
    }
}
