﻿namespace Intermedia.Form
{
    partial class Form_AturStrukturA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DTIJabatanAkhir = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.DTIJabatanAwal = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.TBNim = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.LAlamat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.LNoTelp = new DevComponents.DotNetBar.LabelX();
            this.LKelas = new DevComponents.DotNetBar.LabelX();
            this.LNama = new DevComponents.DotNetBar.LabelX();
            this.LabelX7 = new DevComponents.DotNetBar.LabelX();
            this.LabelX6 = new DevComponents.DotNetBar.LabelX();
            this.LabelX5 = new DevComponents.DotNetBar.LabelX();
            this.GroupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.LabelX4 = new DevComponents.DotNetBar.LabelX();
            this.BSimpan = new DevComponents.DotNetBar.ButtonX();
            this.BTutup = new DevComponents.DotNetBar.ButtonX();
            this.LabelX3 = new DevComponents.DotNetBar.LabelX();
            this.LabelX2 = new DevComponents.DotNetBar.LabelX();
            this.LabelX1 = new DevComponents.DotNetBar.LabelX();
            this.Label1 = new DevComponents.DotNetBar.LabelX();
            this.CBJabatan = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.LId_Jabatan = new DevComponents.DotNetBar.LabelX();
            this.styleManagerAmbient1 = new DevComponents.DotNetBar.StyleManagerAmbient(this.components);
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            ((System.ComponentModel.ISupportInitialize)(this.DTIJabatanAkhir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTIJabatanAwal)).BeginInit();
            this.GroupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DTIJabatanAkhir
            // 
            this.DTIJabatanAkhir.AntiAlias = true;
            this.DTIJabatanAkhir.AutoAdvance = true;
            this.DTIJabatanAkhir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.DTIJabatanAkhir.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DTIJabatanAkhir.BackgroundStyle.CornerDiameter = 2;
            this.DTIJabatanAkhir.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.DTIJabatanAkhir.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DTIJabatanAkhir.ButtonDropDown.Visible = true;
            this.DTIJabatanAkhir.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.DTIJabatanAkhir, true);
            this.DTIJabatanAkhir.IsPopupCalendarOpen = false;
            this.DTIJabatanAkhir.Location = new System.Drawing.Point(402, 142);
            // 
            // 
            // 
            this.DTIJabatanAkhir.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTIJabatanAkhir.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTIJabatanAkhir.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.DTIJabatanAkhir.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DTIJabatanAkhir.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DTIJabatanAkhir.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DTIJabatanAkhir.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DTIJabatanAkhir.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DTIJabatanAkhir.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DTIJabatanAkhir.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DTIJabatanAkhir.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTIJabatanAkhir.MonthCalendar.DisplayMonth = new System.DateTime(2014, 2, 1, 0, 0, 0, 0);
            this.DTIJabatanAkhir.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.DTIJabatanAkhir.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DTIJabatanAkhir.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTIJabatanAkhir.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DTIJabatanAkhir.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DTIJabatanAkhir.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DTIJabatanAkhir.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTIJabatanAkhir.MonthCalendar.TodayButtonVisible = true;
            this.DTIJabatanAkhir.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DTIJabatanAkhir.Name = "DTIJabatanAkhir";
            this.DTIJabatanAkhir.Size = new System.Drawing.Size(206, 20);
            this.DTIJabatanAkhir.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DTIJabatanAkhir.TabIndex = 43;
            this.DTIJabatanAkhir.TimeSelectorTimeFormat = DevComponents.Editors.DateTimeAdv.eTimeSelectorFormat.Time12H;
            // 
            // DTIJabatanAwal
            // 
            this.DTIJabatanAwal.AntiAlias = true;
            this.DTIJabatanAwal.AutoAdvance = true;
            this.DTIJabatanAwal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.DTIJabatanAwal.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DTIJabatanAwal.BackgroundStyle.CornerDiameter = 2;
            this.DTIJabatanAwal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.DTIJabatanAwal.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DTIJabatanAwal.ButtonDropDown.Visible = true;
            this.DTIJabatanAwal.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.DTIJabatanAwal, true);
            this.DTIJabatanAwal.IsPopupCalendarOpen = false;
            this.DTIJabatanAwal.Location = new System.Drawing.Point(402, 110);
            // 
            // 
            // 
            this.DTIJabatanAwal.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTIJabatanAwal.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTIJabatanAwal.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.DTIJabatanAwal.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DTIJabatanAwal.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DTIJabatanAwal.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DTIJabatanAwal.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DTIJabatanAwal.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DTIJabatanAwal.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DTIJabatanAwal.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DTIJabatanAwal.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTIJabatanAwal.MonthCalendar.DisplayMonth = new System.DateTime(2014, 2, 1, 0, 0, 0, 0);
            this.DTIJabatanAwal.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.DTIJabatanAwal.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DTIJabatanAwal.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTIJabatanAwal.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DTIJabatanAwal.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DTIJabatanAwal.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DTIJabatanAwal.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTIJabatanAwal.MonthCalendar.TodayButtonVisible = true;
            this.DTIJabatanAwal.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DTIJabatanAwal.Name = "DTIJabatanAwal";
            this.DTIJabatanAwal.Size = new System.Drawing.Size(206, 20);
            this.DTIJabatanAwal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DTIJabatanAwal.TabIndex = 42;
            this.DTIJabatanAwal.TimeSelectorTimeFormat = DevComponents.Editors.DateTimeAdv.eTimeSelectorFormat.Time12H;
            // 
            // TBNim
            // 
            this.TBNim.AllowPromptAsInput = false;
            this.TBNim.AntiAlias = true;
            this.TBNim.AsciiOnly = true;
            this.TBNim.BackColor = System.Drawing.Color.LightSteelBlue;
            // 
            // 
            // 
            this.TBNim.BackgroundStyle.Class = "TextBoxBorder";
            this.TBNim.BackgroundStyle.CornerDiameter = 2;
            this.TBNim.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBNim.BeepOnError = true;
            this.TBNim.ButtonClear.Enabled = false;
            this.TBNim.ButtonClear.Visible = true;
            this.TBNim.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.styleManagerAmbient1.SetEnableAmbientSettings(this.TBNim, DevComponents.DotNetBar.eAmbientSettings.None);
            this.TBNim.ForeColor = System.Drawing.Color.Black;
            this.TBNim.HidePromptOnLeave = true;
            this.TBNim.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Insert;
            this.TBNim.Location = new System.Drawing.Point(402, 44);
            this.TBNim.Mask = "00.00.0000";
            this.TBNim.Name = "TBNim";
            this.TBNim.ReadOnly = true;
            this.TBNim.ResetOnPrompt = false;
            this.TBNim.ResetOnSpace = false;
            this.TBNim.Size = new System.Drawing.Size(206, 22);
            this.TBNim.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TBNim.TabIndex = 45;
            this.TBNim.Text = "";
            this.TBNim.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // LAlamat
            // 
            this.LAlamat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LAlamat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LAlamat.Border.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.LAlamat.Border.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.LAlamat.Border.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.LAlamat.Border.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.LAlamat.Border.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.LAlamat.Border.Class = "TextBoxBorder";
            this.LAlamat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LAlamat.ForeColor = System.Drawing.Color.Black;
            this.LAlamat.Location = new System.Drawing.Point(100, 72);
            this.LAlamat.Multiline = true;
            this.LAlamat.Name = "LAlamat";
            this.LAlamat.ReadOnly = true;
            this.LAlamat.Size = new System.Drawing.Size(146, 46);
            this.LAlamat.TabIndex = 45;
            this.LAlamat.Text = "Belum ditentukan";
            // 
            // LNoTelp
            // 
            this.LNoTelp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LNoTelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNoTelp.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNoTelp.ForeColor = System.Drawing.Color.Black;
            this.LNoTelp.Location = new System.Drawing.Point(102, 145);
            this.LNoTelp.Name = "LNoTelp";
            this.LNoTelp.Size = new System.Drawing.Size(150, 23);
            this.LNoTelp.TabIndex = 44;
            this.LNoTelp.Text = "Belum ditentukan";
            // 
            // LKelas
            // 
            this.LKelas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LKelas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LKelas.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LKelas.ForeColor = System.Drawing.Color.Black;
            this.LKelas.Location = new System.Drawing.Point(102, 38);
            this.LKelas.Name = "LKelas";
            this.LKelas.Size = new System.Drawing.Size(150, 23);
            this.LKelas.TabIndex = 42;
            this.LKelas.Text = "Belum ditentukan";
            // 
            // LNama
            // 
            this.LNama.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LNama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNama.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNama.ForeColor = System.Drawing.Color.Black;
            this.LNama.Location = new System.Drawing.Point(102, 6);
            this.LNama.Name = "LNama";
            this.LNama.Size = new System.Drawing.Size(150, 23);
            this.LNama.TabIndex = 41;
            this.LNama.Text = "Belum ditentukan";
            // 
            // LabelX7
            // 
            this.LabelX7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelX7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX7.ForeColor = System.Drawing.Color.Black;
            this.LabelX7.Location = new System.Drawing.Point(10, 145);
            this.LabelX7.Name = "LabelX7";
            this.LabelX7.Size = new System.Drawing.Size(100, 23);
            this.LabelX7.TabIndex = 40;
            this.LabelX7.Text = "No. Handphone";
            // 
            // LabelX6
            // 
            this.LabelX6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelX6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX6.ForeColor = System.Drawing.Color.Black;
            this.LabelX6.Location = new System.Drawing.Point(10, 71);
            this.LabelX6.Name = "LabelX6";
            this.LabelX6.Size = new System.Drawing.Size(100, 23);
            this.LabelX6.TabIndex = 39;
            this.LabelX6.Text = "Alamat";
            // 
            // LabelX5
            // 
            this.LabelX5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelX5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX5.ForeColor = System.Drawing.Color.Black;
            this.LabelX5.Location = new System.Drawing.Point(10, 38);
            this.LabelX5.Name = "LabelX5";
            this.LabelX5.Size = new System.Drawing.Size(100, 23);
            this.LabelX5.TabIndex = 38;
            this.LabelX5.Text = "Kelas";
            // 
            // GroupPanel1
            // 
            this.GroupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GroupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.GroupPanel1.Controls.Add(this.LAlamat);
            this.GroupPanel1.Controls.Add(this.LNoTelp);
            this.GroupPanel1.Controls.Add(this.LKelas);
            this.GroupPanel1.Controls.Add(this.LNama);
            this.GroupPanel1.Controls.Add(this.LabelX7);
            this.GroupPanel1.Controls.Add(this.LabelX6);
            this.GroupPanel1.Controls.Add(this.LabelX5);
            this.GroupPanel1.Controls.Add(this.LabelX4);
            this.GroupPanel1.Location = new System.Drawing.Point(10, 6);
            this.GroupPanel1.Name = "GroupPanel1";
            this.GroupPanel1.Size = new System.Drawing.Size(268, 201);
            // 
            // 
            // 
            this.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.GroupPanel1.Style.BackColorGradientAngle = 90;
            this.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground;
            this.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel1.Style.BorderBottomWidth = 1;
            this.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel1.Style.BorderLeftWidth = 1;
            this.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel1.Style.BorderRightWidth = 1;
            this.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.GroupPanel1.Style.BorderTopWidth = 1;
            this.GroupPanel1.Style.CornerDiameter = 4;
            this.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.GroupPanel1.TabIndex = 51;
            this.GroupPanel1.Text = "Info Anggota";
            this.GroupPanel1.TitleImagePosition = DevComponents.DotNetBar.eTitleImagePosition.Center;
            // 
            // LabelX4
            // 
            this.LabelX4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX4.ForeColor = System.Drawing.Color.Black;
            this.LabelX4.Location = new System.Drawing.Point(10, 6);
            this.LabelX4.Name = "LabelX4";
            this.LabelX4.Size = new System.Drawing.Size(100, 23);
            this.LabelX4.TabIndex = 37;
            this.LabelX4.Text = "Nama";
            // 
            // BSimpan
            // 
            this.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BSimpan.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BSimpan.Location = new System.Drawing.Point(468, 174);
            this.BSimpan.Name = "BSimpan";
            this.BSimpan.Size = new System.Drawing.Size(142, 34);
            this.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BSimpan.SubItemsExpandWidth = 18;
            this.BSimpan.TabIndex = 49;
            this.BSimpan.Text = "Simpan";
            this.BSimpan.Click += new System.EventHandler(this.BSimpan_Click);
            // 
            // BTutup
            // 
            this.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BTutup.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.Location = new System.Drawing.Point(304, 174);
            this.BTutup.Name = "BTutup";
            this.BTutup.Size = new System.Drawing.Size(142, 34);
            this.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BTutup.TabIndex = 50;
            this.BTutup.Text = "Batal";
            // 
            // LabelX3
            // 
            this.LabelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX3.ForeColor = System.Drawing.Color.Black;
            this.LabelX3.Location = new System.Drawing.Point(302, 142);
            this.LabelX3.Name = "LabelX3";
            this.LabelX3.Size = new System.Drawing.Size(100, 23);
            this.LabelX3.TabIndex = 48;
            this.LabelX3.Text = "Jabatan Berakhir";
            // 
            // LabelX2
            // 
            this.LabelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX2.ForeColor = System.Drawing.Color.Black;
            this.LabelX2.Location = new System.Drawing.Point(302, 108);
            this.LabelX2.Name = "LabelX2";
            this.LabelX2.Size = new System.Drawing.Size(100, 23);
            this.LabelX2.TabIndex = 47;
            this.LabelX2.Text = "Jabatan Berawal";
            // 
            // LabelX1
            // 
            this.LabelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX1.ForeColor = System.Drawing.Color.Black;
            this.LabelX1.Location = new System.Drawing.Point(302, 76);
            this.LabelX1.Name = "LabelX1";
            this.LabelX1.Size = new System.Drawing.Size(100, 23);
            this.LabelX1.TabIndex = 46;
            this.LabelX1.Text = "Nama Jabatan";
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.Label1.ForeColor = System.Drawing.Color.Black;
            this.Label1.Location = new System.Drawing.Point(302, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 23);
            this.Label1.TabIndex = 44;
            this.Label1.Text = "Nim";
            // 
            // CBJabatan
            // 
            this.CBJabatan.DisplayMember = "Text";
            this.CBJabatan.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CBJabatan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBJabatan.ForeColor = System.Drawing.Color.Black;
            this.CBJabatan.FormattingEnabled = true;
            this.highlighter1.SetHighlightOnFocus(this.CBJabatan, true);
            this.CBJabatan.ItemHeight = 14;
            this.CBJabatan.Items.AddRange(new object[] {
            this.comboItem6,
            this.comboItem1,
            this.comboItem2,
            this.comboItem3,
            this.comboItem4,
            this.comboItem5});
            this.CBJabatan.Location = new System.Drawing.Point(402, 79);
            this.CBJabatan.Name = "CBJabatan";
            this.CBJabatan.Size = new System.Drawing.Size(206, 20);
            this.CBJabatan.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.CBJabatan.TabIndex = 52;
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "Administrator";
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "Ketua";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Wakil Ketua";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "Sekretaris";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "Bendahara";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "Anggota";
            // 
            // labelX8
            // 
            this.labelX8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.ForeColor = System.Drawing.Color.Black;
            this.labelX8.Location = new System.Drawing.Point(302, 12);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(100, 23);
            this.labelX8.TabIndex = 53;
            this.labelX8.Text = "ID";
            // 
            // LId_Jabatan
            // 
            this.LId_Jabatan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LId_Jabatan.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LId_Jabatan.ForeColor = System.Drawing.Color.Black;
            this.LId_Jabatan.Location = new System.Drawing.Point(402, 12);
            this.LId_Jabatan.Name = "LId_Jabatan";
            this.LId_Jabatan.Size = new System.Drawing.Size(100, 23);
            this.LId_Jabatan.TabIndex = 54;
            this.LId_Jabatan.Text = "SA001";
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // Form_AturStrukturA
            // 
            this.AcceptButton = this.BSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BTutup;
            this.ClientSize = new System.Drawing.Size(617, 213);
            this.ControlBox = false;
            this.Controls.Add(this.LId_Jabatan);
            this.Controls.Add(this.labelX8);
            this.Controls.Add(this.CBJabatan);
            this.Controls.Add(this.GroupPanel1);
            this.Controls.Add(this.DTIJabatanAkhir);
            this.Controls.Add(this.DTIJabatanAwal);
            this.Controls.Add(this.BSimpan);
            this.Controls.Add(this.BTutup);
            this.Controls.Add(this.TBNim);
            this.Controls.Add(this.LabelX3);
            this.Controls.Add(this.LabelX2);
            this.Controls.Add(this.LabelX1);
            this.Controls.Add(this.Label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form_AturStrukturA";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ubah Data Struktur Anggota";
            this.Shown += new System.EventHandler(this.Form_AturStrukturA_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.DTIJabatanAkhir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTIJabatanAwal)).EndInit();
            this.GroupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.Editors.DateTimeAdv.DateTimeInput DTIJabatanAkhir;
        internal DevComponents.Editors.DateTimeAdv.DateTimeInput DTIJabatanAwal;
        internal DevComponents.DotNetBar.Controls.MaskedTextBoxAdv TBNim;
        internal DevComponents.DotNetBar.Controls.TextBoxX LAlamat;
        internal DevComponents.DotNetBar.LabelX LNoTelp;
        internal DevComponents.DotNetBar.LabelX LKelas;
        internal DevComponents.DotNetBar.LabelX LNama;
        internal DevComponents.DotNetBar.LabelX LabelX7;
        internal DevComponents.DotNetBar.LabelX LabelX6;
        internal DevComponents.DotNetBar.LabelX LabelX5;
        internal DevComponents.DotNetBar.Controls.GroupPanel GroupPanel1;
        internal DevComponents.DotNetBar.LabelX LabelX4;
        internal DevComponents.DotNetBar.ButtonX BSimpan;
        internal DevComponents.DotNetBar.ButtonX BTutup;
        internal DevComponents.DotNetBar.LabelX LabelX3;
        internal DevComponents.DotNetBar.LabelX LabelX2;
        internal DevComponents.DotNetBar.LabelX LabelX1;
        internal DevComponents.DotNetBar.LabelX Label1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CBJabatan;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        internal DevComponents.DotNetBar.LabelX labelX8;
        internal DevComponents.DotNetBar.LabelX LId_Jabatan;
        private DevComponents.DotNetBar.StyleManagerAmbient styleManagerAmbient1;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
        private DevComponents.Editors.ComboItem comboItem6;
    }
}