﻿namespace Intermedia.Form
{
    partial class Form_AturRapat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LNoID = new DevComponents.DotNetBar.LabelX();
            this.LId = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.TBRapat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.DTITanggal = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.LabelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.BSimpan = new DevComponents.DotNetBar.ButtonX();
            this.BTutup = new DevComponents.DotNetBar.ButtonX();
            this.TBHasil = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.LStatus = new DevComponents.DotNetBar.LabelX();
            this.TBWaktu = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.styleManagerAmbient1 = new DevComponents.DotNetBar.StyleManagerAmbient(this.components);
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            ((System.ComponentModel.ISupportInitialize)(this.DTITanggal)).BeginInit();
            this.SuspendLayout();
            // 
            // LNoID
            // 
            this.LNoID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNoID.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNoID.ForeColor = System.Drawing.Color.Black;
            this.LNoID.Location = new System.Drawing.Point(12, 12);
            this.LNoID.Name = "LNoID";
            this.LNoID.Size = new System.Drawing.Size(100, 23);
            this.LNoID.TabIndex = 93;
            this.LNoID.Text = "ID";
            // 
            // LId
            // 
            this.LId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LId.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LId.ForeColor = System.Drawing.Color.Black;
            this.LId.Location = new System.Drawing.Point(112, 12);
            this.LId.Name = "LId";
            this.LId.Size = new System.Drawing.Size(100, 23);
            this.LId.TabIndex = 94;
            this.LId.Text = "RP001";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(12, 41);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(100, 23);
            this.labelX1.TabIndex = 96;
            this.labelX1.Text = "Nama Rapat";
            // 
            // TBRapat
            // 
            this.TBRapat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBRapat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBRapat.Border.Class = "TextBoxBorder";
            this.TBRapat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TBRapat.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBRapat, true);
            this.TBRapat.Location = new System.Drawing.Point(112, 44);
            this.TBRapat.Multiline = true;
            this.TBRapat.Name = "TBRapat";
            this.TBRapat.Size = new System.Drawing.Size(261, 89);
            this.TBRapat.TabIndex = 58;
            // 
            // DTITanggal
            // 
            this.DTITanggal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DTITanggal.AntiAlias = true;
            this.DTITanggal.AutoAdvance = true;
            this.DTITanggal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.DTITanggal.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DTITanggal.BackgroundStyle.CornerDiameter = 2;
            this.DTITanggal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.DTITanggal.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DTITanggal.ButtonDropDown.Visible = true;
            this.DTITanggal.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.DTITanggal, true);
            this.DTITanggal.IsPopupCalendarOpen = false;
            this.DTITanggal.Location = new System.Drawing.Point(112, 235);
            // 
            // 
            // 
            this.DTITanggal.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTITanggal.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTITanggal.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.DTITanggal.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTITanggal.MonthCalendar.DisplayMonth = new System.DateTime(2014, 2, 1, 0, 0, 0, 0);
            this.DTITanggal.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.DTITanggal.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DTITanggal.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTITanggal.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DTITanggal.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DTITanggal.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DTITanggal.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTITanggal.MonthCalendar.TodayButtonVisible = true;
            this.DTITanggal.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DTITanggal.Name = "DTITanggal";
            this.DTITanggal.Size = new System.Drawing.Size(261, 20);
            this.DTITanggal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DTITanggal.TabIndex = 60;
            this.DTITanggal.TimeSelectorTimeFormat = DevComponents.Editors.DateTimeAdv.eTimeSelectorFormat.Time12H;
            // 
            // LabelX2
            // 
            this.LabelX2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX2.ForeColor = System.Drawing.Color.Black;
            this.LabelX2.Location = new System.Drawing.Point(12, 233);
            this.LabelX2.Name = "LabelX2";
            this.LabelX2.Size = new System.Drawing.Size(100, 23);
            this.LabelX2.TabIndex = 98;
            this.LabelX2.Text = "Tanggal";
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(12, 261);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(94, 23);
            this.labelX3.TabIndex = 99;
            this.labelX3.Text = "Waktu";
            // 
            // BSimpan
            // 
            this.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BSimpan.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.BSimpan.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BSimpan.Location = new System.Drawing.Point(202, 293);
            this.BSimpan.Name = "BSimpan";
            this.BSimpan.Size = new System.Drawing.Size(142, 34);
            this.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BSimpan.SubItemsExpandWidth = 18;
            this.BSimpan.TabIndex = 63;
            this.BSimpan.Text = "Simpan";
            this.BSimpan.Click += new System.EventHandler(this.BSimpan_Click);
            // 
            // BTutup
            // 
            this.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BTutup.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.BTutup.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.Location = new System.Drawing.Point(38, 293);
            this.BTutup.Name = "BTutup";
            this.BTutup.Size = new System.Drawing.Size(142, 34);
            this.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BTutup.TabIndex = 62;
            this.BTutup.Text = "Batal";
            // 
            // TBHasil
            // 
            this.TBHasil.AcceptsReturn = true;
            this.TBHasil.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBHasil.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBHasil.Border.Class = "TextBoxBorder";
            this.TBHasil.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TBHasil.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBHasil, true);
            this.TBHasil.Location = new System.Drawing.Point(112, 139);
            this.TBHasil.Multiline = true;
            this.TBHasil.Name = "TBHasil";
            this.TBHasil.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TBHasil.Size = new System.Drawing.Size(261, 89);
            this.TBHasil.TabIndex = 59;
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(12, 136);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(100, 23);
            this.labelX4.TabIndex = 97;
            this.labelX4.Text = "Hasil Pembahasan";
            // 
            // LStatus
            // 
            this.LStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LStatus.ForeColor = System.Drawing.Color.Black;
            this.LStatus.Location = new System.Drawing.Point(255, 12);
            this.LStatus.Name = "LStatus";
            this.LStatus.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LStatus.Size = new System.Drawing.Size(118, 23);
            this.LStatus.TabIndex = 95;
            this.LStatus.Text = "Belum Dilaksanakan";
            // 
            // TBWaktu
            // 
            this.TBWaktu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBWaktu.AntiAlias = true;
            this.TBWaktu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBWaktu.BackgroundStyle.Class = "TextBoxBorder";
            this.TBWaktu.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TBWaktu.BeepOnError = true;
            this.TBWaktu.ButtonClear.Visible = true;
            this.styleManagerAmbient1.SetEnableAmbientSettings(this.TBWaktu, DevComponents.DotNetBar.eAmbientSettings.None);
            this.TBWaktu.HidePromptOnLeave = true;
            this.highlighter1.SetHighlightOnFocus(this.TBWaktu, true);
            this.TBWaktu.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.TBWaktu.Location = new System.Drawing.Point(112, 262);
            this.TBWaktu.Mask = "00:00";
            this.TBWaktu.Name = "TBWaktu";
            this.TBWaktu.Size = new System.Drawing.Size(261, 19);
            this.TBWaktu.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TBWaktu.TabIndex = 61;
            this.TBWaktu.Text = "";
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // Form_AturRapat
            // 
            this.AcceptButton = this.BSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BTutup;
            this.ClientSize = new System.Drawing.Size(382, 334);
            this.ControlBox = false;
            this.Controls.Add(this.TBWaktu);
            this.Controls.Add(this.LStatus);
            this.Controls.Add(this.TBHasil);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.BSimpan);
            this.Controls.Add(this.BTutup);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.DTITanggal);
            this.Controls.Add(this.LabelX2);
            this.Controls.Add(this.TBRapat);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.LNoID);
            this.Controls.Add(this.LId);
            this.DoubleBuffered = true;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(394, 200);
            this.Name = "Form_AturRapat";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tambah Agenda Rapat";
            this.Shown += new System.EventHandler(this.Form_AturPiket_Tampil);
            ((System.ComponentModel.ISupportInitialize)(this.DTITanggal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.LabelX LNoID;
        internal DevComponents.DotNetBar.LabelX LId;
        internal DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX TBRapat;
        internal DevComponents.Editors.DateTimeAdv.DateTimeInput DTITanggal;
        internal DevComponents.DotNetBar.LabelX LabelX2;
        internal DevComponents.DotNetBar.LabelX labelX3;
        internal DevComponents.DotNetBar.ButtonX BSimpan;
        internal DevComponents.DotNetBar.ButtonX BTutup;
        private DevComponents.DotNetBar.Controls.TextBoxX TBHasil;
        internal DevComponents.DotNetBar.LabelX labelX4;
        internal DevComponents.DotNetBar.LabelX LStatus;
        private DevComponents.DotNetBar.Controls.MaskedTextBoxAdv TBWaktu;
        private DevComponents.DotNetBar.StyleManagerAmbient styleManagerAmbient1;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
    }
}