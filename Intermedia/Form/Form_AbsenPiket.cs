﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_AbsenPiket : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();

        public List<DateTime> DataTanggal = new List<DateTime>();

        public Form_AbsenPiket()
        {
            InitializeComponent();
        }

        private void Form_AbsenRapat_Load(object sender, EventArgs e)
        {
            CBTanggal.DataSource = DataTanggal;

            DGAbsenPiket.DataSource = MDA.IsiDG("select nama_anggota, hari, waktu, absen_hari, absen_tanggal, waktu_absen from v_absen_piket");
            CBTampilSemua.Checked = true;
        }

        private void CBTampilSemua_CheckedChanged(object sender, EventArgs e)
        {
            if (CBTampilSemua.Checked == true)
            {
                CBTanggal.Enabled = false;
                CBLogika.Enabled = false;
                CBWaktu.Enabled = false;
            }
            else
            {
                CBTanggal.Enabled = true;
                CBLogika.Enabled = true;
                CBWaktu.Enabled = true;
            }
            DGAbsenPiket.DataSource = MDA.IsiDG("select nama_anggota, hari, waktu, absen_hari, absen_tanggal, waktu_absen from v_absen_piket");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (CBLogika.SelectedItem.ToString() == "DAN")
                {
                    DGAbsenPiket.DataSource = MDA.IsiDG("select * from v_absen_piket where absen_tanggal = '" + MDA.AturTanggal(DateTime.Parse(CBTanggal.SelectedItem.ToString()), "EN") + "' AND waktu = '" + CBWaktu.SelectedItem.ToString() + "'");
                }
                else
                {
                    DGAbsenPiket.DataSource = MDA.IsiDG("select * from v_absen_piket where absen_tanggal = '" + MDA.AturTanggal(DateTime.Parse(CBTanggal.SelectedItem.ToString()), "EN") + "' OR waktu = '" + CBWaktu.SelectedItem.ToString() + "'");
                }
                timer1.Enabled = false;
            }
            catch (Exception)
            {
            }
        }

        private void CBWaktu_SelectedIndexChanged(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            LWaktu.Text = DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString();
        }
    }
}
