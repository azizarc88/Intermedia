﻿namespace Intermedia.Form
{
    partial class Form_AturPeminjaman_Pinjam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LNoID = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.BPilihBarang = new DevComponents.DotNetBar.ButtonX();
            this.BSimpan = new DevComponents.DotNetBar.ButtonX();
            this.CBOtoClose = new DevComponents.DotNetBar.CheckBoxItem();
            this.BTutup = new DevComponents.DotNetBar.ButtonX();
            this.LId = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.BPilihNim = new DevComponents.DotNetBar.ButtonX();
            this.TBNim = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.TBIDBarang = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.SuspendLayout();
            // 
            // LNoID
            // 
            this.LNoID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNoID.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNoID.ForeColor = System.Drawing.Color.Black;
            this.LNoID.Location = new System.Drawing.Point(12, 7);
            this.LNoID.Name = "LNoID";
            this.LNoID.Size = new System.Drawing.Size(100, 23);
            this.LNoID.TabIndex = 84;
            this.LNoID.Text = "ID";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(12, 69);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(100, 23);
            this.labelX1.TabIndex = 86;
            this.labelX1.Text = "ID Barang";
            // 
            // BPilihBarang
            // 
            this.BPilihBarang.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPilihBarang.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.highlighter1.SetHighlightOnFocus(this.BPilihBarang, true);
            this.BPilihBarang.Location = new System.Drawing.Point(268, 69);
            this.BPilihBarang.Name = "BPilihBarang";
            this.BPilihBarang.Size = new System.Drawing.Size(68, 22);
            this.BPilihBarang.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPilihBarang.TabIndex = 89;
            this.BPilihBarang.Text = "Pilih";
            this.BPilihBarang.Click += new System.EventHandler(this.BPilih_Click);
            // 
            // BSimpan
            // 
            this.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BSimpan.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BSimpan.Location = new System.Drawing.Point(185, 104);
            this.BSimpan.Name = "BSimpan";
            this.BSimpan.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BSimpan.Size = new System.Drawing.Size(142, 34);
            this.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BSimpan.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.CBOtoClose});
            this.BSimpan.SubItemsExpandWidth = 18;
            this.BSimpan.TabIndex = 99;
            this.BSimpan.Text = "Tambah";
            this.BSimpan.Click += new System.EventHandler(this.BSimpan_Click);
            // 
            // CBOtoClose
            // 
            this.CBOtoClose.Checked = true;
            this.CBOtoClose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBOtoClose.GlobalItem = false;
            this.CBOtoClose.Name = "CBOtoClose";
            this.CBOtoClose.Stretch = true;
            this.CBOtoClose.Text = "Tutup otomatis";
            // 
            // BTutup
            // 
            this.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BTutup.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.Location = new System.Drawing.Point(21, 103);
            this.BTutup.Name = "BTutup";
            this.BTutup.Size = new System.Drawing.Size(142, 34);
            this.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BTutup.TabIndex = 98;
            this.BTutup.Text = "Batal";
            // 
            // LId
            // 
            this.LId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LId.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LId.ForeColor = System.Drawing.Color.Black;
            this.LId.Location = new System.Drawing.Point(112, 7);
            this.LId.Name = "LId";
            this.LId.Size = new System.Drawing.Size(51, 23);
            this.LId.TabIndex = 85;
            this.LId.Text = "PB001";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(12, 36);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(100, 23);
            this.labelX2.TabIndex = 100;
            this.labelX2.Text = "Atas Nama";
            // 
            // BPilihNim
            // 
            this.BPilihNim.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPilihNim.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.highlighter1.SetHighlightOnFocus(this.BPilihNim, true);
            this.BPilihNim.Location = new System.Drawing.Point(268, 36);
            this.BPilihNim.Name = "BPilihNim";
            this.BPilihNim.Size = new System.Drawing.Size(68, 22);
            this.BPilihNim.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPilihNim.TabIndex = 102;
            this.BPilihNim.Text = "Pilih";
            this.BPilihNim.Click += new System.EventHandler(this.BPilihNim_Click);
            // 
            // TBNim
            // 
            this.TBNim.AllowPromptAsInput = false;
            this.TBNim.AntiAlias = true;
            this.TBNim.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBNim.BackgroundStyle.Class = "TextBoxBorder";
            this.TBNim.BackgroundStyle.CornerDiameter = 2;
            this.TBNim.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBNim.BeepOnError = true;
            this.TBNim.ButtonClear.Visible = true;
            this.TBNim.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBNim, true);
            this.TBNim.Location = new System.Drawing.Point(112, 36);
            this.TBNim.Mask = "00.00.0000";
            this.TBNim.Name = "TBNim";
            this.TBNim.Size = new System.Drawing.Size(150, 22);
            this.TBNim.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TBNim.TabIndex = 101;
            this.TBNim.Text = "";
            this.TBNim.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // TBIDBarang
            // 
            this.TBIDBarang.AllowPromptAsInput = false;
            this.TBIDBarang.AntiAlias = true;
            this.TBIDBarang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBIDBarang.BackgroundStyle.Class = "TextBoxBorder";
            this.TBIDBarang.BackgroundStyle.CornerDiameter = 2;
            this.TBIDBarang.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBIDBarang.BeepOnError = true;
            this.TBIDBarang.ButtonClear.Visible = true;
            this.TBIDBarang.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBIDBarang, true);
            this.TBIDBarang.Location = new System.Drawing.Point(112, 69);
            this.TBIDBarang.Mask = "BR000";
            this.TBIDBarang.Name = "TBIDBarang";
            this.TBIDBarang.ReadOnly = true;
            this.TBIDBarang.Size = new System.Drawing.Size(150, 22);
            this.TBIDBarang.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TBIDBarang.TabIndex = 103;
            this.TBIDBarang.Text = "";
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // Form_AturPeminjaman_Pinjam
            // 
            this.AcceptButton = this.BSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BTutup;
            this.ClientSize = new System.Drawing.Size(340, 144);
            this.ControlBox = false;
            this.Controls.Add(this.TBIDBarang);
            this.Controls.Add(this.BPilihNim);
            this.Controls.Add(this.TBNim);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.BSimpan);
            this.Controls.Add(this.BTutup);
            this.Controls.Add(this.BPilihBarang);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.LNoID);
            this.Controls.Add(this.LId);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form_AturPeminjaman_Pinjam";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pinjam Barang";
            this.Shown += new System.EventHandler(this.Form_AturPeminjaman_Pinjam_Load_1);
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.LabelX LNoID;
        internal DevComponents.DotNetBar.LabelX labelX1;
        internal DevComponents.DotNetBar.ButtonX BPilihBarang;
        internal DevComponents.DotNetBar.ButtonX BSimpan;
        internal DevComponents.DotNetBar.CheckBoxItem CBOtoClose;
        internal DevComponents.DotNetBar.ButtonX BTutup;
        internal DevComponents.DotNetBar.LabelX LId;
        internal DevComponents.DotNetBar.LabelX labelX2;
        internal DevComponents.DotNetBar.ButtonX BPilihNim;
        internal DevComponents.DotNetBar.Controls.MaskedTextBoxAdv TBNim;
        internal DevComponents.DotNetBar.Controls.MaskedTextBoxAdv TBIDBarang;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
    }
}