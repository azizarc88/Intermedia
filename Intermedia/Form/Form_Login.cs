﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Metro;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_Login : MetroForm
    {
        ModKoneksi DB = new ModKoneksi();
        Pengguna LogPengguna = new Pengguna();

        string LoginSukses = "gagal";

        public Form_Login()
        {
            InitializeComponent();
        }

        private void Login()
        {
            if (TBNim.Text == "")
            {
                pesan p = new pesan("Intermedia", eTaskDialogIcon.Stop, "Data Kurang Lengkap", "Silakan masukan nim Anda dengan benar", eTaskDialogButton.Ok, eTaskDialogBackgroundColor.Orange);
                return;
            }

            if (TBPass.Text == "")
            {
                pesan p = new pesan("Intermedia", eTaskDialogIcon.Stop, "Data Kurang Lengkap", "Silakan masukan password Anda dengan benar", eTaskDialogButton.Ok, eTaskDialogBackgroundColor.Orange);
                return;
            }

            LogPengguna.Nim = TBNim.Text;
            LogPengguna.Pass = TBPass.Text;
            LoginSukses = LogPengguna.CekKeanggotaan();

            if (LoginSukses == "sukses")
            {
                MessageBoxEx.AntiAlias = true;
                MessageBoxEx.Show(this, "Selamat Datang " + LogPengguna.Nama.ToString() + ".", "Intermedia", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Program.Nim = LogPengguna.Nim;
                Program.Nama = LogPengguna.Nama;
                Program.Jabatan = LogPengguna.Jabatan;
                Form_Utama FormUtama = new Form_Utama();
                this.Hide();
                FormUtama.Show();
                DB.Catat("Login", "Login");
            }
            else if (LoginSukses == "gagal")
            {
                pesan p = new pesan("Intermedia", eTaskDialogIcon.Stop, "Login Gagal", "Silakan cek kembali nim dan password yang Anda masukan", eTaskDialogButton.Ok, eTaskDialogBackgroundColor.Red);
            }
        }

        private void BLogin_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void BTutup_Click(object sender, EventArgs e)
        {
            Form_Utama frm = new Form_Utama();
            frm.Close();
            this.Close();
        }
    }
}
