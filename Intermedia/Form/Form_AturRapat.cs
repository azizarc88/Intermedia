﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_AturRapat : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();

        public delegate void DataGridSegarkanHandler(object sender, DataGridSegarkanEventArgs e);
        public event DataGridSegarkanHandler DataGridSegarkan;

        public List<string> DataUbah = new List<string>();
        public string Status;

        public Form_AturRapat()
        {
            InitializeComponent();
        }

        internal void SetTampilan(string UbahATAUTambah)
        {
            if (UbahATAUTambah == "tambah")
            {
                BSimpan.Text = "Tambah";
                labelX4.Visible = false;
                TBHasil.Visible = false;
                this.Size = new Size(398, 276);
                this.Text = "Tambah Agenda Rapat";

                LId.Text = MDA.TentID("RP", "rapat", "id_rapat");
            }
            else if (UbahATAUTambah == "ubah setelah")
            {
                BSimpan.Text = "Tentukan Hasil";
                labelX4.Visible = true;
                TBHasil.Visible = true;
                this.Size = new Size(398, 372);
                this.Text = "Ubah Data Rapat";

                LId.Text = DataUbah[0];
                TBRapat.Text = DataUbah[1];
                TBHasil.Text = DataUbah[2];
                DTITanggal.Text = DataUbah[3];
                TBWaktu.Text = DataUbah[4];
            }
            else if (UbahATAUTambah == "ubah sebelum")
            {
                this.Size = new Size(398, 276);
                BSimpan.Text = "Ubah";
                labelX4.Visible = false;
                TBHasil.Visible = false;
                this.Text = "Ubah Data Rapat";

                LId.Text = DataUbah[0];
                TBRapat.Text = DataUbah[1];
                TBHasil.Text = DataUbah[2];
                DTITanggal.Text = DataUbah[3];
                TBWaktu.Text = DataUbah[4];
            }
            LStatus.Text = Status;
        }

        private bool CekKelengkapan()
        {
            ToastNotification.Close(this);
            if (TBRapat.Text == "")
            {
                ToastNotification.Show(this, "Nama rapat / Pembahasan harus diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBRapat.Focus();
                return false;
            }
            else if (TBHasil.Text == "" && Status == "Sudah Dilaksanakan")
            {
                ToastNotification.Show(this, "Hasil Pembahasan harus diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBHasil.Focus();
                return false;
            }
            else if (DTITanggal.Text.Length == 0)
            {
                ToastNotification.Show(this, "Tanggal harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                DTITanggal.Focus();
                return false;
            }
            else if (TBWaktu.Text == "")
            {
                ToastNotification.Show(this, "Waktu harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBWaktu.Focus();
                return false;
            }
            return true;
        }

        private void Form_AturPiket_Tampil(object sender, EventArgs e)
        {
            if (Status == "Belum Dilaksanakan")
            {
                TBRapat.Focus();
            }
            else
            {
                TBHasil.Focus();
            }
        }

        private void BSimpan_Click(object sender, EventArgs e)
        {
            if ((BSimpan.Text == "Ubah") == true || (BSimpan.Text == "Tentukan Hasil") == true)
            {
                if (CekKelengkapan() == false)
                {
                    return;
                }

                string nilai, nilaiC;
                nilai = "nama_rapat = '" + TBRapat.Text + "', hasil_rapat = '" + TBHasil.Text + "', tanggal = '" + MDA.AturTanggal(DTITanggal.Value) + "', waktu = '" + TBWaktu.Text + "'";
                nilaiC = "(" + LId.Text + ", " + TBRapat.Text + ", " + TBHasil.Text + ", " + MDA.AturTanggal(DTITanggal.Value) + ", " + TBWaktu.Text + ")";
                MDA.UbahData("rapat", nilai, "id_rapat = '" + LId.Text + "'");
                MDA.Catat("Ubah Data Rapat", nilaiC);
            }
            else if (BSimpan.Text == "Tambah")
            {
                if (CekKelengkapan() == false)
                {
                    return;
                }

                string nilai, nilaiC;
                nilai = "'" + LId.Text + "', '" + TBRapat.Text + "', 'Rapat Belum Dilaksanakan', '" + MDA.AturTanggal(DTITanggal.Value) + "', '" + TBWaktu.Text + "'";
                nilaiC = "(" + LId.Text + ", " + TBRapat.Text + ", " + TBHasil.Text + ", " + MDA.AturTanggal(DTITanggal.Value) + ", " + TBWaktu.Text + ")";
                MDA.TambahData("rapat", nilai);
                MDA.Catat("Tambah Data Rapat", nilaiC);
            }

            DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Rapat");
            DataGridSegarkan(this, args);

            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }
    }
}
