﻿namespace Intermedia.Form
{
    partial class Form_RiwayatAplikasi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DGRiwayat = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.id_riwayat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.keterangan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date_time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DGRiwayat)).BeginInit();
            this.SuspendLayout();
            // 
            // DGRiwayat
            // 
            this.DGRiwayat.AllowUserToAddRows = false;
            this.DGRiwayat.AllowUserToDeleteRows = false;
            this.DGRiwayat.AllowUserToResizeColumns = false;
            this.DGRiwayat.AllowUserToResizeRows = false;
            this.DGRiwayat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGRiwayat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGRiwayat.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGRiwayat.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGRiwayat.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGRiwayat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGRiwayat.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_riwayat,
            this.nama,
            this.tipe,
            this.keterangan,
            this.date_time});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGRiwayat.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGRiwayat.EnableHeadersVisualStyles = false;
            this.DGRiwayat.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGRiwayat.Location = new System.Drawing.Point(12, 12);
            this.DGRiwayat.Name = "DGRiwayat";
            this.DGRiwayat.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGRiwayat.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGRiwayat.RowHeadersVisible = false;
            this.DGRiwayat.RowHeadersWidth = 80;
            this.DGRiwayat.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGRiwayat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGRiwayat.Size = new System.Drawing.Size(810, 451);
            this.DGRiwayat.TabIndex = 13;
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // id_riwayat
            // 
            this.id_riwayat.DataPropertyName = "id_riwayat";
            this.id_riwayat.HeaderText = "ID Riwayat";
            this.id_riwayat.Name = "id_riwayat";
            this.id_riwayat.ReadOnly = true;
            this.id_riwayat.Visible = false;
            // 
            // nama
            // 
            this.nama.DataPropertyName = "nama";
            this.nama.FillWeight = 40F;
            this.nama.HeaderText = "Nama Anggota";
            this.nama.Name = "nama";
            this.nama.ReadOnly = true;
            // 
            // tipe
            // 
            this.tipe.DataPropertyName = "tipe";
            this.tipe.FillWeight = 50F;
            this.tipe.HeaderText = "Tipe";
            this.tipe.Name = "tipe";
            this.tipe.ReadOnly = true;
            // 
            // keterangan
            // 
            this.keterangan.DataPropertyName = "keterangan";
            this.keterangan.HeaderText = "Keterangan";
            this.keterangan.Name = "keterangan";
            this.keterangan.ReadOnly = true;
            // 
            // date_time
            // 
            this.date_time.DataPropertyName = "date_time";
            this.date_time.FillWeight = 40F;
            this.date_time.HeaderText = "Tanggal dan Waktu";
            this.date_time.Name = "date_time";
            this.date_time.ReadOnly = true;
            // 
            // Form_RiwayatAplikasi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 475);
            this.Controls.Add(this.DGRiwayat);
            this.DoubleBuffered = true;
            this.MinimumSize = new System.Drawing.Size(846, 509);
            this.Name = "Form_RiwayatAplikasi";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Riwayat Aplikasi";
            this.Load += new System.EventHandler(this.Form_RiwayatAplikasi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGRiwayat)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevComponents.DotNetBar.Controls.DataGridViewX DGRiwayat;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_riwayat;
        private System.Windows.Forms.DataGridViewTextBoxColumn nama;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipe;
        private System.Windows.Forms.DataGridViewTextBoxColumn keterangan;
        private System.Windows.Forms.DataGridViewTextBoxColumn date_time;
    }
}