﻿namespace Intermedia
{
    partial class Form_Utama
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.styleManagerAmbient1 = new DevComponents.DotNetBar.StyleManagerAmbient(this.components);
            this.BAbsenPiket = new DevComponents.DotNetBar.ButtonX();
            this.RMPiket = new DevComponents.DotNetBar.RadialMenu();
            this.RMITambahPiket = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIUbahPiket = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIHapusPiket = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIAbsenPiket = new DevComponents.DotNetBar.RadialMenuItem();
            this.BAbsenRapat = new DevComponents.DotNetBar.ButtonX();
            this.metroStatusBar1 = new DevComponents.DotNetBar.Metro.MetroStatusBar();
            this.LNamaP = new DevComponents.DotNetBar.LabelItem();
            this.LNim = new DevComponents.DotNetBar.LabelItem();
            this.LJabatan = new DevComponents.DotNetBar.LabelItem();
            this.LWaktuSekarang = new DevComponents.DotNetBar.LabelItem();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.LBendahara = new DevComponents.DotNetBar.LabelX();
            this.LSekretaris = new DevComponents.DotNetBar.LabelX();
            this.LWakilKetua = new DevComponents.DotNetBar.LabelX();
            this.LKetua = new DevComponents.DotNetBar.LabelX();
            this.LabelX3 = new DevComponents.DotNetBar.LabelX();
            this.LabelX4 = new DevComponents.DotNetBar.LabelX();
            this.LabelX2 = new DevComponents.DotNetBar.LabelX();
            this.LabelX1 = new DevComponents.DotNetBar.LabelX();
            this.tabControl1 = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.RMAnggota = new DevComponents.DotNetBar.RadialMenu();
            this.RMITambahA = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIUbahA = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIHapusA = new DevComponents.DotNetBar.RadialMenuItem();
            this.DGAnggota = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.NimDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NamaanggotaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KelasDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlamatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JkelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NohpDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.TSSLBanyakDataAnggota = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLNimAnggota = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSAnggota = new System.Windows.Forms.ToolStripStatusLabel();
            this.TBIAnggota = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.DGStrukturAngg = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.TSSLBanyakDataStruktur = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLNimStruktur = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSStruktur = new System.Windows.Forms.ToolStripStatusLabel();
            this.RMStruktur = new DevComponents.DotNetBar.RadialMenu();
            this.RMIUbahStruktur = new DevComponents.DotNetBar.RadialMenuItem();
            this.radialMenuItem5 = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIBagan = new DevComponents.DotNetBar.RadialMenuItem();
            this.radialMenuItem8 = new DevComponents.DotNetBar.RadialMenuItem();
            this.TBIStruktur = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel4 = new DevComponents.DotNetBar.TabControlPanel();
            this.DGRapat = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.id_rapat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nama_rapat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hasil_rapat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tanggal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.waktu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip4 = new System.Windows.Forms.StatusStrip();
            this.TSSLBanyakDataRapat = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel11 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLNamaRapat = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSRapat = new System.Windows.Forms.ToolStripStatusLabel();
            this.RMRapat = new DevComponents.DotNetBar.RadialMenu();
            this.RMITambahRapat = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIUbahRapat = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIHapusRapat = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIAbsenRapat = new DevComponents.DotNetBar.RadialMenuItem();
            this.TBIRapat = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel5 = new DevComponents.DotNetBar.TabControlPanel();
            this.DGProker = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.id_proker = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip5 = new System.Windows.Forms.StatusStrip();
            this.TSSLBanyakDataProker = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel15 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLIdProker = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSProker = new System.Windows.Forms.ToolStripStatusLabel();
            this.RMProker = new DevComponents.DotNetBar.RadialMenu();
            this.RMITambahProker = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIUbahProker = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIHapusProker = new DevComponents.DotNetBar.RadialMenuItem();
            this.TBIProker = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel6 = new DevComponents.DotNetBar.TabControlPanel();
            this.DGInventaris = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.id_barang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip6 = new System.Windows.Forms.StatusStrip();
            this.TSSLBanyakDataInvent = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel19 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLNimInvent = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSInvent = new System.Windows.Forms.ToolStripStatusLabel();
            this.RMInventaris = new DevComponents.DotNetBar.RadialMenu();
            this.RMITambahInvent = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIUbahInvent = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIHapusInvent = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIPeminjaman = new DevComponents.DotNetBar.RadialMenuItem();
            this.TBIInventaris = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel7 = new DevComponents.DotNetBar.TabControlPanel();
            this.DGKeuangan = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.id_keuangan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nama_anggota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.keterangan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nominal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip7 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLTotalDana = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel23 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLIdKeuangan = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSKeuangan = new System.Windows.Forms.ToolStripStatusLabel();
            this.RMKeuangan = new DevComponents.DotNetBar.RadialMenu();
            this.RMITambahDana = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIHapusDana = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIUbahDana = new DevComponents.DotNetBar.RadialMenuItem();
            this.radialMenuItem4 = new DevComponents.DotNetBar.RadialMenuItem();
            this.TBIKeuangan = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.DGPiket = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hari = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip3 = new System.Windows.Forms.StatusStrip();
            this.TSSLBanyakDataPiket = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel7 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSLNimPiket = new System.Windows.Forms.ToolStripStatusLabel();
            this.LDSPiket = new System.Windows.Forms.ToolStripStatusLabel();
            this.TBIPiket = new DevComponents.DotNetBar.TabItem(this.components);
            this.radialMenuItem7 = new DevComponents.DotNetBar.RadialMenuItem();
            this.radialMenuItem9 = new DevComponents.DotNetBar.RadialMenuItem();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.RLNoJadwal = new DevComponents.DotNetBar.Controls.ReflectionLabel();
            this.DGPiketNow = new System.Windows.Forms.DataGridView();
            this.ReflectionLabel1 = new DevComponents.DotNetBar.Controls.ReflectionLabel();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.RMMenu = new DevComponents.DotNetBar.RadialMenu();
            this.RMITentang = new DevComponents.DotNetBar.RadialMenuItem();
            this.radialMenuItem6 = new DevComponents.DotNetBar.RadialMenuItem();
            this.RMIRiwayat = new DevComponents.DotNetBar.RadialMenuItem();
            this.radialMenuItem11 = new DevComponents.DotNetBar.RadialMenuItem();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.LRapatSelanjutnya = new DevComponents.DotNetBar.LabelX();
            this.LAgendaSelanjutnya = new DevComponents.DotNetBar.LabelX();
            this.LBarangBaru = new DevComponents.DotNetBar.LabelX();
            this.LTotalKas = new DevComponents.DotNetBar.LabelX();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.labelX12 = new DevComponents.DotNetBar.LabelX();
            this.labelX13 = new DevComponents.DotNetBar.LabelX();
            this.labelX14 = new DevComponents.DotNetBar.LabelX();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGAnggota)).BeginInit();
            this.StatusStrip1.SuspendLayout();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGStrukturAngg)).BeginInit();
            this.statusStrip2.SuspendLayout();
            this.tabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGRapat)).BeginInit();
            this.statusStrip4.SuspendLayout();
            this.tabControlPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGProker)).BeginInit();
            this.statusStrip5.SuspendLayout();
            this.tabControlPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGInventaris)).BeginInit();
            this.statusStrip6.SuspendLayout();
            this.tabControlPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGKeuangan)).BeginInit();
            this.statusStrip7.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGPiket)).BeginInit();
            this.statusStrip3.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGPiketNow)).BeginInit();
            this.groupPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242))))), System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204))))));
            // 
            // BAbsenPiket
            // 
            this.BAbsenPiket.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BAbsenPiket.AntiAlias = true;
            this.BAbsenPiket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.styleManagerAmbient1.SetEnableAmbientSettings(this.BAbsenPiket, DevComponents.DotNetBar.eAmbientSettings.None);
            this.BAbsenPiket.EnableMarkup = false;
            this.BAbsenPiket.Location = new System.Drawing.Point(84, 8);
            this.BAbsenPiket.Name = "BAbsenPiket";
            this.BAbsenPiket.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.BAbsenPiket.Size = new System.Drawing.Size(28, 28);
            this.BAbsenPiket.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.BAbsenPiket.Symbol = "";
            this.BAbsenPiket.SymbolSize = 14F;
            this.BAbsenPiket.TabIndex = 101;
            this.BAbsenPiket.TextColor = System.Drawing.Color.AliceBlue;
            this.BAbsenPiket.Tooltip = "Absen kehadiran item terpilih.";
            this.BAbsenPiket.Click += new System.EventHandler(this.BAbsenPiket_Click);
            // 
            // RMPiket
            // 
            this.RMPiket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.styleManagerAmbient1.SetEnableAmbientSettings(this.RMPiket, DevComponents.DotNetBar.eAmbientSettings.None);
            this.RMPiket.ForeColor = System.Drawing.Color.Black;
            this.RMPiket.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMITambahPiket,
            this.RMIUbahPiket,
            this.RMIHapusPiket,
            this.RMIAbsenPiket});
            this.RMPiket.Location = new System.Drawing.Point(44, 8);
            this.RMPiket.Name = "RMPiket";
            this.RMPiket.Size = new System.Drawing.Size(28, 28);
            this.RMPiket.Symbol = "";
            this.RMPiket.SymbolSize = 14F;
            this.RMPiket.TabIndex = 13;
            this.RMPiket.Text = "RadialMenu2";
            this.RMPiket.MouseEnter += new System.EventHandler(this.Menu_MouseEnter);
            this.RMPiket.MouseLeave += new System.EventHandler(this.Menu_MouseLeave);
            // 
            // RMITambahPiket
            // 
            this.RMITambahPiket.Name = "RMITambahPiket";
            this.RMITambahPiket.Symbol = "";
            this.RMITambahPiket.Text = "Tambah";
            this.RMITambahPiket.Click += new System.EventHandler(this.RMITambahPiket_Klik);
            // 
            // RMIUbahPiket
            // 
            this.RMIUbahPiket.Name = "RMIUbahPiket";
            this.RMIUbahPiket.Symbol = "";
            this.RMIUbahPiket.Text = "Ubah";
            this.RMIUbahPiket.Click += new System.EventHandler(this.RMIUbahPiket_Klik);
            // 
            // RMIHapusPiket
            // 
            this.RMIHapusPiket.Name = "RMIHapusPiket";
            this.RMIHapusPiket.Symbol = "";
            this.RMIHapusPiket.Text = "Hapus";
            this.RMIHapusPiket.Click += new System.EventHandler(this.RMIHapusPiket_Klik);
            // 
            // RMIAbsenPiket
            // 
            this.RMIAbsenPiket.Name = "RMIAbsenPiket";
            this.RMIAbsenPiket.Symbol = "";
            this.RMIAbsenPiket.Text = "Absen";
            this.RMIAbsenPiket.Click += new System.EventHandler(this.RMIAbsenPiket_Klik);
            // 
            // BAbsenRapat
            // 
            this.BAbsenRapat.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BAbsenRapat.AntiAlias = true;
            this.BAbsenRapat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.styleManagerAmbient1.SetEnableAmbientSettings(this.BAbsenRapat, DevComponents.DotNetBar.eAmbientSettings.None);
            this.BAbsenRapat.EnableMarkup = false;
            this.BAbsenRapat.Location = new System.Drawing.Point(84, 8);
            this.BAbsenRapat.Name = "BAbsenRapat";
            this.BAbsenRapat.Shape = new DevComponents.DotNetBar.EllipticalShapeDescriptor();
            this.BAbsenRapat.Size = new System.Drawing.Size(28, 28);
            this.BAbsenRapat.Style = DevComponents.DotNetBar.eDotNetBarStyle.Windows7;
            this.BAbsenRapat.Symbol = "";
            this.BAbsenRapat.SymbolSize = 14F;
            this.BAbsenRapat.TabIndex = 102;
            this.BAbsenRapat.TextColor = System.Drawing.Color.AliceBlue;
            this.BAbsenRapat.Tooltip = "Absen kehadiran item terpilih.";
            this.BAbsenRapat.Click += new System.EventHandler(this.BAbsenRapat_Click);
            // 
            // metroStatusBar1
            // 
            this.metroStatusBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.metroStatusBar1.BackgroundStyle.BackColor = System.Drawing.Color.SteelBlue;
            this.metroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.metroStatusBar1.ContainerControlProcessDialogKey = true;
            this.metroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroStatusBar1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.metroStatusBar1.ForeColor = System.Drawing.Color.Black;
            this.metroStatusBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.LNamaP,
            this.LNim,
            this.LJabatan,
            this.LWaktuSekarang});
            this.metroStatusBar1.Location = new System.Drawing.Point(0, 538);
            this.metroStatusBar1.Name = "metroStatusBar1";
            this.metroStatusBar1.Size = new System.Drawing.Size(913, 22);
            this.metroStatusBar1.TabIndex = 1;
            this.metroStatusBar1.Text = "metroStatusBar1";
            // 
            // LNamaP
            // 
            this.LNamaP.ImageTextSpacing = 5;
            this.LNamaP.Name = "LNamaP";
            this.LNamaP.Stretch = true;
            this.LNamaP.Symbol = "";
            this.LNamaP.SymbolSize = 13F;
            this.LNamaP.Text = "Nama_Pengguna";
            this.LNamaP.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // LNim
            // 
            this.LNim.ImageTextSpacing = 5;
            this.LNim.Name = "LNim";
            this.LNim.Stretch = true;
            this.LNim.Symbol = "";
            this.LNim.SymbolSize = 13F;
            this.LNim.Text = "00.00.0000";
            this.LNim.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // LJabatan
            // 
            this.LJabatan.ImageTextSpacing = 5;
            this.LJabatan.Name = "LJabatan";
            this.LJabatan.Stretch = true;
            this.LJabatan.Symbol = "";
            this.LJabatan.SymbolSize = 13F;
            this.LJabatan.Text = "Jabatan";
            this.LJabatan.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // LWaktuSekarang
            // 
            this.LWaktuSekarang.ImageTextSpacing = 5;
            this.LWaktuSekarang.Name = "LWaktuSekarang";
            this.LWaktuSekarang.Stretch = true;
            this.LWaktuSekarang.Symbol = "";
            this.LWaktuSekarang.SymbolSize = 13F;
            this.LWaktuSekarang.Text = "Hari, 00 Bulan 0000";
            this.LWaktuSekarang.TextLineAlignment = System.Drawing.StringAlignment.Near;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.groupPanel1.Controls.Add(this.LBendahara);
            this.groupPanel1.Controls.Add(this.LSekretaris);
            this.groupPanel1.Controls.Add(this.LWakilKetua);
            this.groupPanel1.Controls.Add(this.LKetua);
            this.groupPanel1.Controls.Add(this.LabelX3);
            this.groupPanel1.Controls.Add(this.LabelX4);
            this.groupPanel1.Controls.Add(this.LabelX2);
            this.groupPanel1.Controls.Add(this.LabelX1);
            this.groupPanel1.Location = new System.Drawing.Point(12, 12);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(300, 167);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 2;
            this.groupPanel1.Text = "Struktur Anggota";
            // 
            // LBendahara
            // 
            this.LBendahara.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LBendahara.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LBendahara.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LBendahara.ForeColor = System.Drawing.Color.Black;
            this.LBendahara.Location = new System.Drawing.Point(136, 114);
            this.LBendahara.Name = "LBendahara";
            this.LBendahara.Size = new System.Drawing.Size(147, 23);
            this.LBendahara.TabIndex = 15;
            this.LBendahara.Text = "Belum ditetapkan";
            // 
            // LSekretaris
            // 
            this.LSekretaris.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LSekretaris.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LSekretaris.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LSekretaris.ForeColor = System.Drawing.Color.Black;
            this.LSekretaris.Location = new System.Drawing.Point(136, 78);
            this.LSekretaris.Name = "LSekretaris";
            this.LSekretaris.Size = new System.Drawing.Size(147, 23);
            this.LSekretaris.TabIndex = 14;
            this.LSekretaris.Text = "Belum ditetapkan";
            // 
            // LWakilKetua
            // 
            this.LWakilKetua.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LWakilKetua.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LWakilKetua.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LWakilKetua.ForeColor = System.Drawing.Color.Black;
            this.LWakilKetua.Location = new System.Drawing.Point(136, 42);
            this.LWakilKetua.Name = "LWakilKetua";
            this.LWakilKetua.Size = new System.Drawing.Size(147, 23);
            this.LWakilKetua.TabIndex = 13;
            this.LWakilKetua.Text = "Belum ditetapkan";
            // 
            // LKetua
            // 
            this.LKetua.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LKetua.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LKetua.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LKetua.ForeColor = System.Drawing.Color.Black;
            this.LKetua.Location = new System.Drawing.Point(136, 6);
            this.LKetua.Name = "LKetua";
            this.LKetua.Size = new System.Drawing.Size(147, 23);
            this.LKetua.TabIndex = 12;
            this.LKetua.Text = "Belum ditetapkan";
            // 
            // LabelX3
            // 
            this.LabelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.LabelX3.ForeColor = System.Drawing.Color.Black;
            this.LabelX3.ImageTextSpacing = 5;
            this.LabelX3.Location = new System.Drawing.Point(10, 114);
            this.LabelX3.Name = "LabelX3";
            this.LabelX3.Size = new System.Drawing.Size(122, 23);
            this.LabelX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.LabelX3.Symbol = "";
            this.LabelX3.SymbolColor = System.Drawing.Color.SteelBlue;
            this.LabelX3.SymbolSize = 18F;
            this.LabelX3.TabIndex = 11;
            this.LabelX3.Text = "Bendahara";
            // 
            // LabelX4
            // 
            this.LabelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.LabelX4.ForeColor = System.Drawing.Color.Black;
            this.LabelX4.ImageTextSpacing = 5;
            this.LabelX4.Location = new System.Drawing.Point(10, 78);
            this.LabelX4.Name = "LabelX4";
            this.LabelX4.Size = new System.Drawing.Size(122, 23);
            this.LabelX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.LabelX4.Symbol = "";
            this.LabelX4.SymbolColor = System.Drawing.Color.SteelBlue;
            this.LabelX4.SymbolSize = 18F;
            this.LabelX4.TabIndex = 10;
            this.LabelX4.Text = "Sekretaris";
            // 
            // LabelX2
            // 
            this.LabelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.LabelX2.ForeColor = System.Drawing.Color.Black;
            this.LabelX2.ImageTextSpacing = 5;
            this.LabelX2.Location = new System.Drawing.Point(10, 42);
            this.LabelX2.Name = "LabelX2";
            this.LabelX2.Size = new System.Drawing.Size(122, 23);
            this.LabelX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.LabelX2.Symbol = "";
            this.LabelX2.SymbolColor = System.Drawing.Color.SteelBlue;
            this.LabelX2.SymbolSize = 18F;
            this.LabelX2.TabIndex = 9;
            this.LabelX2.Text = "Wakil Ketua";
            // 
            // LabelX1
            // 
            this.LabelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.LabelX1.ForeColor = System.Drawing.Color.Black;
            this.LabelX1.ImageTextSpacing = 5;
            this.LabelX1.Location = new System.Drawing.Point(10, 6);
            this.LabelX1.Name = "LabelX1";
            this.LabelX1.Size = new System.Drawing.Size(122, 23);
            this.LabelX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.LabelX1.Symbol = "";
            this.LabelX1.SymbolColor = System.Drawing.Color.SteelBlue;
            this.LabelX1.SymbolSize = 18F;
            this.LabelX1.TabIndex = 8;
            this.LabelX1.Text = "Ketua";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.AntiAlias = true;
            this.tabControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControl1.CanReorderTabs = true;
            this.tabControl1.Controls.Add(this.tabControlPanel1);
            this.tabControl1.Controls.Add(this.tabControlPanel2);
            this.tabControl1.Controls.Add(this.tabControlPanel4);
            this.tabControl1.Controls.Add(this.tabControlPanel5);
            this.tabControl1.Controls.Add(this.tabControlPanel6);
            this.tabControl1.Controls.Add(this.tabControlPanel7);
            this.tabControl1.Controls.Add(this.tabControlPanel3);
            this.tabControl1.FixedTabSize = new System.Drawing.Size(140, 0);
            this.tabControl1.ForeColor = System.Drawing.Color.Black;
            this.tabControl1.Location = new System.Drawing.Point(12, 185);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabControl1.SelectedTabIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(889, 347);
            this.tabControl1.Style = DevComponents.DotNetBar.eTabStripStyle.Metro;
            this.tabControl1.TabIndex = 4;
            this.tabControl1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabControl1.Tabs.Add(this.TBIAnggota);
            this.tabControl1.Tabs.Add(this.TBIStruktur);
            this.tabControl1.Tabs.Add(this.TBIPiket);
            this.tabControl1.Tabs.Add(this.TBIRapat);
            this.tabControl1.Tabs.Add(this.TBIProker);
            this.tabControl1.Tabs.Add(this.TBIInventaris);
            this.tabControl1.Tabs.Add(this.TBIKeuangan);
            this.tabControl1.Text = "tabControl1";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.RMAnggota);
            this.tabControlPanel1.Controls.Add(this.DGAnggota);
            this.tabControlPanel1.Controls.Add(this.StatusStrip1);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(889, 320);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.TBIAnggota;
            // 
            // RMAnggota
            // 
            this.RMAnggota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.RMAnggota.ForeColor = System.Drawing.Color.Black;
            this.RMAnggota.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMITambahA,
            this.RMIUbahA,
            this.RMIHapusA});
            this.RMAnggota.Location = new System.Drawing.Point(44, 8);
            this.RMAnggota.Name = "RMAnggota";
            this.RMAnggota.Size = new System.Drawing.Size(28, 28);
            this.RMAnggota.Symbol = "";
            this.RMAnggota.SymbolSize = 14F;
            this.RMAnggota.TabIndex = 14;
            this.RMAnggota.Text = "RadialMenu2";
            this.RMAnggota.MouseEnter += new System.EventHandler(this.Menu_MouseEnter);
            this.RMAnggota.MouseLeave += new System.EventHandler(this.Menu_MouseLeave);
            // 
            // RMITambahA
            // 
            this.RMITambahA.Name = "RMITambahA";
            this.RMITambahA.Symbol = "";
            this.RMITambahA.Text = "Tambah";
            this.RMITambahA.Click += new System.EventHandler(this.RMITambahAnggota_Klik);
            // 
            // RMIUbahA
            // 
            this.RMIUbahA.Name = "RMIUbahA";
            this.RMIUbahA.Symbol = "";
            this.RMIUbahA.Text = "Ubah";
            this.RMIUbahA.Click += new System.EventHandler(this.RMIUbahAnggota_Klik);
            // 
            // RMIHapusA
            // 
            this.RMIHapusA.Name = "RMIHapusA";
            this.RMIHapusA.Symbol = "";
            this.RMIHapusA.Text = "Hapus";
            this.RMIHapusA.Click += new System.EventHandler(this.RMIHapusAnggota_Klik);
            // 
            // DGAnggota
            // 
            this.DGAnggota.AllowUserToAddRows = false;
            this.DGAnggota.AllowUserToDeleteRows = false;
            this.DGAnggota.AllowUserToResizeColumns = false;
            this.DGAnggota.AllowUserToResizeRows = false;
            this.DGAnggota.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGAnggota.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGAnggota.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGAnggota.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGAnggota.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGAnggota.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGAnggota.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NimDataGridViewTextBoxColumn,
            this.NamaanggotaDataGridViewTextBoxColumn,
            this.KelasDataGridViewTextBoxColumn,
            this.AlamatDataGridViewTextBoxColumn,
            this.JkelDataGridViewTextBoxColumn,
            this.NohpDataGridViewTextBoxColumn});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGAnggota.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGAnggota.EnableHeadersVisualStyles = false;
            this.DGAnggota.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGAnggota.Location = new System.Drawing.Point(1, 42);
            this.DGAnggota.Name = "DGAnggota";
            this.DGAnggota.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGAnggota.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DGAnggota.RowHeadersVisible = false;
            this.DGAnggota.RowHeadersWidth = 80;
            this.DGAnggota.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGAnggota.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGAnggota.Size = new System.Drawing.Size(887, 255);
            this.DGAnggota.TabIndex = 12;
            this.DGAnggota.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGAnggota_CellContentClick);
            // 
            // NimDataGridViewTextBoxColumn
            // 
            this.NimDataGridViewTextBoxColumn.DataPropertyName = "nim";
            this.NimDataGridViewTextBoxColumn.FillWeight = 40F;
            this.NimDataGridViewTextBoxColumn.HeaderText = "Nim";
            this.NimDataGridViewTextBoxColumn.Name = "NimDataGridViewTextBoxColumn";
            this.NimDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // NamaanggotaDataGridViewTextBoxColumn
            // 
            this.NamaanggotaDataGridViewTextBoxColumn.DataPropertyName = "nama_anggota";
            this.NamaanggotaDataGridViewTextBoxColumn.HeaderText = "Nama Anggota";
            this.NamaanggotaDataGridViewTextBoxColumn.Name = "NamaanggotaDataGridViewTextBoxColumn";
            this.NamaanggotaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // KelasDataGridViewTextBoxColumn
            // 
            this.KelasDataGridViewTextBoxColumn.DataPropertyName = "kelas";
            this.KelasDataGridViewTextBoxColumn.FillWeight = 40F;
            this.KelasDataGridViewTextBoxColumn.HeaderText = "Kelas";
            this.KelasDataGridViewTextBoxColumn.Name = "KelasDataGridViewTextBoxColumn";
            this.KelasDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // AlamatDataGridViewTextBoxColumn
            // 
            this.AlamatDataGridViewTextBoxColumn.DataPropertyName = "alamat";
            this.AlamatDataGridViewTextBoxColumn.HeaderText = "Alamat";
            this.AlamatDataGridViewTextBoxColumn.Name = "AlamatDataGridViewTextBoxColumn";
            this.AlamatDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // JkelDataGridViewTextBoxColumn
            // 
            this.JkelDataGridViewTextBoxColumn.DataPropertyName = "j_kel";
            this.JkelDataGridViewTextBoxColumn.FillWeight = 30F;
            this.JkelDataGridViewTextBoxColumn.HeaderText = "Jenis Kelamin";
            this.JkelDataGridViewTextBoxColumn.Name = "JkelDataGridViewTextBoxColumn";
            this.JkelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // NohpDataGridViewTextBoxColumn
            // 
            this.NohpDataGridViewTextBoxColumn.DataPropertyName = "no_hp";
            this.NohpDataGridViewTextBoxColumn.FillWeight = 60F;
            this.NohpDataGridViewTextBoxColumn.HeaderText = "No. Handphone";
            this.NohpDataGridViewTextBoxColumn.Name = "NohpDataGridViewTextBoxColumn";
            this.NohpDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.StatusStrip1.ForeColor = System.Drawing.Color.Black;
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSSLBanyakDataAnggota,
            this.ToolStripStatusLabel1,
            this.TSSLNimAnggota,
            this.LDSAnggota});
            this.StatusStrip1.Location = new System.Drawing.Point(1, 297);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.StatusStrip1.Size = new System.Drawing.Size(887, 22);
            this.StatusStrip1.SizingGrip = false;
            this.StatusStrip1.TabIndex = 11;
            this.StatusStrip1.Text = "SSAnggota";
            // 
            // TSSLBanyakDataAnggota
            // 
            this.TSSLBanyakDataAnggota.Name = "TSSLBanyakDataAnggota";
            this.TSSLBanyakDataAnggota.Size = new System.Drawing.Size(0, 17);
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(19, 17);
            this.ToolStripStatusLabel1.Text = " | ";
            // 
            // TSSLNimAnggota
            // 
            this.TSSLNimAnggota.Name = "TSSLNimAnggota";
            this.TSSLNimAnggota.Size = new System.Drawing.Size(0, 17);
            // 
            // LDSAnggota
            // 
            this.LDSAnggota.Name = "LDSAnggota";
            this.LDSAnggota.Size = new System.Drawing.Size(853, 17);
            this.LDSAnggota.Spring = true;
            // 
            // TBIAnggota
            // 
            this.TBIAnggota.AttachedControl = this.tabControlPanel1;
            this.TBIAnggota.Name = "TBIAnggota";
            this.TBIAnggota.Text = "Anggota";
            this.TBIAnggota.Click += new System.EventHandler(this.Tab_Klik);
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.DGStrukturAngg);
            this.tabControlPanel2.Controls.Add(this.statusStrip2);
            this.tabControlPanel2.Controls.Add(this.RMStruktur);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(889, 320);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.TBIStruktur;
            // 
            // DGStrukturAngg
            // 
            this.DGStrukturAngg.AllowUserToAddRows = false;
            this.DGStrukturAngg.AllowUserToDeleteRows = false;
            this.DGStrukturAngg.AllowUserToResizeColumns = false;
            this.DGStrukturAngg.AllowUserToResizeRows = false;
            this.DGStrukturAngg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGStrukturAngg.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGStrukturAngg.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGStrukturAngg.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGStrukturAngg.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DGStrukturAngg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGStrukturAngg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGStrukturAngg.DefaultCellStyle = dataGridViewCellStyle5;
            this.DGStrukturAngg.EnableHeadersVisualStyles = false;
            this.DGStrukturAngg.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGStrukturAngg.Location = new System.Drawing.Point(1, 42);
            this.DGStrukturAngg.MultiSelect = false;
            this.DGStrukturAngg.Name = "DGStrukturAngg";
            this.DGStrukturAngg.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGStrukturAngg.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DGStrukturAngg.RowHeadersVisible = false;
            this.DGStrukturAngg.RowHeadersWidth = 80;
            this.DGStrukturAngg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGStrukturAngg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGStrukturAngg.Size = new System.Drawing.Size(887, 255);
            this.DGStrukturAngg.TabIndex = 15;
            this.DGStrukturAngg.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGStrukturAngg_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "nim";
            this.dataGridViewTextBoxColumn1.FillWeight = 30F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Nim";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "nama_anggota";
            this.dataGridViewTextBoxColumn2.FillWeight = 60F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Nama Anggota";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "kelas";
            this.dataGridViewTextBoxColumn3.FillWeight = 30F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Kelas";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "no_hp";
            this.dataGridViewTextBoxColumn6.FillWeight = 30F;
            this.dataGridViewTextBoxColumn6.HeaderText = "No. Handphone";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "nama_jabatan";
            this.dataGridViewTextBoxColumn4.FillWeight = 50F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Jabatan";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "jabatan_mulai";
            this.dataGridViewTextBoxColumn5.FillWeight = 45F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Berstatus Sejak";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // statusStrip2
            // 
            this.statusStrip2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.statusStrip2.ForeColor = System.Drawing.Color.Black;
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSSLBanyakDataStruktur,
            this.toolStripStatusLabel3,
            this.TSSLNimStruktur,
            this.LDSStruktur});
            this.statusStrip2.Location = new System.Drawing.Point(1, 297);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip2.Size = new System.Drawing.Size(887, 22);
            this.statusStrip2.SizingGrip = false;
            this.statusStrip2.TabIndex = 14;
            this.statusStrip2.Text = "SSAnggota";
            // 
            // TSSLBanyakDataStruktur
            // 
            this.TSSLBanyakDataStruktur.Name = "TSSLBanyakDataStruktur";
            this.TSSLBanyakDataStruktur.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel3.Text = " | ";
            // 
            // TSSLNimStruktur
            // 
            this.TSSLNimStruktur.Name = "TSSLNimStruktur";
            this.TSSLNimStruktur.Size = new System.Drawing.Size(10, 17);
            this.TSSLNimStruktur.Text = " ";
            // 
            // LDSStruktur
            // 
            this.LDSStruktur.Name = "LDSStruktur";
            this.LDSStruktur.Size = new System.Drawing.Size(843, 17);
            this.LDSStruktur.Spring = true;
            // 
            // RMStruktur
            // 
            this.RMStruktur.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.RMStruktur.ForeColor = System.Drawing.Color.Black;
            this.RMStruktur.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMIUbahStruktur,
            this.radialMenuItem5,
            this.RMIBagan,
            this.radialMenuItem8});
            this.RMStruktur.Location = new System.Drawing.Point(44, 8);
            this.RMStruktur.Name = "RMStruktur";
            this.RMStruktur.Size = new System.Drawing.Size(28, 28);
            this.RMStruktur.Symbol = "";
            this.RMStruktur.SymbolSize = 14F;
            this.RMStruktur.TabIndex = 13;
            this.RMStruktur.Text = "RadialMenu2";
            this.RMStruktur.MouseEnter += new System.EventHandler(this.Menu_MouseEnter);
            this.RMStruktur.MouseLeave += new System.EventHandler(this.Menu_MouseLeave);
            // 
            // RMIUbahStruktur
            // 
            this.RMIUbahStruktur.Name = "RMIUbahStruktur";
            this.RMIUbahStruktur.Symbol = "";
            this.RMIUbahStruktur.Text = "Ubah";
            this.RMIUbahStruktur.Click += new System.EventHandler(this.RMIUbahStruktur_Klik);
            // 
            // radialMenuItem5
            // 
            this.radialMenuItem5.AutoCollapseOnClick = false;
            this.radialMenuItem5.Name = "radialMenuItem5";
            this.radialMenuItem5.TextVisible = false;
            this.radialMenuItem5.TracksMouse = false;
            // 
            // RMIBagan
            // 
            this.RMIBagan.AutoCollapseOnClick = false;
            this.RMIBagan.Name = "RMIBagan";
            this.RMIBagan.TracksMouse = false;
            this.RMIBagan.Visible = false;
            // 
            // radialMenuItem8
            // 
            this.radialMenuItem8.AutoCollapseOnClick = false;
            this.radialMenuItem8.Name = "radialMenuItem8";
            // 
            // TBIStruktur
            // 
            this.TBIStruktur.AttachedControl = this.tabControlPanel2;
            this.TBIStruktur.Name = "TBIStruktur";
            this.TBIStruktur.Text = "Struktur Anggota";
            this.TBIStruktur.Click += new System.EventHandler(this.Tab_Klik);
            // 
            // tabControlPanel4
            // 
            this.tabControlPanel4.Controls.Add(this.BAbsenRapat);
            this.tabControlPanel4.Controls.Add(this.DGRapat);
            this.tabControlPanel4.Controls.Add(this.statusStrip4);
            this.tabControlPanel4.Controls.Add(this.RMRapat);
            this.tabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel4.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel4.Name = "tabControlPanel4";
            this.tabControlPanel4.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel4.Size = new System.Drawing.Size(889, 320);
            this.tabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel4.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel4.Style.GradientAngle = 90;
            this.tabControlPanel4.TabIndex = 4;
            this.tabControlPanel4.TabItem = this.TBIRapat;
            // 
            // DGRapat
            // 
            this.DGRapat.AllowUserToAddRows = false;
            this.DGRapat.AllowUserToDeleteRows = false;
            this.DGRapat.AllowUserToResizeColumns = false;
            this.DGRapat.AllowUserToResizeRows = false;
            this.DGRapat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGRapat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGRapat.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGRapat.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGRapat.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.DGRapat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGRapat.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_rapat,
            this.nama_rapat,
            this.hasil_rapat,
            this.tanggal,
            this.waktu});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGRapat.DefaultCellStyle = dataGridViewCellStyle8;
            this.DGRapat.EnableHeadersVisualStyles = false;
            this.DGRapat.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGRapat.Location = new System.Drawing.Point(1, 42);
            this.DGRapat.Name = "DGRapat";
            this.DGRapat.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGRapat.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.DGRapat.RowHeadersVisible = false;
            this.DGRapat.RowHeadersWidth = 80;
            this.DGRapat.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGRapat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGRapat.Size = new System.Drawing.Size(887, 255);
            this.DGRapat.TabIndex = 18;
            this.DGRapat.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGRapat_CellKlik);
            // 
            // id_rapat
            // 
            this.id_rapat.DataPropertyName = "id_rapat";
            this.id_rapat.FillWeight = 50F;
            this.id_rapat.HeaderText = "ID Rapat";
            this.id_rapat.Name = "id_rapat";
            this.id_rapat.ReadOnly = true;
            this.id_rapat.Visible = false;
            // 
            // nama_rapat
            // 
            this.nama_rapat.DataPropertyName = "nama_rapat";
            this.nama_rapat.FillWeight = 60F;
            this.nama_rapat.HeaderText = "Nama Rapat (Tema)";
            this.nama_rapat.Name = "nama_rapat";
            this.nama_rapat.ReadOnly = true;
            // 
            // hasil_rapat
            // 
            this.hasil_rapat.DataPropertyName = "hasil_rapat";
            this.hasil_rapat.HeaderText = "Hasil Rapat / Pembahasan";
            this.hasil_rapat.Name = "hasil_rapat";
            this.hasil_rapat.ReadOnly = true;
            // 
            // tanggal
            // 
            this.tanggal.DataPropertyName = "tanggal";
            this.tanggal.FillWeight = 40F;
            this.tanggal.HeaderText = "Tanggal";
            this.tanggal.Name = "tanggal";
            this.tanggal.ReadOnly = true;
            // 
            // waktu
            // 
            this.waktu.DataPropertyName = "waktu";
            this.waktu.FillWeight = 30F;
            this.waktu.HeaderText = "Waktu";
            this.waktu.Name = "waktu";
            this.waktu.ReadOnly = true;
            // 
            // statusStrip4
            // 
            this.statusStrip4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.statusStrip4.ForeColor = System.Drawing.Color.Black;
            this.statusStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSSLBanyakDataRapat,
            this.toolStripStatusLabel11,
            this.TSSLNamaRapat,
            this.LDSRapat});
            this.statusStrip4.Location = new System.Drawing.Point(1, 297);
            this.statusStrip4.Name = "statusStrip4";
            this.statusStrip4.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip4.Size = new System.Drawing.Size(887, 22);
            this.statusStrip4.SizingGrip = false;
            this.statusStrip4.TabIndex = 17;
            this.statusStrip4.Text = "SSAnggota";
            // 
            // TSSLBanyakDataRapat
            // 
            this.TSSLBanyakDataRapat.Name = "TSSLBanyakDataRapat";
            this.TSSLBanyakDataRapat.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel11.Text = " | ";
            // 
            // TSSLNamaRapat
            // 
            this.TSSLNamaRapat.Name = "TSSLNamaRapat";
            this.TSSLNamaRapat.Size = new System.Drawing.Size(10, 17);
            this.TSSLNamaRapat.Text = " ";
            // 
            // LDSRapat
            // 
            this.LDSRapat.AutoSize = false;
            this.LDSRapat.Name = "LDSRapat";
            this.LDSRapat.Size = new System.Drawing.Size(843, 17);
            this.LDSRapat.Spring = true;
            // 
            // RMRapat
            // 
            this.RMRapat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.RMRapat.ForeColor = System.Drawing.Color.Black;
            this.RMRapat.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMITambahRapat,
            this.RMIUbahRapat,
            this.RMIHapusRapat,
            this.RMIAbsenRapat});
            this.RMRapat.Location = new System.Drawing.Point(44, 8);
            this.RMRapat.Name = "RMRapat";
            this.RMRapat.Size = new System.Drawing.Size(28, 28);
            this.RMRapat.Symbol = "";
            this.RMRapat.SymbolSize = 14F;
            this.RMRapat.TabIndex = 16;
            this.RMRapat.Text = "RadialMenu2";
            this.RMRapat.MouseEnter += new System.EventHandler(this.Menu_MouseEnter);
            this.RMRapat.MouseLeave += new System.EventHandler(this.Menu_MouseLeave);
            // 
            // RMITambahRapat
            // 
            this.RMITambahRapat.Name = "RMITambahRapat";
            this.RMITambahRapat.Symbol = "";
            this.RMITambahRapat.Text = "Tambah";
            this.RMITambahRapat.Click += new System.EventHandler(this.RMITambahRapat_Klik);
            // 
            // RMIUbahRapat
            // 
            this.RMIUbahRapat.Name = "RMIUbahRapat";
            this.RMIUbahRapat.Symbol = "";
            this.RMIUbahRapat.Text = "Ubah";
            this.RMIUbahRapat.Click += new System.EventHandler(this.RMIUbahRapat_Klik);
            // 
            // RMIHapusRapat
            // 
            this.RMIHapusRapat.Name = "RMIHapusRapat";
            this.RMIHapusRapat.Symbol = "";
            this.RMIHapusRapat.Text = "Hapus";
            this.RMIHapusRapat.Click += new System.EventHandler(this.RMIHapusRapat_Klik);
            // 
            // RMIAbsenRapat
            // 
            this.RMIAbsenRapat.Name = "RMIAbsenRapat";
            this.RMIAbsenRapat.Symbol = "";
            this.RMIAbsenRapat.Text = "Absen";
            this.RMIAbsenRapat.Click += new System.EventHandler(this.RMIAbsenRapat_Klik);
            // 
            // TBIRapat
            // 
            this.TBIRapat.AttachedControl = this.tabControlPanel4;
            this.TBIRapat.Name = "TBIRapat";
            this.TBIRapat.Text = "Rapat";
            this.TBIRapat.Click += new System.EventHandler(this.Tab_Klik);
            // 
            // tabControlPanel5
            // 
            this.tabControlPanel5.Controls.Add(this.DGProker);
            this.tabControlPanel5.Controls.Add(this.statusStrip5);
            this.tabControlPanel5.Controls.Add(this.RMProker);
            this.tabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel5.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel5.Name = "tabControlPanel5";
            this.tabControlPanel5.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel5.Size = new System.Drawing.Size(889, 320);
            this.tabControlPanel5.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel5.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel5.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel5.Style.GradientAngle = 90;
            this.tabControlPanel5.TabIndex = 5;
            this.tabControlPanel5.TabItem = this.TBIProker;
            // 
            // DGProker
            // 
            this.DGProker.AllowUserToAddRows = false;
            this.DGProker.AllowUserToDeleteRows = false;
            this.DGProker.AllowUserToResizeColumns = false;
            this.DGProker.AllowUserToResizeRows = false;
            this.DGProker.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGProker.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGProker.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGProker.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGProker.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.DGProker.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGProker.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_proker,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGProker.DefaultCellStyle = dataGridViewCellStyle11;
            this.DGProker.EnableHeadersVisualStyles = false;
            this.DGProker.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGProker.Location = new System.Drawing.Point(1, 42);
            this.DGProker.Name = "DGProker";
            this.DGProker.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGProker.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.DGProker.RowHeadersVisible = false;
            this.DGProker.RowHeadersWidth = 80;
            this.DGProker.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGProker.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGProker.Size = new System.Drawing.Size(887, 255);
            this.DGProker.TabIndex = 18;
            this.DGProker.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGProker_CellKlik);
            // 
            // id_proker
            // 
            this.id_proker.DataPropertyName = "id_proker";
            this.id_proker.HeaderText = "ID Proker";
            this.id_proker.Name = "id_proker";
            this.id_proker.ReadOnly = true;
            this.id_proker.Visible = false;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "nama";
            this.dataGridViewTextBoxColumn16.FillWeight = 70F;
            this.dataGridViewTextBoxColumn16.HeaderText = "Nama";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "tanggal";
            this.dataGridViewTextBoxColumn17.FillWeight = 40F;
            this.dataGridViewTextBoxColumn17.HeaderText = "Tanggal";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "dana";
            this.dataGridViewTextBoxColumn18.FillWeight = 40F;
            this.dataGridViewTextBoxColumn18.HeaderText = "Total Dana";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "keterangan";
            this.dataGridViewTextBoxColumn19.FillWeight = 80F;
            this.dataGridViewTextBoxColumn19.HeaderText = "Keterangan";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // statusStrip5
            // 
            this.statusStrip5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.statusStrip5.ForeColor = System.Drawing.Color.Black;
            this.statusStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSSLBanyakDataProker,
            this.toolStripStatusLabel15,
            this.TSSLIdProker,
            this.LDSProker});
            this.statusStrip5.Location = new System.Drawing.Point(1, 297);
            this.statusStrip5.Name = "statusStrip5";
            this.statusStrip5.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip5.Size = new System.Drawing.Size(887, 22);
            this.statusStrip5.SizingGrip = false;
            this.statusStrip5.TabIndex = 17;
            this.statusStrip5.Text = "SSAnggota";
            // 
            // TSSLBanyakDataProker
            // 
            this.TSSLBanyakDataProker.Name = "TSSLBanyakDataProker";
            this.TSSLBanyakDataProker.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel15
            // 
            this.toolStripStatusLabel15.Name = "toolStripStatusLabel15";
            this.toolStripStatusLabel15.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel15.Text = " | ";
            // 
            // TSSLIdProker
            // 
            this.TSSLIdProker.Name = "TSSLIdProker";
            this.TSSLIdProker.Size = new System.Drawing.Size(10, 17);
            this.TSSLIdProker.Text = " ";
            // 
            // LDSProker
            // 
            this.LDSProker.AutoSize = false;
            this.LDSProker.Name = "LDSProker";
            this.LDSProker.Size = new System.Drawing.Size(843, 17);
            this.LDSProker.Spring = true;
            // 
            // RMProker
            // 
            this.RMProker.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.RMProker.ForeColor = System.Drawing.Color.Black;
            this.RMProker.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMITambahProker,
            this.RMIUbahProker,
            this.RMIHapusProker});
            this.RMProker.Location = new System.Drawing.Point(44, 8);
            this.RMProker.Name = "RMProker";
            this.RMProker.Size = new System.Drawing.Size(28, 28);
            this.RMProker.Symbol = "";
            this.RMProker.SymbolSize = 14F;
            this.RMProker.TabIndex = 16;
            this.RMProker.Text = "RadialMenu2";
            this.RMProker.MouseEnter += new System.EventHandler(this.Menu_MouseEnter);
            this.RMProker.MouseLeave += new System.EventHandler(this.Menu_MouseLeave);
            // 
            // RMITambahProker
            // 
            this.RMITambahProker.Name = "RMITambahProker";
            this.RMITambahProker.Symbol = "";
            this.RMITambahProker.Text = "Tambah";
            this.RMITambahProker.Click += new System.EventHandler(this.RMITambahProker_Klik);
            // 
            // RMIUbahProker
            // 
            this.RMIUbahProker.Name = "RMIUbahProker";
            this.RMIUbahProker.Symbol = "";
            this.RMIUbahProker.Text = "Ubah";
            this.RMIUbahProker.Click += new System.EventHandler(this.RMIUbahProker_Klik);
            // 
            // RMIHapusProker
            // 
            this.RMIHapusProker.Name = "RMIHapusProker";
            this.RMIHapusProker.Symbol = "";
            this.RMIHapusProker.Text = "Hapus";
            this.RMIHapusProker.Click += new System.EventHandler(this.RMIHapusProker_Klik);
            // 
            // TBIProker
            // 
            this.TBIProker.AttachedControl = this.tabControlPanel5;
            this.TBIProker.Name = "TBIProker";
            this.TBIProker.Text = "Proker";
            this.TBIProker.Click += new System.EventHandler(this.Tab_Klik);
            // 
            // tabControlPanel6
            // 
            this.tabControlPanel6.Controls.Add(this.DGInventaris);
            this.tabControlPanel6.Controls.Add(this.statusStrip6);
            this.tabControlPanel6.Controls.Add(this.RMInventaris);
            this.tabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel6.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel6.Name = "tabControlPanel6";
            this.tabControlPanel6.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel6.Size = new System.Drawing.Size(889, 320);
            this.tabControlPanel6.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel6.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel6.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel6.Style.GradientAngle = 90;
            this.tabControlPanel6.TabIndex = 6;
            this.tabControlPanel6.TabItem = this.TBIInventaris;
            // 
            // DGInventaris
            // 
            this.DGInventaris.AllowUserToAddRows = false;
            this.DGInventaris.AllowUserToDeleteRows = false;
            this.DGInventaris.AllowUserToResizeColumns = false;
            this.DGInventaris.AllowUserToResizeRows = false;
            this.DGInventaris.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGInventaris.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGInventaris.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGInventaris.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGInventaris.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.DGInventaris.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGInventaris.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_barang,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn25});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGInventaris.DefaultCellStyle = dataGridViewCellStyle14;
            this.DGInventaris.EnableHeadersVisualStyles = false;
            this.DGInventaris.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGInventaris.Location = new System.Drawing.Point(1, 42);
            this.DGInventaris.Name = "DGInventaris";
            this.DGInventaris.ReadOnly = true;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGInventaris.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.DGInventaris.RowHeadersVisible = false;
            this.DGInventaris.RowHeadersWidth = 80;
            this.DGInventaris.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGInventaris.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGInventaris.Size = new System.Drawing.Size(887, 255);
            this.DGInventaris.TabIndex = 18;
            this.DGInventaris.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGInventaris_CellKlik);
            // 
            // id_barang
            // 
            this.id_barang.DataPropertyName = "id_barang";
            this.id_barang.HeaderText = "ID Barang";
            this.id_barang.Name = "id_barang";
            this.id_barang.ReadOnly = true;
            this.id_barang.Visible = false;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "nim";
            this.dataGridViewTextBoxColumn21.FillWeight = 30F;
            this.dataGridViewTextBoxColumn21.HeaderText = "Nim";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "nama_anggota";
            this.dataGridViewTextBoxColumn22.HeaderText = "Nama Anggota";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "nama_barang";
            this.dataGridViewTextBoxColumn25.HeaderText = "Nama Barang";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            // 
            // statusStrip6
            // 
            this.statusStrip6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.statusStrip6.ForeColor = System.Drawing.Color.Black;
            this.statusStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSSLBanyakDataInvent,
            this.toolStripStatusLabel19,
            this.TSSLNimInvent,
            this.LDSInvent});
            this.statusStrip6.Location = new System.Drawing.Point(1, 297);
            this.statusStrip6.Name = "statusStrip6";
            this.statusStrip6.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip6.Size = new System.Drawing.Size(887, 22);
            this.statusStrip6.SizingGrip = false;
            this.statusStrip6.TabIndex = 17;
            this.statusStrip6.Text = "SSAnggota";
            // 
            // TSSLBanyakDataInvent
            // 
            this.TSSLBanyakDataInvent.Name = "TSSLBanyakDataInvent";
            this.TSSLBanyakDataInvent.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel19
            // 
            this.toolStripStatusLabel19.Name = "toolStripStatusLabel19";
            this.toolStripStatusLabel19.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel19.Text = " | ";
            // 
            // TSSLNimInvent
            // 
            this.TSSLNimInvent.Name = "TSSLNimInvent";
            this.TSSLNimInvent.Size = new System.Drawing.Size(10, 17);
            this.TSSLNimInvent.Text = " ";
            // 
            // LDSInvent
            // 
            this.LDSInvent.AutoSize = false;
            this.LDSInvent.Name = "LDSInvent";
            this.LDSInvent.Size = new System.Drawing.Size(843, 17);
            this.LDSInvent.Spring = true;
            // 
            // RMInventaris
            // 
            this.RMInventaris.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.RMInventaris.ForeColor = System.Drawing.Color.Black;
            this.RMInventaris.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMITambahInvent,
            this.RMIUbahInvent,
            this.RMIHapusInvent,
            this.RMIPeminjaman});
            this.RMInventaris.Location = new System.Drawing.Point(44, 8);
            this.RMInventaris.Name = "RMInventaris";
            this.RMInventaris.Size = new System.Drawing.Size(28, 28);
            this.RMInventaris.Symbol = "";
            this.RMInventaris.SymbolSize = 14F;
            this.RMInventaris.TabIndex = 16;
            this.RMInventaris.Text = "RadialMenu2";
            this.RMInventaris.MouseEnter += new System.EventHandler(this.Menu_MouseEnter);
            this.RMInventaris.MouseLeave += new System.EventHandler(this.Menu_MouseLeave);
            // 
            // RMITambahInvent
            // 
            this.RMITambahInvent.Name = "RMITambahInvent";
            this.RMITambahInvent.Symbol = "";
            this.RMITambahInvent.Text = "Tambah";
            this.RMITambahInvent.Click += new System.EventHandler(this.RMITambahInvent_Klik);
            // 
            // RMIUbahInvent
            // 
            this.RMIUbahInvent.Name = "RMIUbahInvent";
            this.RMIUbahInvent.Symbol = "";
            this.RMIUbahInvent.Text = "Ubah";
            this.RMIUbahInvent.Click += new System.EventHandler(this.RMIUbahInvent_Klik);
            // 
            // RMIHapusInvent
            // 
            this.RMIHapusInvent.Name = "RMIHapusInvent";
            this.RMIHapusInvent.Symbol = "";
            this.RMIHapusInvent.Text = "Hapus";
            this.RMIHapusInvent.Click += new System.EventHandler(this.RMIHapusInvent_Klik);
            // 
            // RMIPeminjaman
            // 
            this.RMIPeminjaman.Name = "RMIPeminjaman";
            this.RMIPeminjaman.Symbol = "";
            this.RMIPeminjaman.Text = "Invent";
            this.RMIPeminjaman.Click += new System.EventHandler(this.RMIPeminjaman_Klik);
            // 
            // TBIInventaris
            // 
            this.TBIInventaris.AttachedControl = this.tabControlPanel6;
            this.TBIInventaris.Name = "TBIInventaris";
            this.TBIInventaris.Text = "Inventaris";
            this.TBIInventaris.Click += new System.EventHandler(this.Tab_Klik);
            // 
            // tabControlPanel7
            // 
            this.tabControlPanel7.Controls.Add(this.DGKeuangan);
            this.tabControlPanel7.Controls.Add(this.statusStrip7);
            this.tabControlPanel7.Controls.Add(this.RMKeuangan);
            this.tabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel7.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel7.Name = "tabControlPanel7";
            this.tabControlPanel7.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel7.Size = new System.Drawing.Size(889, 320);
            this.tabControlPanel7.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel7.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel7.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel7.Style.GradientAngle = 90;
            this.tabControlPanel7.TabIndex = 7;
            this.tabControlPanel7.TabItem = this.TBIKeuangan;
            // 
            // DGKeuangan
            // 
            this.DGKeuangan.AllowUserToAddRows = false;
            this.DGKeuangan.AllowUserToDeleteRows = false;
            this.DGKeuangan.AllowUserToResizeColumns = false;
            this.DGKeuangan.AllowUserToResizeRows = false;
            this.DGKeuangan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGKeuangan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGKeuangan.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGKeuangan.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGKeuangan.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.DGKeuangan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGKeuangan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_keuangan,
            this.nama_anggota,
            this.tipes,
            this.keterangan,
            this.nominal,
            this.dataGridViewTextBoxColumn30,
            this.total});
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGKeuangan.DefaultCellStyle = dataGridViewCellStyle17;
            this.DGKeuangan.EnableHeadersVisualStyles = false;
            this.DGKeuangan.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGKeuangan.Location = new System.Drawing.Point(1, 42);
            this.DGKeuangan.Name = "DGKeuangan";
            this.DGKeuangan.ReadOnly = true;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGKeuangan.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.DGKeuangan.RowHeadersVisible = false;
            this.DGKeuangan.RowHeadersWidth = 80;
            this.DGKeuangan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGKeuangan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGKeuangan.Size = new System.Drawing.Size(887, 255);
            this.DGKeuangan.TabIndex = 18;
            this.DGKeuangan.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGKeuangan_CellKlik);
            // 
            // id_keuangan
            // 
            this.id_keuangan.DataPropertyName = "id_keuangan";
            this.id_keuangan.HeaderText = "ID Keuangan";
            this.id_keuangan.Name = "id_keuangan";
            this.id_keuangan.ReadOnly = true;
            this.id_keuangan.Visible = false;
            // 
            // nama_anggota
            // 
            this.nama_anggota.DataPropertyName = "nama_anggota";
            this.nama_anggota.HeaderText = "Nama Anggota";
            this.nama_anggota.Name = "nama_anggota";
            this.nama_anggota.ReadOnly = true;
            // 
            // tipes
            // 
            this.tipes.DataPropertyName = "tipes";
            this.tipes.FillWeight = 40F;
            this.tipes.HeaderText = "Tipe";
            this.tipes.Name = "tipes";
            this.tipes.ReadOnly = true;
            // 
            // keterangan
            // 
            this.keterangan.DataPropertyName = "keterangan";
            this.keterangan.HeaderText = "Keterangan";
            this.keterangan.Name = "keterangan";
            this.keterangan.ReadOnly = true;
            // 
            // nominal
            // 
            this.nominal.DataPropertyName = "nominal";
            this.nominal.FillWeight = 50F;
            this.nominal.HeaderText = "Nominal";
            this.nominal.Name = "nominal";
            this.nominal.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "tanggal";
            this.dataGridViewTextBoxColumn30.FillWeight = 80F;
            this.dataGridViewTextBoxColumn30.HeaderText = "Tanggal";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            // 
            // total
            // 
            this.total.DataPropertyName = "total";
            this.total.FillWeight = 50F;
            this.total.HeaderText = "Jumlah";
            this.total.Name = "total";
            this.total.ReadOnly = true;
            // 
            // statusStrip7
            // 
            this.statusStrip7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.statusStrip7.ForeColor = System.Drawing.Color.Black;
            this.statusStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2,
            this.TSSLTotalDana,
            this.toolStripStatusLabel23,
            this.TSSLIdKeuangan,
            this.LDSKeuangan});
            this.statusStrip7.Location = new System.Drawing.Point(1, 297);
            this.statusStrip7.Name = "statusStrip7";
            this.statusStrip7.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip7.Size = new System.Drawing.Size(887, 22);
            this.statusStrip7.SizingGrip = false;
            this.statusStrip7.TabIndex = 17;
            this.statusStrip7.Text = "SSAnggota";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(27, 17);
            this.toolStripStatusLabel2.Text = "Rp. ";
            // 
            // TSSLTotalDana
            // 
            this.TSSLTotalDana.Name = "TSSLTotalDana";
            this.TSSLTotalDana.Size = new System.Drawing.Size(13, 17);
            this.TSSLTotalDana.Text = "#";
            // 
            // toolStripStatusLabel23
            // 
            this.toolStripStatusLabel23.Name = "toolStripStatusLabel23";
            this.toolStripStatusLabel23.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel23.Text = " | ";
            // 
            // TSSLIdKeuangan
            // 
            this.TSSLIdKeuangan.Name = "TSSLIdKeuangan";
            this.TSSLIdKeuangan.Size = new System.Drawing.Size(10, 17);
            this.TSSLIdKeuangan.Text = " ";
            // 
            // LDSKeuangan
            // 
            this.LDSKeuangan.Name = "LDSKeuangan";
            this.LDSKeuangan.Size = new System.Drawing.Size(803, 17);
            this.LDSKeuangan.Spring = true;
            // 
            // RMKeuangan
            // 
            this.RMKeuangan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.RMKeuangan.ForeColor = System.Drawing.Color.Black;
            this.RMKeuangan.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMITambahDana,
            this.RMIHapusDana,
            this.RMIUbahDana,
            this.radialMenuItem4});
            this.RMKeuangan.Location = new System.Drawing.Point(44, 8);
            this.RMKeuangan.Name = "RMKeuangan";
            this.RMKeuangan.Size = new System.Drawing.Size(28, 28);
            this.RMKeuangan.Symbol = "";
            this.RMKeuangan.SymbolSize = 14F;
            this.RMKeuangan.TabIndex = 16;
            this.RMKeuangan.Text = "RadialMenu2";
            this.RMKeuangan.MouseEnter += new System.EventHandler(this.Menu_MouseEnter);
            this.RMKeuangan.MouseLeave += new System.EventHandler(this.Menu_MouseLeave);
            // 
            // RMITambahDana
            // 
            this.RMITambahDana.Name = "RMITambahDana";
            this.RMITambahDana.Symbol = "";
            this.RMITambahDana.Text = "Tambah";
            this.RMITambahDana.Click += new System.EventHandler(this.RMITambahDataKeuangan_Klik);
            // 
            // RMIHapusDana
            // 
            this.RMIHapusDana.AutoCollapseOnClick = false;
            this.RMIHapusDana.Name = "RMIHapusDana";
            this.RMIHapusDana.TracksMouse = false;
            this.RMIHapusDana.Visible = false;
            // 
            // RMIUbahDana
            // 
            this.RMIUbahDana.Name = "RMIUbahDana";
            this.RMIUbahDana.Symbol = "";
            this.RMIUbahDana.Text = "Ubah";
            this.RMIUbahDana.Click += new System.EventHandler(this.RMIUbahDataKeuangan_Klik);
            // 
            // radialMenuItem4
            // 
            this.radialMenuItem4.AutoCollapseOnClick = false;
            this.radialMenuItem4.Name = "radialMenuItem4";
            this.radialMenuItem4.TracksMouse = false;
            // 
            // TBIKeuangan
            // 
            this.TBIKeuangan.AttachedControl = this.tabControlPanel7;
            this.TBIKeuangan.Name = "TBIKeuangan";
            this.TBIKeuangan.Text = "Keuangan";
            this.TBIKeuangan.Click += new System.EventHandler(this.Tab_Klik);
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.Controls.Add(this.BAbsenPiket);
            this.tabControlPanel3.Controls.Add(this.DGPiket);
            this.tabControlPanel3.Controls.Add(this.statusStrip3);
            this.tabControlPanel3.Controls.Add(this.RMPiket);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 27);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(889, 320);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(196)))), ((int)(((byte)(198)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right) 
            | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 3;
            this.tabControlPanel3.TabItem = this.TBIPiket;
            // 
            // DGPiket
            // 
            this.DGPiket.AllowUserToAddRows = false;
            this.DGPiket.AllowUserToDeleteRows = false;
            this.DGPiket.AllowUserToResizeColumns = false;
            this.DGPiket.AllowUserToResizeRows = false;
            this.DGPiket.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGPiket.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGPiket.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGPiket.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGPiket.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.DGPiket.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGPiket.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn12,
            this.hari,
            this.jam});
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGPiket.DefaultCellStyle = dataGridViewCellStyle20;
            this.DGPiket.EnableHeadersVisualStyles = false;
            this.DGPiket.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(157)))));
            this.DGPiket.Location = new System.Drawing.Point(1, 42);
            this.DGPiket.Name = "DGPiket";
            this.DGPiket.ReadOnly = true;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGPiket.RowHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.DGPiket.RowHeadersVisible = false;
            this.DGPiket.RowHeadersWidth = 80;
            this.DGPiket.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGPiket.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGPiket.Size = new System.Drawing.Size(887, 255);
            this.DGPiket.TabIndex = 15;
            this.DGPiket.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGPiket_CellContentClick);
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "nim";
            this.dataGridViewTextBoxColumn7.FillWeight = 30F;
            this.dataGridViewTextBoxColumn7.HeaderText = "Nim";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "nama_anggota";
            this.dataGridViewTextBoxColumn8.HeaderText = "Nama Anggota";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "kelas";
            this.dataGridViewTextBoxColumn9.FillWeight = 40F;
            this.dataGridViewTextBoxColumn9.HeaderText = "Kelas";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "no_hp";
            this.dataGridViewTextBoxColumn12.FillWeight = 60F;
            this.dataGridViewTextBoxColumn12.HeaderText = "No. Handphone";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // hari
            // 
            this.hari.DataPropertyName = "hari";
            this.hari.FillWeight = 30F;
            this.hari.HeaderText = "Hari";
            this.hari.Name = "hari";
            this.hari.ReadOnly = true;
            // 
            // jam
            // 
            this.jam.DataPropertyName = "waktu";
            this.jam.FillWeight = 30F;
            this.jam.HeaderText = "Jam";
            this.jam.Name = "jam";
            this.jam.ReadOnly = true;
            // 
            // statusStrip3
            // 
            this.statusStrip3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.statusStrip3.ForeColor = System.Drawing.Color.Black;
            this.statusStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSSLBanyakDataPiket,
            this.toolStripStatusLabel7,
            this.TSSLNimPiket,
            this.LDSPiket});
            this.statusStrip3.Location = new System.Drawing.Point(1, 297);
            this.statusStrip3.Name = "statusStrip3";
            this.statusStrip3.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip3.Size = new System.Drawing.Size(887, 22);
            this.statusStrip3.SizingGrip = false;
            this.statusStrip3.TabIndex = 14;
            this.statusStrip3.Text = "SSAnggota";
            // 
            // TSSLBanyakDataPiket
            // 
            this.TSSLBanyakDataPiket.Name = "TSSLBanyakDataPiket";
            this.TSSLBanyakDataPiket.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel7
            // 
            this.toolStripStatusLabel7.Name = "toolStripStatusLabel7";
            this.toolStripStatusLabel7.Size = new System.Drawing.Size(19, 17);
            this.toolStripStatusLabel7.Text = " | ";
            // 
            // TSSLNimPiket
            // 
            this.TSSLNimPiket.Name = "TSSLNimPiket";
            this.TSSLNimPiket.Size = new System.Drawing.Size(10, 17);
            this.TSSLNimPiket.Text = " ";
            // 
            // LDSPiket
            // 
            this.LDSPiket.Name = "LDSPiket";
            this.LDSPiket.Size = new System.Drawing.Size(843, 17);
            this.LDSPiket.Spring = true;
            // 
            // TBIPiket
            // 
            this.TBIPiket.AttachedControl = this.tabControlPanel3;
            this.TBIPiket.Name = "TBIPiket";
            this.TBIPiket.Text = "Piket";
            this.TBIPiket.Click += new System.EventHandler(this.Tab_Klik);
            // 
            // radialMenuItem7
            // 
            this.radialMenuItem7.Name = "radialMenuItem7";
            // 
            // radialMenuItem9
            // 
            this.radialMenuItem9.Name = "radialMenuItem9";
            // 
            // groupPanel2
            // 
            this.groupPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.groupPanel2.Controls.Add(this.RLNoJadwal);
            this.groupPanel2.Controls.Add(this.DGPiketNow);
            this.groupPanel2.Location = new System.Drawing.Point(688, 12);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(213, 167);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel2.TabIndex = 3;
            this.groupPanel2.Text = "Piket Hari Ini";
            // 
            // RLNoJadwal
            // 
            // 
            // 
            // 
            this.RLNoJadwal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.RLNoJadwal.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.RLNoJadwal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.RLNoJadwal.Location = new System.Drawing.Point(5, 36);
            this.RLNoJadwal.Name = "RLNoJadwal";
            this.RLNoJadwal.Size = new System.Drawing.Size(196, 70);
            this.RLNoJadwal.TabIndex = 24;
            this.RLNoJadwal.Text = "Tidak ada jadwal piket hari ini.";
            // 
            // DGPiketNow
            // 
            this.DGPiketNow.AllowUserToAddRows = false;
            this.DGPiketNow.AllowUserToDeleteRows = false;
            this.DGPiketNow.AllowUserToResizeColumns = false;
            this.DGPiketNow.AllowUserToResizeRows = false;
            this.DGPiketNow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGPiketNow.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGPiketNow.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.DGPiketNow.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGPiketNow.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.DGPiketNow.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.DGPiketNow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGPiketNow.ColumnHeadersVisible = false;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGPiketNow.DefaultCellStyle = dataGridViewCellStyle22;
            this.DGPiketNow.EnableHeadersVisualStyles = false;
            this.DGPiketNow.Location = new System.Drawing.Point(5, 19);
            this.DGPiketNow.MultiSelect = false;
            this.DGPiketNow.Name = "DGPiketNow";
            this.DGPiketNow.ReadOnly = true;
            this.DGPiketNow.RowHeadersVisible = false;
            this.DGPiketNow.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.DGPiketNow.Size = new System.Drawing.Size(196, 118);
            this.DGPiketNow.TabIndex = 1;
            // 
            // ReflectionLabel1
            // 
            this.ReflectionLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReflectionLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.ReflectionLabel1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.ReflectionLabel1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.ReflectionLabel1.BackgroundStyle.TextColor = System.Drawing.Color.LightSteelBlue;
            this.ReflectionLabel1.BackgroundStyle.TextShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ReflectionLabel1.BackgroundStyle.TextShadowOffset = new System.Drawing.Point(5, 5);
            this.ReflectionLabel1.Font = new System.Drawing.Font("Continuum Medium", 14.75F);
            this.ReflectionLabel1.ForeColor = System.Drawing.Color.Black;
            this.ReflectionLabel1.Location = new System.Drawing.Point(163, 215);
            this.ReflectionLabel1.Name = "ReflectionLabel1";
            this.ReflectionLabel1.Size = new System.Drawing.Size(563, 36);
            this.ReflectionLabel1.TabIndex = 11;
            this.ReflectionLabel1.Text = "Information Technology Research and Multimedia [INTERMEDIA]";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(28, 225);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(16, 23);
            this.labelX5.Symbol = "";
            this.labelX5.SymbolColor = System.Drawing.Color.Silver;
            this.labelX5.SymbolSize = 20F;
            this.labelX5.TabIndex = 12;
            // 
            // labelX6
            // 
            this.labelX6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelX6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(868, 225);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(16, 23);
            this.labelX6.Symbol = "";
            this.labelX6.SymbolColor = System.Drawing.Color.Silver;
            this.labelX6.SymbolSize = 20F;
            this.labelX6.TabIndex = 13;
            // 
            // RMMenu
            // 
            this.RMMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RMMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.RMMenu.ForeColor = System.Drawing.Color.Black;
            this.RMMenu.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.RMITentang,
            this.radialMenuItem6,
            this.RMIRiwayat,
            this.radialMenuItem11});
            this.RMMenu.Location = new System.Drawing.Point(829, 220);
            this.RMMenu.Name = "RMMenu";
            this.RMMenu.Size = new System.Drawing.Size(28, 28);
            this.RMMenu.Symbol = "";
            this.RMMenu.TabIndex = 14;
            this.RMMenu.Text = "radialMenu1";
            this.RMMenu.MouseEnter += new System.EventHandler(this.RMenu_MouseEnter);
            this.RMMenu.MouseLeave += new System.EventHandler(this.RMenu_MouseLeave);
            // 
            // RMITentang
            // 
            this.RMITentang.Name = "RMITentang";
            this.RMITentang.Symbol = "";
            this.RMITentang.Text = "Tentang";
            this.RMITentang.Click += new System.EventHandler(this.RMITentang_Klik);
            // 
            // radialMenuItem6
            // 
            this.radialMenuItem6.AutoCollapseOnClick = false;
            this.radialMenuItem6.Name = "radialMenuItem6";
            this.radialMenuItem6.Text = "Item 2";
            this.radialMenuItem6.TracksMouse = false;
            this.radialMenuItem6.Visible = false;
            // 
            // RMIRiwayat
            // 
            this.RMIRiwayat.Name = "RMIRiwayat";
            this.RMIRiwayat.Symbol = "";
            this.RMIRiwayat.Text = "Riwayat";
            this.RMIRiwayat.Click += new System.EventHandler(this.RMIRiwayat_Klik);
            // 
            // radialMenuItem11
            // 
            this.radialMenuItem11.AutoCollapseOnClick = false;
            this.radialMenuItem11.Name = "radialMenuItem11";
            this.radialMenuItem11.Text = "Item 4";
            this.radialMenuItem11.TracksMouse = false;
            this.radialMenuItem11.Visible = false;
            // 
            // groupPanel3
            // 
            this.groupPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.groupPanel3.Controls.Add(this.LRapatSelanjutnya);
            this.groupPanel3.Controls.Add(this.LAgendaSelanjutnya);
            this.groupPanel3.Controls.Add(this.LBarangBaru);
            this.groupPanel3.Controls.Add(this.LTotalKas);
            this.groupPanel3.Controls.Add(this.labelX11);
            this.groupPanel3.Controls.Add(this.labelX12);
            this.groupPanel3.Controls.Add(this.labelX13);
            this.groupPanel3.Controls.Add(this.labelX14);
            this.groupPanel3.Location = new System.Drawing.Point(318, 12);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(364, 167);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.Style.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            this.groupPanel3.Style.BackColorGradientAngle = 90;
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel3.TabIndex = 15;
            this.groupPanel3.Text = "Informasi";
            // 
            // LRapatSelanjutnya
            // 
            this.LRapatSelanjutnya.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LRapatSelanjutnya.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LRapatSelanjutnya.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LRapatSelanjutnya.ForeColor = System.Drawing.Color.Black;
            this.LRapatSelanjutnya.Location = new System.Drawing.Point(165, 116);
            this.LRapatSelanjutnya.Name = "LRapatSelanjutnya";
            this.LRapatSelanjutnya.Size = new System.Drawing.Size(185, 23);
            this.LRapatSelanjutnya.TabIndex = 23;
            this.LRapatSelanjutnya.Text = "Belum ditetapkan";
            // 
            // LAgendaSelanjutnya
            // 
            this.LAgendaSelanjutnya.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LAgendaSelanjutnya.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LAgendaSelanjutnya.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LAgendaSelanjutnya.ForeColor = System.Drawing.Color.Black;
            this.LAgendaSelanjutnya.Location = new System.Drawing.Point(165, 80);
            this.LAgendaSelanjutnya.Name = "LAgendaSelanjutnya";
            this.LAgendaSelanjutnya.Size = new System.Drawing.Size(185, 23);
            this.LAgendaSelanjutnya.TabIndex = 22;
            this.LAgendaSelanjutnya.Text = "Belum ditetapkan";
            // 
            // LBarangBaru
            // 
            this.LBarangBaru.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LBarangBaru.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LBarangBaru.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LBarangBaru.ForeColor = System.Drawing.Color.Black;
            this.LBarangBaru.Location = new System.Drawing.Point(165, 44);
            this.LBarangBaru.Name = "LBarangBaru";
            this.LBarangBaru.Size = new System.Drawing.Size(185, 23);
            this.LBarangBaru.TabIndex = 21;
            this.LBarangBaru.Text = "Belum ditetapkan";
            // 
            // LTotalKas
            // 
            this.LTotalKas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LTotalKas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LTotalKas.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LTotalKas.ForeColor = System.Drawing.Color.Black;
            this.LTotalKas.Location = new System.Drawing.Point(165, 8);
            this.LTotalKas.Name = "LTotalKas";
            this.LTotalKas.Size = new System.Drawing.Size(185, 23);
            this.LTotalKas.TabIndex = 20;
            this.LTotalKas.Text = "Belum ditetapkan";
            // 
            // labelX11
            // 
            this.labelX11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.labelX11.ForeColor = System.Drawing.Color.Black;
            this.labelX11.ImageTextSpacing = 5;
            this.labelX11.Location = new System.Drawing.Point(9, 116);
            this.labelX11.Name = "labelX11";
            this.labelX11.Size = new System.Drawing.Size(139, 23);
            this.labelX11.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX11.Symbol = "";
            this.labelX11.SymbolColor = System.Drawing.Color.SteelBlue;
            this.labelX11.SymbolSize = 18F;
            this.labelX11.TabIndex = 19;
            this.labelX11.Text = "Rapat";
            // 
            // labelX12
            // 
            this.labelX12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.labelX12.ForeColor = System.Drawing.Color.Black;
            this.labelX12.ImageTextSpacing = 5;
            this.labelX12.Location = new System.Drawing.Point(9, 80);
            this.labelX12.Name = "labelX12";
            this.labelX12.Size = new System.Drawing.Size(143, 23);
            this.labelX12.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX12.Symbol = "";
            this.labelX12.SymbolColor = System.Drawing.Color.SteelBlue;
            this.labelX12.SymbolSize = 18F;
            this.labelX12.TabIndex = 18;
            this.labelX12.Text = "Agenda Selanjutnya";
            // 
            // labelX13
            // 
            this.labelX13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.labelX13.ForeColor = System.Drawing.Color.Black;
            this.labelX13.ImageTextSpacing = 5;
            this.labelX13.Location = new System.Drawing.Point(9, 44);
            this.labelX13.Name = "labelX13";
            this.labelX13.Size = new System.Drawing.Size(139, 23);
            this.labelX13.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX13.Symbol = "";
            this.labelX13.SymbolColor = System.Drawing.Color.SteelBlue;
            this.labelX13.SymbolSize = 18F;
            this.labelX13.TabIndex = 17;
            this.labelX13.Text = "Barang Baru";
            // 
            // labelX14
            // 
            this.labelX14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.labelX14.ForeColor = System.Drawing.Color.Black;
            this.labelX14.ImageTextSpacing = 5;
            this.labelX14.Location = new System.Drawing.Point(9, 8);
            this.labelX14.Name = "labelX14";
            this.labelX14.Size = new System.Drawing.Size(139, 23);
            this.labelX14.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro;
            this.labelX14.Symbol = "";
            this.labelX14.SymbolColor = System.Drawing.Color.SteelBlue;
            this.labelX14.SymbolSize = 18F;
            this.labelX14.TabIndex = 16;
            this.labelX14.Text = "Total Kas";
            // 
            // Form_Utama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 560);
            this.Controls.Add(this.groupPanel3);
            this.Controls.Add(this.RMMenu);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.ReflectionLabel1);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.metroStatusBar1);
            this.DoubleBuffered = true;
            this.HelpButtonText = "LOGOUT";
            this.HelpButtonVisible = true;
            this.MinimumSize = new System.Drawing.Size(800, 594);
            this.Name = "Form_Utama";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Intermedia";
            this.HelpButtonClick += new System.EventHandler(this.Logout_Klik);
            this.Deactivate += new System.EventHandler(this.FormUtama_Aktif);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Utama_Tertutup);
            this.Load += new System.EventHandler(this.Form_Utama_Load);
            this.Shown += new System.EventHandler(this.FormUtama_Tampil);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            this.tabControlPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGAnggota)).EndInit();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.tabControlPanel2.ResumeLayout(false);
            this.tabControlPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGStrukturAngg)).EndInit();
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.tabControlPanel4.ResumeLayout(false);
            this.tabControlPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGRapat)).EndInit();
            this.statusStrip4.ResumeLayout(false);
            this.statusStrip4.PerformLayout();
            this.tabControlPanel5.ResumeLayout(false);
            this.tabControlPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGProker)).EndInit();
            this.statusStrip5.ResumeLayout(false);
            this.statusStrip5.PerformLayout();
            this.tabControlPanel6.ResumeLayout(false);
            this.tabControlPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGInventaris)).EndInit();
            this.statusStrip6.ResumeLayout(false);
            this.statusStrip6.PerformLayout();
            this.tabControlPanel7.ResumeLayout(false);
            this.tabControlPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGKeuangan)).EndInit();
            this.statusStrip7.ResumeLayout(false);
            this.statusStrip7.PerformLayout();
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGPiket)).EndInit();
            this.statusStrip3.ResumeLayout(false);
            this.statusStrip3.PerformLayout();
            this.groupPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGPiketNow)).EndInit();
            this.groupPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.StyleManagerAmbient styleManagerAmbient1;
        private DevComponents.DotNetBar.Metro.MetroStatusBar metroStatusBar1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.TabControl tabControl1;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel3;
        private DevComponents.DotNetBar.TabItem TBIPiket;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.TabItem TBIStruktur;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private DevComponents.DotNetBar.TabItem TBIAnggota;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel4;
        private DevComponents.DotNetBar.TabItem TBIRapat;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.LabelItem LNamaP;
        private DevComponents.DotNetBar.LabelItem LNim;
        private DevComponents.DotNetBar.LabelItem LJabatan;
        private DevComponents.DotNetBar.LabelItem LWaktuSekarang;
        private DevComponents.DotNetBar.RadialMenuItem RMIUbahA;
        private DevComponents.DotNetBar.RadialMenuItem RMITambahA;
        private DevComponents.DotNetBar.RadialMenuItem radialMenuItem7;
        private DevComponents.DotNetBar.RadialMenuItem radialMenuItem9;
        private DevComponents.DotNetBar.RadialMenuItem RMIHapusA;
        internal DevComponents.DotNetBar.Controls.ReflectionLabel ReflectionLabel1;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLBanyakDataAnggota;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLNimAnggota;
        internal System.Windows.Forms.ToolStripStatusLabel LDSAnggota;
        private System.Windows.Forms.DataGridViewTextBoxColumn NimDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NamaanggotaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn KelasDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlamatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn JkelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NohpDataGridViewTextBoxColumn;
        internal DevComponents.DotNetBar.LabelX LBendahara;
        internal DevComponents.DotNetBar.LabelX LSekretaris;
        internal DevComponents.DotNetBar.LabelX LWakilKetua;
        internal DevComponents.DotNetBar.LabelX LKetua;
        internal DevComponents.DotNetBar.LabelX LabelX3;
        internal DevComponents.DotNetBar.LabelX LabelX4;
        internal DevComponents.DotNetBar.LabelX LabelX2;
        internal DevComponents.DotNetBar.LabelX LabelX1;
        internal System.Windows.Forms.DataGridView DGPiketNow;
        internal System.Windows.Forms.StatusStrip statusStrip2;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLBanyakDataStruktur;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLNimStruktur;
        internal System.Windows.Forms.ToolStripStatusLabel LDSStruktur;
        internal DevComponents.DotNetBar.RadialMenu RMStruktur;
        private DevComponents.DotNetBar.RadialMenuItem RMIUbahStruktur;
        private DevComponents.DotNetBar.RadialMenuItem radialMenuItem5;
        private DevComponents.DotNetBar.RadialMenuItem RMIBagan;
        private DevComponents.DotNetBar.RadialMenuItem radialMenuItem8;
        internal DevComponents.DotNetBar.Controls.DataGridViewX DGPiket;
        internal System.Windows.Forms.StatusStrip statusStrip3;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLBanyakDataPiket;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel7;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLNimPiket;
        internal System.Windows.Forms.ToolStripStatusLabel LDSPiket;
        internal DevComponents.DotNetBar.RadialMenu RMPiket;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel7;
        private DevComponents.DotNetBar.TabItem TBIKeuangan;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel6;
        private DevComponents.DotNetBar.TabItem TBIInventaris;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel5;
        private DevComponents.DotNetBar.TabItem TBIProker;
        internal DevComponents.DotNetBar.RadialMenu RMAnggota;
        internal DevComponents.DotNetBar.Controls.DataGridViewX DGKeuangan;
        internal System.Windows.Forms.StatusStrip statusStrip7;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLTotalDana;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel23;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLIdKeuangan;
        internal System.Windows.Forms.ToolStripStatusLabel LDSKeuangan;
        internal DevComponents.DotNetBar.RadialMenu RMKeuangan;
        internal DevComponents.DotNetBar.Controls.DataGridViewX DGInventaris;
        internal System.Windows.Forms.StatusStrip statusStrip6;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLBanyakDataInvent;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel19;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLNimInvent;
        internal System.Windows.Forms.ToolStripStatusLabel LDSInvent;
        internal DevComponents.DotNetBar.RadialMenu RMInventaris;
        internal DevComponents.DotNetBar.Controls.DataGridViewX DGProker;
        internal System.Windows.Forms.StatusStrip statusStrip5;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLBanyakDataProker;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel15;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLIdProker;
        internal System.Windows.Forms.ToolStripStatusLabel LDSProker;
        internal DevComponents.DotNetBar.RadialMenu RMProker;
        internal DevComponents.DotNetBar.Controls.DataGridViewX DGRapat;
        internal System.Windows.Forms.StatusStrip statusStrip4;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLBanyakDataRapat;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel11;
        internal System.Windows.Forms.ToolStripStatusLabel TSSLNamaRapat;
        internal System.Windows.Forms.ToolStripStatusLabel LDSRapat;
        internal DevComponents.DotNetBar.RadialMenu RMRapat;
        public DevComponents.DotNetBar.Controls.DataGridViewX DGStrukturAngg;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        public DevComponents.DotNetBar.Controls.DataGridViewX DGAnggota;
        private DevComponents.DotNetBar.RadialMenu RMMenu;
        private DevComponents.DotNetBar.RadialMenuItem RMITentang;
        private DevComponents.DotNetBar.RadialMenuItem radialMenuItem6;
        private DevComponents.DotNetBar.RadialMenuItem RMIRiwayat;
        private DevComponents.DotNetBar.RadialMenuItem radialMenuItem11;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        internal DevComponents.DotNetBar.LabelX LRapatSelanjutnya;
        internal DevComponents.DotNetBar.LabelX LAgendaSelanjutnya;
        internal DevComponents.DotNetBar.LabelX LBarangBaru;
        internal DevComponents.DotNetBar.LabelX LTotalKas;
        internal DevComponents.DotNetBar.LabelX labelX11;
        internal DevComponents.DotNetBar.LabelX labelX12;
        internal DevComponents.DotNetBar.LabelX labelX13;
        internal DevComponents.DotNetBar.LabelX labelX14;
        private DevComponents.DotNetBar.RadialMenuItem RMITambahPiket;
        private DevComponents.DotNetBar.RadialMenuItem RMIUbahPiket;
        private DevComponents.DotNetBar.RadialMenuItem RMIHapusPiket;
        private DevComponents.DotNetBar.RadialMenuItem RMIAbsenPiket;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn hari;
        private System.Windows.Forms.DataGridViewTextBoxColumn jam;
        internal DevComponents.DotNetBar.ButtonX BAbsenPiket;
        internal DevComponents.DotNetBar.ButtonX BAbsenRapat;
        private DevComponents.DotNetBar.RadialMenuItem RMITambahRapat;
        private DevComponents.DotNetBar.RadialMenuItem RMIUbahRapat;
        private DevComponents.DotNetBar.RadialMenuItem RMIHapusRapat;
        private DevComponents.DotNetBar.RadialMenuItem RMIAbsenRapat;
        private DevComponents.DotNetBar.RadialMenuItem RMITambahProker;
        private DevComponents.DotNetBar.RadialMenuItem RMIUbahProker;
        private DevComponents.DotNetBar.RadialMenuItem RMIHapusProker;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_proker;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private DevComponents.DotNetBar.RadialMenuItem RMITambahInvent;
        private DevComponents.DotNetBar.RadialMenuItem RMIUbahInvent;
        private DevComponents.DotNetBar.RadialMenuItem RMIHapusInvent;
        private DevComponents.DotNetBar.RadialMenuItem RMIPeminjaman;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_rapat;
        private System.Windows.Forms.DataGridViewTextBoxColumn nama_rapat;
        private System.Windows.Forms.DataGridViewTextBoxColumn hasil_rapat;
        private System.Windows.Forms.DataGridViewTextBoxColumn tanggal;
        private System.Windows.Forms.DataGridViewTextBoxColumn waktu;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_barang;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private DevComponents.DotNetBar.RadialMenuItem RMITambahDana;
        private DevComponents.DotNetBar.RadialMenuItem RMIUbahDana;
        private DevComponents.DotNetBar.RadialMenuItem RMIHapusDana;
        private DevComponents.DotNetBar.RadialMenuItem radialMenuItem4;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_keuangan;
        private System.Windows.Forms.DataGridViewTextBoxColumn nama_anggota;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipes;
        private System.Windows.Forms.DataGridViewTextBoxColumn keterangan;
        private System.Windows.Forms.DataGridViewTextBoxColumn nominal;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private DevComponents.DotNetBar.Controls.ReflectionLabel RLNoJadwal;
    }
}

