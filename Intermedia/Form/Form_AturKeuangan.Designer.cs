﻿namespace Intermedia.Form
{
    partial class Form_AturKeuangan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.LId = new DevComponents.DotNetBar.LabelX();
            this.BPilih = new DevComponents.DotNetBar.ButtonX();
            this.TBNim = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.CBTipe = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.BTutup = new DevComponents.DotNetBar.ButtonX();
            this.BSimpan = new DevComponents.DotNetBar.ButtonX();
            this.CBOtoClose = new DevComponents.DotNetBar.CheckBoxItem();
            this.styleManagerAmbient1 = new DevComponents.DotNetBar.StyleManagerAmbient(this.components);
            this.DTITanggal = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.TBKeterangan = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.TBNominal = new DevComponents.Editors.IntegerInput();
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            ((System.ComponentModel.ISupportInitialize)(this.DTITanggal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TBNominal)).BeginInit();
            this.SuspendLayout();
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(12, 4);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(84, 23);
            this.labelX1.TabIndex = 99;
            this.labelX1.Text = "ID";
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(12, 33);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(84, 23);
            this.labelX2.TabIndex = 97;
            this.labelX2.Text = "Nim";
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(12, 62);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(84, 23);
            this.labelX3.TabIndex = 96;
            this.labelX3.Text = "Tipe";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(12, 91);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(84, 23);
            this.labelX4.TabIndex = 95;
            this.labelX4.Text = "Keterangan";
            // 
            // labelX5
            // 
            this.labelX5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.ForeColor = System.Drawing.Color.Black;
            this.labelX5.Location = new System.Drawing.Point(12, 120);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(84, 23);
            this.labelX5.TabIndex = 94;
            this.labelX5.Text = "Nominal";
            // 
            // labelX6
            // 
            this.labelX6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.ForeColor = System.Drawing.Color.Black;
            this.labelX6.Location = new System.Drawing.Point(12, 149);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(84, 23);
            this.labelX6.TabIndex = 93;
            this.labelX6.Text = "Tanggal";
            // 
            // LId
            // 
            this.LId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LId.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LId.ForeColor = System.Drawing.Color.Black;
            this.LId.Location = new System.Drawing.Point(102, 4);
            this.LId.Name = "LId";
            this.LId.Size = new System.Drawing.Size(84, 23);
            this.LId.TabIndex = 98;
            this.LId.Text = "KS001";
            // 
            // BPilih
            // 
            this.BPilih.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPilih.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.highlighter1.SetHighlightOnFocus(this.BPilih, true);
            this.BPilih.Location = new System.Drawing.Point(240, 33);
            this.BPilih.Name = "BPilih";
            this.BPilih.Size = new System.Drawing.Size(68, 22);
            this.BPilih.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPilih.TabIndex = 51;
            this.BPilih.Text = "Pilih";
            this.BPilih.Click += new System.EventHandler(this.BPilih_Click);
            // 
            // TBNim
            // 
            this.TBNim.AllowPromptAsInput = false;
            this.TBNim.AntiAlias = true;
            this.TBNim.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBNim.BackgroundStyle.Class = "TextBoxBorder";
            this.TBNim.BackgroundStyle.CornerDiameter = 2;
            this.TBNim.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBNim.ButtonClear.Visible = true;
            this.styleManagerAmbient1.SetEnableAmbientSettings(this.TBNim, DevComponents.DotNetBar.eAmbientSettings.None);
            this.TBNim.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBNim, true);
            this.TBNim.Location = new System.Drawing.Point(102, 33);
            this.TBNim.Mask = "00.00.0000";
            this.TBNim.Name = "TBNim";
            this.TBNim.Size = new System.Drawing.Size(132, 22);
            this.TBNim.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TBNim.TabIndex = 50;
            this.TBNim.Text = "";
            this.TBNim.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // CBTipe
            // 
            this.CBTipe.DisplayMember = "Text";
            this.CBTipe.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CBTipe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBTipe.ForeColor = System.Drawing.Color.Black;
            this.CBTipe.FormattingEnabled = true;
            this.highlighter1.SetHighlightOnFocus(this.CBTipe, true);
            this.CBTipe.ItemHeight = 14;
            this.CBTipe.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2});
            this.CBTipe.Location = new System.Drawing.Point(102, 65);
            this.CBTipe.Name = "CBTipe";
            this.CBTipe.Size = new System.Drawing.Size(206, 20);
            this.CBTipe.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBTipe.TabIndex = 52;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "Masuk";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "Keluar";
            // 
            // BTutup
            // 
            this.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BTutup.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.Location = new System.Drawing.Point(6, 183);
            this.BTutup.Name = "BTutup";
            this.BTutup.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BTutup.Size = new System.Drawing.Size(142, 34);
            this.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BTutup.TabIndex = 60;
            this.BTutup.Text = "Batal";
            // 
            // BSimpan
            // 
            this.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BSimpan.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BSimpan.Location = new System.Drawing.Point(170, 183);
            this.BSimpan.Name = "BSimpan";
            this.BSimpan.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BSimpan.Size = new System.Drawing.Size(142, 34);
            this.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BSimpan.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.CBOtoClose});
            this.BSimpan.SubItemsExpandWidth = 18;
            this.BSimpan.TabIndex = 61;
            this.BSimpan.Text = "Tambah";
            this.BSimpan.Click += new System.EventHandler(this.BSimpan_Click);
            // 
            // CBOtoClose
            // 
            this.CBOtoClose.Checked = true;
            this.CBOtoClose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBOtoClose.GlobalItem = false;
            this.CBOtoClose.Name = "CBOtoClose";
            this.CBOtoClose.Stretch = true;
            this.CBOtoClose.Text = "Tutup otomatis";
            // 
            // DTITanggal
            // 
            this.DTITanggal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DTITanggal.AntiAlias = true;
            this.DTITanggal.AutoAdvance = true;
            this.DTITanggal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.DTITanggal.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DTITanggal.BackgroundStyle.CornerDiameter = 2;
            this.DTITanggal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.DTITanggal.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DTITanggal.ButtonDropDown.Visible = true;
            this.DTITanggal.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.DTITanggal, true);
            this.DTITanggal.IsPopupCalendarOpen = false;
            this.DTITanggal.Location = new System.Drawing.Point(102, 150);
            // 
            // 
            // 
            this.DTITanggal.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTITanggal.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTITanggal.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.DTITanggal.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTITanggal.MonthCalendar.DisplayMonth = new System.DateTime(2014, 2, 1, 0, 0, 0, 0);
            this.DTITanggal.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.DTITanggal.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DTITanggal.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTITanggal.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DTITanggal.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DTITanggal.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DTITanggal.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTITanggal.MonthCalendar.TodayButtonVisible = true;
            this.DTITanggal.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DTITanggal.Name = "DTITanggal";
            this.DTITanggal.Size = new System.Drawing.Size(206, 20);
            this.DTITanggal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DTITanggal.TabIndex = 55;
            this.DTITanggal.TimeSelectorTimeFormat = DevComponents.Editors.DateTimeAdv.eTimeSelectorFormat.Time12H;
            // 
            // TBKeterangan
            // 
            this.TBKeterangan.DisplayMember = "Text";
            this.TBKeterangan.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.TBKeterangan.ForeColor = System.Drawing.Color.Black;
            this.TBKeterangan.FormattingEnabled = true;
            this.highlighter1.SetHighlightOnFocus(this.TBKeterangan, true);
            this.TBKeterangan.ItemHeight = 14;
            this.TBKeterangan.Items.AddRange(new object[] {
            this.comboItem5,
            this.comboItem6,
            this.comboItem7,
            this.comboItem8});
            this.TBKeterangan.Location = new System.Drawing.Point(102, 92);
            this.TBKeterangan.Name = "TBKeterangan";
            this.TBKeterangan.Size = new System.Drawing.Size(206, 20);
            this.TBKeterangan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TBKeterangan.TabIndex = 53;
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "Kas";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "Sumbangan";
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "Pemasukan Acara";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "Pengeluaran Acara";
            // 
            // TBNominal
            // 
            this.TBNominal.AntiAlias = true;
            this.TBNominal.AutoOverwrite = true;
            this.TBNominal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBNominal.BackgroundStyle.Class = "DateTimeInputBackground";
            this.TBNominal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TBNominal.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.TBNominal.DisplayFormat = "Rp .,00";
            this.styleManagerAmbient1.SetEnableAmbientSettings(this.TBNominal, DevComponents.DotNetBar.eAmbientSettings.None);
            this.TBNominal.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBNominal, true);
            this.TBNominal.Increment = 100;
            this.TBNominal.Location = new System.Drawing.Point(102, 121);
            this.TBNominal.Name = "TBNominal";
            this.TBNominal.ShowUpDown = true;
            this.TBNominal.Size = new System.Drawing.Size(206, 20);
            this.TBNominal.TabIndex = 54;
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // Form_AturKeuangan
            // 
            this.AcceptButton = this.BSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BTutup;
            this.ClientSize = new System.Drawing.Size(320, 224);
            this.ControlBox = false;
            this.Controls.Add(this.TBNominal);
            this.Controls.Add(this.TBKeterangan);
            this.Controls.Add(this.DTITanggal);
            this.Controls.Add(this.BTutup);
            this.Controls.Add(this.BSimpan);
            this.Controls.Add(this.CBTipe);
            this.Controls.Add(this.BPilih);
            this.Controls.Add(this.TBNim);
            this.Controls.Add(this.LId);
            this.Controls.Add(this.labelX6);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.labelX1);
            this.DoubleBuffered = true;
            this.Name = "Form_AturKeuangan";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tambah Dana";
            this.Shown += new System.EventHandler(this.Form_AturKeuangan_Tampil);
            ((System.ComponentModel.ISupportInitialize)(this.DTITanggal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TBNominal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX LId;
        internal DevComponents.DotNetBar.ButtonX BPilih;
        internal DevComponents.DotNetBar.Controls.MaskedTextBoxAdv TBNim;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CBTipe;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        internal DevComponents.DotNetBar.ButtonX BTutup;
        internal DevComponents.DotNetBar.ButtonX BSimpan;
        internal DevComponents.DotNetBar.CheckBoxItem CBOtoClose;
        private DevComponents.DotNetBar.StyleManagerAmbient styleManagerAmbient1;
        internal DevComponents.Editors.DateTimeAdv.DateTimeInput DTITanggal;
        private DevComponents.DotNetBar.Controls.ComboBoxEx TBKeterangan;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.Editors.IntegerInput TBNominal;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
    }
}