﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Metro;
using System.Data.SqlClient;
using Intermedia.Class;
using Intermedia.Form;

namespace Intermedia
{
    public partial class Form_Utama : MetroForm
    {
        ModKoneksi DB = new ModKoneksi();
        ManagerData MDB = new ManagerData();
        Pengguna LogPengguna = new Pengguna();
        ManageDataAnggota MDA = new ManageDataAnggota();
        List<string> datastruktur;

        #region DeklarasiFungsi

        public Form_Utama()
        {
            InitializeComponent();
        }

        private void HapusData_Utama(DataGridViewSelectedRowCollection DataGridSelectedRows, string NamaTabel, string Bagian, bool Sembunyi = false, string unique = "nim")
        {
            string Query;
            MessageBoxEx.AntiAlias = true;
            MessageBoxEx.UseSystemLocalizedString = true;
            if (Sembunyi == false)
            {
                if (MessageBoxEx.Show(this, "Apa Anda yakin ingin menghapus data terpilih ?", "Intermedia", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    foreach (DataGridViewRow item in DataGridSelectedRows)
                    {
                        Query = NamaTabel + " " + unique + " = " + item.Cells[0].Value.ToString() + "";
                        MDA.HapusData(NamaTabel, unique + " = '" + item.Cells[0].Value.ToString() + "'");
                        MDA.Catat("Hapus Data " + Bagian, Query);
                    }
                    Segarkan(Bagian);
                    ToastNotification.Show(this, "Data terpilih telah terhapus", eToastPosition.BottomCenter);
                }
            }
            else
            {
                foreach (DataGridViewRow item in DataGridSelectedRows)
                {
                    Query = NamaTabel + " " + unique + " = " + item.Cells[0].Value.ToString() + "";
                    MDA.HapusData(NamaTabel, unique + " = '" + item.Cells[0].Value.ToString() + "'");
                    MDA.Catat("Hapus Data " + Bagian, Query);
                }
            }
        }

        private bool CekWaktuAbsenPiket(string Jam1, string Jam2, string hari, string hari2)
        {
            int jamI1, jamI2;
            jamI1 = int.Parse(1 + Jam1.Replace(":", ""));
            jamI2 = int.Parse(1 + Jam2.Replace(":00", "").Replace(":", ""));

            if ((jamI2 == 10950 && (jamI1 > 11040 || jamI1 < 10950)) || hari != hari2)
            {
                return false;
            }
            else if ((jamI2 == 11040 && (jamI1 > 11230 || jamI1 < 11040)) || hari != hari2)
            {
                return false;
            }
            else if ((jamI2 == 11230 && (jamI1 > 11420 || jamI1 < 11230)) || hari != hari2)
            {
                return false;
            }
            else if ((jamI2 == 11420 && (jamI1 > 11610 || jamI1 < 11430)) || hari != hari2)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CekWaktuAbsenRapat(string waktu1, string waktu2)
        {
            int waktuI1, waktuI2;
            waktuI1 = int.Parse(1 + waktu1.Replace(":", ""));
            waktuI2 = int.Parse(1 + waktu2.Replace(":00", "").Replace(":", ""));

            if (waktuI1 < waktuI2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SetKeterangan()
        {
            datastruktur = new List<string>();
            datastruktur = MDB.SetStruktur();
            try
            {
                LKetua.Text = datastruktur[0];
                LWakilKetua.Text = datastruktur[1];
                LSekretaris.Text = datastruktur[2];
                LBendahara.Text = datastruktur[3];
            }
            catch (Exception)
            {
            }
        }

        private string AmbilTotal()
        {
            string total;
            try
            {
                total = MDA.AmbilTeratasDariOrder("totalkeuangan", "total", "id_keuangan", "desc");
            }
            catch (Exception)
            {
                total = "0";
            }

            return total;
        }

        private string AmbilBarangBaru()
        {
            string data = MDA.AmbilTeratasDariOrder("inventaris", "nama_barang", "id_barang", "desc");
            if (data == null)
	        {
                data = "Tidak ada barang baru untuk saat ini.";
	        }
            return data;
        }

        private string AmbilAgendaSelanjutnya(bool refresh = true)
        {
            string data = "Tidak ada agenda yang akan datang untuk saat ini.";
            if (refresh == true)
            {
                Segarkan("Proker");
            }
            foreach (DataGridViewRow baris in DGProker.Rows)
            {
                if (DateTime.Parse(baris.Cells[2].Value.ToString()) > DateTime.Now)
                {
                    data = baris.Cells[1].Value.ToString();
                    return data;
                }
            }
            return data;
        }

        private string AmbilRapatSelanjutnya(bool refresh = true)
        {
            string data = "Tidak ada rapat yang akan datang untuk saat ini.";
            if (refresh == true)
            {
                Segarkan("Rapat");
            }
            foreach (DataGridViewRow baris in DGRapat.Rows)
            {
                if (DateTime.Parse(baris.Cells[3].Value.ToString()) > DateTime.Now)
                {
                    data = baris.Cells[1].Value.ToString();
                    return data;
                }
            }
            return data;
        }

        public void SegarkanInfo()
        {
            LTotalKas.Text = "Rp. " + AmbilTotal();
            LBarangBaru.Text = AmbilBarangBaru();
            LAgendaSelanjutnya.Text = AmbilAgendaSelanjutnya();
            LRapatSelanjutnya.Text = AmbilRapatSelanjutnya();
            DGPiketNow.DataSource = MDA.IsiDG("select nama_anggota from v_piket_now where hari = '" + MDA.KonversiHari(DateTime.Now.DayOfWeek.ToString(), "ID") + "'");
            if (DGPiketNow.RowCount > 0)
            {
                RLNoJadwal.Visible = false;
            }
            else
            {
                RLNoJadwal.Visible = true;
            }
        }

        public void Segarkan(string DataGrid)
        {
            if (DataGrid == "Struktur Anggota")
            {
                DGStrukturAngg.DataSource = MDB.IsiDG(DataGrid);
                TSSLBanyakDataStruktur.Text = DGStrukturAngg.RowCount.ToString();
                SetKeterangan();
                SetStatusMenu(LJabatan.Text, TSSLNimStruktur, TSSLBanyakDataStruktur);
            }
            else if (DataGrid == "Piket")
            {
                DGPiket.DataSource = MDB.IsiDG(DataGrid);
                TSSLBanyakDataPiket.Text = DGPiket.RowCount.ToString();
                SetStatusMenu(LJabatan.Text, TSSLNimPiket, TSSLBanyakDataPiket);
                DGPiketNow.DataSource = MDA.IsiDG("select nama_anggota from v_piket_now where hari = '" + MDA.KonversiHari(DateTime.Now.DayOfWeek.ToString(), "ID") + "'");
                if (DGPiketNow.RowCount > 0)
                {
                    RLNoJadwal.Visible = false;
                }
                else
                {
                    RLNoJadwal.Visible = true;
                }
            }
            else if (DataGrid == "Rapat")
            {
                DGRapat.DataSource = MDB.IsiDG(DataGrid);
                TSSLBanyakDataRapat.Text = DGRapat.RowCount.ToString();
                SetStatusMenu(LJabatan.Text, null, TSSLBanyakDataRapat);
                LRapatSelanjutnya.Text = AmbilRapatSelanjutnya(false);
            }
            else if (DataGrid == "Proker")
            {
                DGProker.DataSource = MDB.IsiDG(DataGrid);
                TSSLBanyakDataProker.Text = DGProker.RowCount.ToString();
                SetStatusMenu(LJabatan.Text, null, TSSLBanyakDataProker);
                LAgendaSelanjutnya.Text = AmbilAgendaSelanjutnya(false);
            }
            else if (DataGrid == "Inventaris")
            {
                DGInventaris.DataSource = MDB.IsiDG(DataGrid);
                TSSLBanyakDataInvent.Text = DGInventaris.RowCount.ToString();
                SetStatusMenu(LJabatan.Text, null, TSSLBanyakDataInvent);
                LBarangBaru.Text = AmbilBarangBaru();
            }
            else if (DataGrid == "Keuangan")
            {
                DGKeuangan.DataSource = MDB.IsiDG(DataGrid);
                TSSLTotalDana.Text = AmbilTotal();
                SetStatusMenu(LJabatan.Text, null, TSSLTotalDana);
                LTotalKas.Text = "Rp. " + AmbilTotal();
            }
            else
            {
                DGAnggota.DataSource = MDB.IsiDG(DataGrid);
                TSSLBanyakDataAnggota.Text = DGAnggota.RowCount.ToString();
                SetStatusMenu(LJabatan.Text, TSSLNimAnggota, TSSLBanyakDataAnggota);
            }
        }

        private void NonaktifkanSemua()
        {
            RMITambahA.Enabled = false;
            RMITambahA.TracksMouse = false;
            RMIUbahA.Enabled = false;
            RMIUbahA.TracksMouse = false;
            RMIHapusA.Enabled = false;
            RMIHapusA.TracksMouse = false;

            RMIUbahStruktur.Enabled = false;
            RMIUbahStruktur.TracksMouse = false;

            RMIUbahPiket.Enabled = false;
            RMIUbahPiket.TracksMouse = false;
            RMITambahPiket.Enabled = false;
            RMITambahPiket.TracksMouse = false;
            RMIHapusPiket.Enabled = false;
            RMIHapusPiket.TracksMouse = false;
            BAbsenPiket.Enabled = false;

            RMITambahRapat.Enabled = false;
            RMITambahRapat.TracksMouse = false;
            RMIUbahRapat.Enabled = false;
            RMIUbahRapat.TracksMouse = false;
            RMIHapusRapat.Enabled = false;
            RMIHapusRapat.TracksMouse = false;
            BAbsenRapat.Enabled = false;

            RMITambahProker.Enabled = false;
            RMITambahProker.TracksMouse = false;
            RMIUbahProker.Enabled = false;
            RMIUbahProker.TracksMouse = false;
            RMIHapusProker.Enabled = false;
            RMIHapusProker.TracksMouse = false;

            RMITambahInvent.Enabled = false;
            RMITambahInvent.TracksMouse = false;
            RMIUbahInvent.Enabled = false;
            RMIUbahInvent.TracksMouse = false;
            RMIHapusInvent.Enabled = false;
            RMIHapusInvent.TracksMouse = false;

            RMITambahDana.Enabled = false;
            RMITambahDana.TracksMouse = false;
            RMIUbahDana.Enabled = false;
            RMIUbahDana.TracksMouse = false;

            RMIRiwayat.Enabled = false;
            RMIRiwayat.TracksMouse = false;
        }

        private void SetStatusMenu(string Jabatan, ToolStripStatusLabel TSSLBagian, ToolStripStatusLabel BanyakData)
        {
            bool b;
            if (TSSLBagian != null)
            {
                b = TSSLBagian.Text == LNim.Text;
            }
            else
            {
                b = true;
            }

            if (int.Parse(BanyakData.Text.ToString()) > 0)
            {
                if ((Jabatan != "Wakil Ketua" && Jabatan != "Administrator" && Jabatan != "Ketua" && Jabatan != "Sekretaris") && b)
                {
                    RMITambahA.Enabled = false;
                    RMITambahA.TracksMouse = false;
                    RMIUbahA.Enabled = true;
                    RMIUbahA.TracksMouse = true;
                    RMIHapusA.Enabled = false;
                    RMIHapusA.TracksMouse = false;

                    RMIUbahStruktur.Enabled = false;
                    RMIUbahStruktur.TracksMouse = false;

                    RMIUbahPiket.Enabled = true;
                    RMIUbahPiket.TracksMouse = true;
                    RMITambahPiket.Enabled = false;
                    RMITambahPiket.TracksMouse = false;
                    RMIHapusPiket.Enabled = false;
                    RMIHapusPiket.TracksMouse = false;
                    BAbsenPiket.Enabled = true;

                    RMITambahRapat.Enabled = false;
                    RMITambahRapat.TracksMouse = false;
                    RMIUbahRapat.Enabled = false;
                    RMIUbahRapat.TracksMouse = false;
                    RMIHapusRapat.Enabled = false;
                    RMIHapusRapat.TracksMouse = false;
                    BAbsenRapat.Enabled = true;

                    RMITambahProker.Enabled = false;
                    RMITambahProker.TracksMouse = false;
                    RMIUbahProker.Enabled = false;
                    RMIUbahProker.TracksMouse = false;
                    RMIHapusProker.Enabled = false;
                    RMIHapusProker.TracksMouse = false;

                    RMITambahInvent.Enabled = false;
                    RMITambahInvent.TracksMouse = false;
                    RMIUbahInvent.Enabled = false;
                    RMIUbahInvent.TracksMouse = false;
                    RMIHapusInvent.Enabled = false;
                    RMIHapusInvent.TracksMouse = false;

                    RMITambahDana.Enabled = false;
                    RMITambahDana.TracksMouse = false;
                    RMIUbahDana.Enabled = false;
                    RMIUbahDana.TracksMouse = false;
                }
                else if (Jabatan == "Wakil Ketua" || Jabatan == "Administrator" || Jabatan == "Ketua" || Jabatan == "Sekretaris")
                {
                    RMITambahA.Enabled = true;
                    RMITambahA.TracksMouse = true;
                    RMIUbahA.Enabled = true;
                    RMIUbahA.TracksMouse = true;
                    RMIHapusA.Enabled = true;
                    RMIHapusA.TracksMouse = true;

                    RMIUbahStruktur.Enabled = true;
                    RMIUbahStruktur.TracksMouse = true;

                    RMIUbahPiket.Enabled = true;
                    RMIUbahPiket.TracksMouse = true;
                    RMITambahPiket.Enabled = true;
                    RMITambahPiket.TracksMouse = true;
                    RMIHapusPiket.Enabled = true;
                    RMIHapusPiket.TracksMouse = true;
                    BAbsenPiket.Enabled = true;

                    RMITambahRapat.Enabled = true;
                    RMITambahRapat.TracksMouse = true;
                    RMIUbahRapat.Enabled = true;
                    RMIUbahRapat.TracksMouse = true;
                    RMIHapusRapat.Enabled = true;
                    RMIHapusRapat.TracksMouse = true;
                    BAbsenRapat.Enabled = true;

                    RMITambahProker.Enabled = true;
                    RMITambahProker.TracksMouse = true;
                    RMIUbahProker.Enabled = true;
                    RMIUbahProker.TracksMouse = true;
                    RMIHapusProker.Enabled = true;
                    RMIHapusProker.TracksMouse = true;

                    RMITambahInvent.Enabled = true;
                    RMITambahInvent.TracksMouse = true;
                    RMIUbahInvent.Enabled = true;
                    RMIUbahInvent.TracksMouse = true;
                    RMIHapusInvent.Enabled = true;
                    RMIHapusInvent.TracksMouse = true;

                    RMITambahDana.Enabled = true;
                    RMITambahDana.TracksMouse = true;
                    RMIUbahDana.Enabled = true;
                    RMIUbahDana.TracksMouse = true;
                }
                else
                {
                    NonaktifkanSemua();
                }

                if (Jabatan == "Bendahara")
                {
                    RMITambahDana.Enabled = true;
                    RMITambahDana.TracksMouse = true;
                    RMIUbahDana.Enabled = true;
                    RMIUbahDana.TracksMouse = true;
                }
            }
            else if (LJabatan.Text == "Wakil Ketua" || LJabatan.Text == "Administrator" || LJabatan.Text == "Ketua" || Jabatan == "Sekretaris")
            {
                RMITambahA.Enabled = true;
                RMITambahA.TracksMouse = true;
                RMIUbahA.Enabled = false;
                RMIUbahA.TracksMouse = false;
                RMIHapusA.Enabled = false;
                RMIHapusA.TracksMouse = false;

                RMIUbahStruktur.Enabled = false;
                RMIUbahStruktur.TracksMouse = false;

                RMIUbahPiket.Enabled = false;
                RMIUbahPiket.TracksMouse = false;
                RMITambahPiket.Enabled = true;
                RMITambahPiket.TracksMouse = true;
                RMIHapusPiket.Enabled = false;
                RMIHapusPiket.TracksMouse = false;
                BAbsenPiket.Enabled = false;

                RMITambahRapat.Enabled = true;
                RMITambahRapat.TracksMouse = true;
                RMIUbahRapat.Enabled = false;
                RMIUbahRapat.TracksMouse = false;
                RMIHapusRapat.Enabled = false;
                RMIHapusRapat.TracksMouse = false;
                BAbsenRapat.Enabled = false;

                RMITambahProker.Enabled = true;
                RMITambahProker.TracksMouse = true;
                RMIUbahProker.Enabled = false;
                RMIUbahProker.TracksMouse = false;
                RMIHapusProker.Enabled = false;
                RMIHapusProker.TracksMouse = false;

                RMITambahInvent.Enabled = true;
                RMITambahInvent.TracksMouse = true;
                RMIUbahInvent.Enabled = false;
                RMIUbahInvent.TracksMouse = false;
                RMIHapusInvent.Enabled = false;
                RMIHapusInvent.TracksMouse = false;

                RMITambahDana.Enabled = true;
                RMITambahDana.TracksMouse = true;
                RMIUbahDana.Enabled = false;
                RMIUbahDana.TracksMouse = false;
            }
            else if (Jabatan == "Bendahara")
            {
                RMITambahDana.Enabled = true;
                RMITambahDana.TracksMouse = true;
                RMIUbahDana.Enabled = false;
                RMIUbahDana.TracksMouse = false;
            }
            else
            {
                NonaktifkanSemua();
            }

            if (Jabatan == "Administrator")
            {
                RMIRiwayat.Enabled = true;
                RMIRiwayat.TracksMouse = true;
            }
            else
            {
                RMIRiwayat.Enabled = false;
                RMIRiwayat.TracksMouse = false;
            }
        }

        #endregion

        #region EventFormUtama

        private void FormUtama_Aktif(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.SteelBlue;
        }

        private void Segarkan_Event(object sender, DataGridSegarkanEventArgs e)
        {
            Segarkan(e.NamaTabel);
        }

        private void Logout_Klik(object sender, EventArgs e)
        {
            MessageBoxEx.AntiAlias = true;
            MessageBoxEx.UseSystemLocalizedString = true;
            if (MessageBoxEx.Show(this, "Apa Anda yakin ingin logout ?", "Intermedia", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                MDA.Catat("Logout", "Logout");
                this.Hide();
                Form_Login frm = new Form_Login();
                frm.Show();
            }
        }

        private void BAbsenPiket_Click(object sender, EventArgs e)
        {
            int y = DGPiket.CurrentCellAddress.Y;
            string id_absen, id_piket, nim, hari, hari2, jam, jam2;

            id_absen = DB.TentID("AP", "absen_piket", "id_absen");
            nim = DGPiket[0, y].Value.ToString();
            id_piket = MDA.AmbilNilai("id_piket", "piket", "nim = '" + nim + "'");
            hari = MDA.KonversiHari(DateTime.Now.DayOfWeek.ToString());
            hari2 = DGPiket[4, y].Value.ToString();
            jam = DateTime.Now.ToShortTimeString();
            bool ada = MDA.DataAda("tanggal", "absen_piket", "nim = '" + nim + "' and tanggal ='" + MDA.AturTanggal(DateTime.Now, "EN") + "'");
            jam2 = DGPiket[5, y].Value.ToString();

            if (CekWaktuAbsenPiket(jam, jam2, hari, hari2) == false)
            {
                ToastNotification.Show(DGPiket, DGPiket[1, y].Value.ToString() + " tidak diperkenankan absen di hari ini dan jam sekarang", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                return;
            }

            if (ada)
            {
                ToastNotification.Show(DGPiket, DGPiket[1, y].Value.ToString() + " sudah absen hari ini", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                return;
            }

            string nilai = "'" + id_absen + "', '" + id_piket + "', '" + nim + "', '" + hari + "', '" + DB.AturTanggal(DateTime.Now) + "', '" + jam + "'";
            string nilai2 = "(" + id_absen + ", " + id_piket + ", " + nim + ", " + hari + ", " + DB.AturTanggal(DateTime.Now) + ", " + jam + ")";
            MDA.TambahData("absen_piket", nilai);
            MDA.Catat("Tambah Absen Piket", nilai2);

            ToastNotification.Show(DGPiket, DGPiket[1, y].Value.ToString() + " telah absen piket", eToastPosition.BottomCenter);
        }

        private void BAbsenRapat_Click(object sender, EventArgs e)
        {
            int y = DGRapat.CurrentCellAddress.Y;
            string id_absen, id_rapat, nim = LNim.Text.Replace(".", ""), jam, waktu1, waktu2;
            DateTime tanggal, tanggal2;

            id_absen = DB.TentID("AR", "absen_rapat", "id_absen");
            id_rapat = DGRapat[0, y].Value.ToString();
            jam = DateTime.Now.ToShortTimeString();
            waktu1 = DateTime.Now.ToShortTimeString();
            waktu2 = DGRapat[4, y].Value.ToString();
            tanggal = DateTime.Parse(DateTime.Now.ToShortDateString());
            tanggal2 = DateTime.Parse(DGRapat[3, y].Value.ToString());
            bool ada = MDA.DataAda("tanggal", "absen_rapat", "nim = '" + nim + "' and tanggal ='" + MDA.AturTanggal(DateTime.Now, "EN") + "'");

            if (tanggal != tanggal2 || CekWaktuAbsenRapat(waktu1, waktu2))
            {
                ToastNotification.Show(DGRapat, "Anda tidak diperkenankan absen di hari ini dan jam sekarang", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                return;
            }

            if (ada)
            {
                ToastNotification.Show(DGRapat, "Anda sudah absen hari ini", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                return;
            }

            string nilai = "'" + id_absen + "', '" + id_rapat + "', '" + nim + "', '" + DB.AturTanggal(DateTime.Now) + "', '" + jam + "'";
            string nilai2 = "(" + id_absen + ", " + id_rapat + ", " + nim + ", " + DB.AturTanggal(DateTime.Now) + ", " + jam + ")";
            MDA.TambahData("absen_rapat", nilai);
            MDA.Catat("Tambah Absen Rapat", nilai2);

            ToastNotification.Show(DGRapat, "Anda telah absen piket", eToastPosition.BottomCenter);
        }

        private void Menu_MouseEnter(object sender, EventArgs e)
        {
            RMAnggota.Symbol = "";
            RMStruktur.Symbol = "";
            RMPiket.Symbol = "";
            RMRapat.Symbol = "";
            RMProker.Symbol = "";
            RMInventaris.Symbol = "";
            RMKeuangan.Symbol = "";
        }

        private void Menu_MouseLeave(object sender, EventArgs e)
        {   
            RMAnggota.Symbol = "";
            RMStruktur.Symbol = "";
            RMPiket.Symbol = "";
            RMRapat.Symbol = "";
            RMProker.Symbol = "";
            RMInventaris.Symbol = "";
            RMKeuangan.Symbol = "";
        }

        private void RMenu_MouseEnter(object sender, EventArgs e)
        {
            RMMenu.Symbol = "";
        }

        private void RMenu_MouseLeave(object sender, EventArgs e)
        {
            RMMenu.Symbol = "";
        }

        private void DGAnggota_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int y = DGAnggota.CurrentCellAddress.Y;
            string nim, sem = DGAnggota[0, y].Value.ToString();
            try
            {
                nim = sem.Substring(0, 2) + "." + sem.Substring(2, 2) + "." + sem.Substring(4, 4);
            }
            catch (Exception)
            {
                nim = sem;
            }
            TSSLNimAnggota.Text = nim;
            LDSAnggota.Text = DGAnggota.CurrentCell.Value.ToString();
            SetStatusMenu(LJabatan.Text, TSSLNimAnggota, TSSLBanyakDataAnggota);
        }

        private void DGStrukturAngg_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int y = DGStrukturAngg.CurrentCellAddress.Y;
            string nim, sem = DGStrukturAngg[0, y].Value.ToString();
            try
            {
                nim = sem.Substring(0, 2) + "." + sem.Substring(2, 2) + "." + sem.Substring(4, 4);
            }
            catch (Exception)
            {
                nim = sem;
            }
            TSSLNimStruktur.Text = nim;
            LDSStruktur.Text = DGStrukturAngg.CurrentCell.Value.ToString();
            SetStatusMenu(LJabatan.Text, TSSLNimStruktur, TSSLBanyakDataStruktur);
        }

        private void DGPiket_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int y = DGPiket.CurrentCellAddress.Y;
            string nim, sem = DGPiket[0, y].Value.ToString();
            try
            {
                nim = sem.Substring(0, 2) + "." + sem.Substring(2, 2) + "." + sem.Substring(4, 4);
            }
            catch (Exception)
            {
                nim = sem;
            }
            TSSLNimPiket.Text = nim;
            LDSPiket.Text = DGPiket.CurrentCell.Value.ToString();
            SetStatusMenu(LJabatan.Text, TSSLNimPiket, TSSLBanyakDataPiket);
        }

        private void DGRapat_CellKlik(object sender, DataGridViewCellEventArgs e)
        {
            int y = DGRapat.CurrentCellAddress.Y;
            string id = DGRapat[0, y].Value.ToString();
            TSSLNamaRapat.Text = id;
            LDSRapat.Text = DGRapat.CurrentCell.Value.ToString();
            SetStatusMenu(LJabatan.Text, null, TSSLBanyakDataRapat);
        }

        private void DGProker_CellKlik(object sender, DataGridViewCellEventArgs e)
        {
            int y = DGProker.CurrentCellAddress.Y;
            string id = DGProker[0, y].Value.ToString();
            TSSLIdProker.Text = id;
            LDSProker.Text = DGProker.CurrentCell.Value.ToString();
            SetStatusMenu(LJabatan.Text, null, TSSLBanyakDataProker);
        }

        private void DGInventaris_CellKlik(object sender, DataGridViewCellEventArgs e)
        {
            int y = DGInventaris.CurrentCellAddress.Y;
            string id = DGInventaris[0, y].Value.ToString();
            TSSLNimInvent.Text = id;
            LDSInvent.Text = DGInventaris.CurrentCell.Value.ToString();
            SetStatusMenu(LJabatan.Text, null, TSSLBanyakDataInvent);
        }

        private void DGKeuangan_CellKlik(object sender, DataGridViewCellEventArgs e)
        {
            int y = DGKeuangan.CurrentCellAddress.Y;
            string id = DGKeuangan[0, y].Value.ToString();
            TSSLIdKeuangan.Text = id;
            LDSKeuangan.Text = DGKeuangan.CurrentCell.Value.ToString();
            SetStatusMenu(LJabatan.Text, null, TSSLTotalDana);
        }

        private void Form_Utama_Load(object sender, EventArgs e)
        {
            string nim;
            try
            {
                nim = Program.Nim.Substring(0, 2) + "." + Program.Nim.Substring(2, 2) + "." + Program.Nim.Substring(4, 4);
            }
            catch (Exception)
            {
                nim = Program.Nim; ;
            }
            LNim.Text = nim;
            LJabatan.Text = Program.Jabatan;
            LNamaP.Text = Program.Nama;

            if (LJabatan.Text == "Administrator")
            {
                DGAnggota.DataSource = DB.IsiDG("select nim, nama_anggota, kelas, alamat, j_kel, no_hp from anggota order by nama_anggota asc");
            }
            else
            {
                DGAnggota.DataSource = DB.IsiDG("select nim, nama_anggota, kelas, alamat, j_kel, no_hp from anggota where nim > '1000' order by nama_anggota asc");
            }
            SetKeterangan();
            TSSLBanyakDataAnggota.Text = DGAnggota.RowCount.ToString();
            LWaktuSekarang.Text = DateTime.Now.ToLongDateString();
            Segarkan("Keuangan");
            SegarkanInfo();
        }

        private void Tab_Klik(object sender, EventArgs e)
        {
            string tab = sender.ToString();
            Segarkan(tab);
        }

        private void FormUtama_Tampil(object sender, EventArgs e)
        {
            SetStatusMenu(LJabatan.Text, TSSLNimAnggota, TSSLBanyakDataAnggota);
        }

        private void Form_Utama_Tertutup(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
            MDA.Catat("Logout", "Logout");
        }

        #endregion

        #region EventRadialMenu

        private void RMITambahAnggota_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            Form_AturAnggota FrmUbah = new Form_AturAnggota();
            FrmUbah.DataGridSegarkan += new Form_AturAnggota.DataGridSegarkanHandler(Segarkan_Event);
            FrmUbah.SetTampilan("tambah", LJabatan.Text);
            FrmUbah.ShowDialog();
            if (FrmUbah.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGAnggota, "Data telah ditambahkan", eToastPosition.MiddleCenter);
            }
        }

        private void RMIUbahAnggota_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            int y = DGAnggota.CurrentCellAddress.Y;
            Form_AturAnggota FrmUbah = new Form_AturAnggota();
            FrmUbah.DataGridSegarkan += new Form_AturAnggota.DataGridSegarkanHandler(Segarkan_Event);
            FrmUbah.sem = y;
            FrmUbah.DataUbah = MDA.BacaDataAnggota(DGAnggota[0, y].Value.ToString());
            FrmUbah.Nim = MDA.ID(DGAnggota.Rows);
            FrmUbah.max = DGAnggota.RowCount - 1;
            FrmUbah.SetTampilan("ubah", LJabatan.Text);
            FrmUbah.ShowDialog();
            if (FrmUbah.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGAnggota, "Data telah diubah", eToastPosition.MiddleCenter);
            }
        }

        private void RMIHapusAnggota_Klik(object sender, EventArgs e)
        {
            HapusData_Utama(DGAnggota.SelectedRows, "tabel_peminjaman", "Inventaris", true);
            HapusData_Utama(DGAnggota.SelectedRows, "inventaris", "Inventaris", true);
            HapusData_Utama(DGAnggota.SelectedRows, "totalkeuangan", "Keuangan", true);
            HapusData_Utama(DGAnggota.SelectedRows, "keuangan", "Keuangan", true);
            HapusData_Utama(DGAnggota.SelectedRows, "absen_rapat", "Absen Rapat", true);
            HapusData_Utama(DGAnggota.SelectedRows, "absen_piket", "Absen Piket", true);
            HapusData_Utama(DGAnggota.SelectedRows, "piket", "Piket", true);
            HapusData_Utama(DGAnggota.SelectedRows, "struktur_anggota", "Struktur Anggota", true);
            HapusData_Utama(DGAnggota.SelectedRows, "anggota", "Anggota");
        }

        private void RMIUbahStruktur_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            int y = DGStrukturAngg.CurrentCellAddress.Y;
            Form_AturStrukturA FrmStuktur = new Form_AturStrukturA();
            FrmStuktur.DataGridSegarkan += new Form_AturStrukturA.DataGridSegarkanHandler(Segarkan_Event);
            FrmStuktur.DataUbah = MDA.BacaDataStruktur(DGStrukturAngg[0, y].Value.ToString());
            FrmStuktur.DataInfo = MDA.BacaDataAnggota(DGStrukturAngg[0, y].Value.ToString());
            FrmStuktur.SetTampilan();
            FrmStuktur.ShowDialog();
            if (FrmStuktur.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGStrukturAngg, "Data telah diubah", eToastPosition.MiddleCenter);
            }
        }

        private void RMITambahPiket_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            Form_AturPiket FrmPiket = new Form_AturPiket();
            FrmPiket.DataGridSegarkan += new Form_AturPiket.DataGridSegarkanHandler(Segarkan_Event);
            FrmPiket.SetTampilan("tambah");
            FrmPiket.Nim = MDA.ID(DGPiket.Rows);
            FrmPiket.ShowDialog();
            if (FrmPiket.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGPiket, "Data telah ditambahkan", eToastPosition.MiddleCenter);
            }
        }

        private void RMIUbahPiket_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            int y = DGPiket.CurrentCellAddress.Y;
            Form_AturPiket FrmPiket = new Form_AturPiket();
            FrmPiket.DataGridSegarkan += new Form_AturPiket.DataGridSegarkanHandler(Segarkan_Event);
            FrmPiket.DataInfo = MDA.BacaDataAnggota(DGPiket[0, y].Value.ToString());
            FrmPiket.DataUbah = MDA.BacaDataPiket(DGPiket[0, y].Value.ToString());
            FrmPiket.SetTampilan("ubah");
            FrmPiket.ShowDialog();
            if (FrmPiket.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGPiket, "Data telah diubah", eToastPosition.MiddleCenter);
            }
        }

        private void RMIHapusPiket_Klik(object sender, EventArgs e)
        {
            HapusData_Utama(DGPiket.SelectedRows, "absen_piket", "Piket", true);
            HapusData_Utama(DGPiket.SelectedRows, "piket", "Piket");
        }

        private void RMIAbsenPiket_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            Form_AbsenPiket FrmAbsen = new Form_AbsenPiket();
            FrmAbsen.DataTanggal = MDA.AmbilVarian("tanggal", "absen_piket");
            FrmAbsen.Show();
        }

        private void RMITambahRapat_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            Form_AturRapat FrmRapat = new Form_AturRapat();
            FrmRapat.DataGridSegarkan += new Form_AturRapat.DataGridSegarkanHandler(Segarkan_Event);
            FrmRapat.SetTampilan("tambah");
            FrmRapat.Status = "Belum Ditentukan";
            FrmRapat.ShowDialog();
            if (FrmRapat.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGRapat, "Data telah ditambahkan", eToastPosition.MiddleCenter);
            }
        }

        private void RMIUbahRapat_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            int y = DGRapat.CurrentCellAddress.Y;
            Form_AturRapat FrmRapat = new Form_AturRapat();
            FrmRapat.DataGridSegarkan += new Form_AturRapat.DataGridSegarkanHandler(Segarkan_Event);
            FrmRapat.DataUbah = MDA.BacaDataRapat(DGRapat[0, y].Value.ToString());
            if (DateTime.Parse(DGRapat[3, y].Value.ToString()) <= DateTime.Now)
            {
                FrmRapat.Status = "Sudah Dilaksanakan";
                FrmRapat.SetTampilan("ubah setelah");
            }
            else
            {
                FrmRapat.Status = "Belum Dilaksanakan";
                FrmRapat.SetTampilan("ubah sebelum");
            }
            FrmRapat.ShowDialog();
            if (FrmRapat.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGRapat, "Data telah ditambahkan", eToastPosition.MiddleCenter);
            }
        }

        private void RMIHapusRapat_Klik(object sender, EventArgs e)
        {
            HapusData_Utama(DGRapat.SelectedRows, "absen_rapat", "Rapat", true, "id_rapat");
            HapusData_Utama(DGRapat.SelectedRows, "rapat", "Rapat", false, "id_rapat");
        }

        private void RMIAbsenRapat_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            Form_AbsenRapat FrmRapat = new Form_AbsenRapat();
            FrmRapat.DataTanggal = MDA.AmbilVarian("tanggal", "absen_rapat");
            FrmRapat.Show();
        }

        private void RMITambahProker_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            Form_AturProker FrmProker = new Form_AturProker();
            FrmProker.DataGridSegarkan += new Form_AturProker.DataGridSegarkanHandler(Segarkan_Event);
            FrmProker.SetTampilan("tambah");
            FrmProker.ShowDialog();
            if (FrmProker.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGRapat, "Data telah ditambahkan", eToastPosition.MiddleCenter);
            }
        }

        private void RMIUbahProker_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            int y = DGProker.CurrentCellAddress.Y;
            Form_AturProker FrmProker = new Form_AturProker();
            FrmProker.DataGridSegarkan += new Form_AturProker.DataGridSegarkanHandler(Segarkan_Event);
            FrmProker.DataUbah = MDA.BacaDataProker(DGProker[0, y].Value.ToString());
            FrmProker.SetTampilan("ubah");
            FrmProker.ShowDialog();
            if (FrmProker.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGPiket, "Data telah diubah", eToastPosition.MiddleCenter);
            }
        }

        private void RMIHapusProker_Klik(object sender, EventArgs e)
        {
            HapusData_Utama(DGProker.SelectedRows, "program_kerja", "Proker", false, "id_proker");
        }

        private void RMITambahInvent_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            Form_AturInventaris FrmInvent = new Form_AturInventaris();
            FrmInvent.DataGridSegarkan += new Form_AturInventaris.DataGridSegarkanHandler(Segarkan_Event);
            FrmInvent.SetTampilan("tambah");
            FrmInvent.ShowDialog();
            if (FrmInvent.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGRapat, "Data telah ditambahkan", eToastPosition.MiddleCenter);
            }
        }

        private void RMIUbahInvent_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            int y = DGInventaris.CurrentCellAddress.Y;
            Form_AturInventaris FrmInvent = new Form_AturInventaris();
            FrmInvent.DataGridSegarkan += new Form_AturInventaris.DataGridSegarkanHandler(Segarkan_Event);
            FrmInvent.DataUbah = MDA.BacaDataInvent(DGInventaris[0, y].Value.ToString());
            FrmInvent.SetTampilan("ubah");
            FrmInvent.ShowDialog();
            if (FrmInvent.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGInventaris, "Data telah diubah", eToastPosition.MiddleCenter);
            }
        }

        private void RMIHapusInvent_Klik(object sender, EventArgs e)
        {
            HapusData_Utama(DGInventaris.SelectedRows, "tabel_peminjaman", "Inventaris", true, "id_barang");
            HapusData_Utama(DGInventaris.SelectedRows, "inventaris", "Inventaris", false, "id_barang");
        }

        private void RMIPeminjaman_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            Form_AturPeminjaman FrmPeminjaman = new Form_AturPeminjaman();
            FrmPeminjaman.nim = LNim.Text;
            FrmPeminjaman.jabatan = LJabatan.Text;
            FrmPeminjaman.nama = LNamaP.Text;
            FrmPeminjaman.Show();
        }

        private void RMITambahDataKeuangan_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            int y = DGKeuangan.CurrentCellAddress.Y;
            Form_AturKeuangan FrmKeuangan = new Form_AturKeuangan();
            FrmKeuangan.DataGridSegarkan += new Form_AturKeuangan.DataGridSegarkanHandler(Segarkan_Event);
            FrmKeuangan.SetTampilan("tambah");
            int total;
            try
            {
                total = int.Parse(MDA.AmbilTeratasDariOrder("totalkeuangan", "total", "id_keuangan", "desc"));
            }
            catch (Exception)
            {
                total = 0;
            }
            FrmKeuangan.total = total;
            FrmKeuangan.ShowDialog();
            if (FrmKeuangan.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGRapat, "Data telah ditambahkan", eToastPosition.MiddleCenter);
            }
        }

        private void RMIUbahDataKeuangan_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            int maks = DGKeuangan.RowCount - 1;
            int y = DGKeuangan.CurrentCellAddress.Y;
            Form_AturKeuangan FrmKeuangan = new Form_AturKeuangan();
            FrmKeuangan.DataGridSegarkan += new Form_AturKeuangan.DataGridSegarkanHandler(Segarkan_Event);
            FrmKeuangan.DataUbah = MDA.BacaDataKeuangan(DGKeuangan[0, y].Value.ToString());
            if (maks != y)
            {
                FrmKeuangan.SetTampilan("ubahl");
            }
            else
            {
                FrmKeuangan.SetTampilan("ubah");
            }
            FrmKeuangan.total = int.Parse(MDA.AmbilNilai("total", "totalkeuangan", "id_keuangan = '" + DGKeuangan[0, y].Value.ToString() + "'"));
            FrmKeuangan.ShowDialog();
            if (FrmKeuangan.DialogResult == System.Windows.Forms.DialogResult.Yes)
            {
                ToastNotification.Show(DGRapat, "Data telah diubah", eToastPosition.MiddleCenter);
            }
        }

        private void RMITentang_Klik(object sender, EventArgs e)
        {
            metroStatusBar1.BackgroundStyle.BackColor = Color.Chocolate;
            Form_Tentang FrmTentang = new Form_Tentang();
            FrmTentang.ShowDialog();
        }

        private void RMIRiwayat_Klik(object sender, EventArgs e)
        {
            Form_RiwayatAplikasi FrmTentang = new Form_RiwayatAplikasi();
            FrmTentang.Show();
        }

        #endregion
    }
}
