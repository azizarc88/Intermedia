﻿namespace Intermedia.Form
{
    partial class Form_AturAnggota
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BPrev = new DevComponents.DotNetBar.ButtonX();
            this.BNext = new DevComponents.DotNetBar.ButtonX();
            this.RequiredFieldValidator1 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Nim harus diisi");
            this.LabelX8 = new DevComponents.DotNetBar.LabelX();
            this.TBRePass = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.CBOtoClose = new DevComponents.DotNetBar.CheckBoxItem();
            this.BSimpan = new DevComponents.DotNetBar.ButtonX();
            this.BTutup = new DevComponents.DotNetBar.ButtonX();
            this.LabelX7 = new DevComponents.DotNetBar.LabelX();
            this.TBPass = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.LabelX6 = new DevComponents.DotNetBar.LabelX();
            this.TBPassLama = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TBNoTelp = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.LabelX5 = new DevComponents.DotNetBar.LabelX();
            this.LabelX4 = new DevComponents.DotNetBar.LabelX();
            this.LabelX3 = new DevComponents.DotNetBar.LabelX();
            this.TBAlamat = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.LabelX2 = new DevComponents.DotNetBar.LabelX();
            this.LabelX1 = new DevComponents.DotNetBar.LabelX();
            this.TBNama = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.CBGender = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.styleManagerAmbient1 = new DevComponents.DotNetBar.StyleManagerAmbient(this.components);
            this.TBKelas = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TBNim = new DevComponents.DotNetBar.Controls.MaskedTextBoxAdv();
            this.LNamaAnggota = new DevComponents.DotNetBar.LabelX();
            this.SuspendLayout();
            // 
            // BPrev
            // 
            this.BPrev.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BPrev.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BPrev.Enabled = false;
            this.BPrev.Location = new System.Drawing.Point(336, 169);
            this.BPrev.Name = "BPrev";
            this.BPrev.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BPrev.Size = new System.Drawing.Size(22, 34);
            this.BPrev.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BPrev.Symbol = "";
            this.BPrev.SymbolSize = 15F;
            this.BPrev.TabIndex = 47;
            this.BPrev.Click += new System.EventHandler(this.BPrev_Click);
            // 
            // BNext
            // 
            this.BNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BNext.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BNext.Enabled = false;
            this.BNext.Location = new System.Drawing.Point(622, 169);
            this.BNext.Name = "BNext";
            this.BNext.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BNext.Size = new System.Drawing.Size(22, 34);
            this.BNext.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BNext.Symbol = "";
            this.BNext.SymbolSize = 15F;
            this.BNext.TabIndex = 46;
            this.BNext.Click += new System.EventHandler(this.BNext_Click);
            // 
            // RequiredFieldValidator1
            // 
            this.RequiredFieldValidator1.ErrorMessage = "Nim harus diisi";
            this.RequiredFieldValidator1.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Orange;
            // 
            // LabelX8
            // 
            this.LabelX8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX8.ForeColor = System.Drawing.Color.Black;
            this.LabelX8.Location = new System.Drawing.Point(336, 135);
            this.LabelX8.Name = "LabelX8";
            this.LabelX8.Size = new System.Drawing.Size(100, 23);
            this.LabelX8.TabIndex = 44;
            this.LabelX8.Text = "Ulangi Password";
            // 
            // TBRePass
            // 
            this.TBRePass.AutoSelectAll = true;
            this.TBRePass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBRePass.Border.Class = "TextBoxBorder";
            this.TBRePass.Border.CornerDiameter = 2;
            this.TBRePass.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBRePass.Border.TextShadowColor = System.Drawing.SystemColors.Highlight;
            this.TBRePass.Border.TextShadowOffset = new System.Drawing.Point(2, 2);
            this.styleManagerAmbient1.SetEnableAmbientSettings(this.TBRePass, DevComponents.DotNetBar.eAmbientSettings.None);
            this.TBRePass.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBRePass, true);
            this.TBRePass.Location = new System.Drawing.Point(436, 136);
            this.TBRePass.MaxLength = 16;
            this.TBRePass.Name = "TBRePass";
            this.TBRePass.PasswordChar = '●';
            this.TBRePass.Size = new System.Drawing.Size(206, 22);
            this.TBRePass.TabIndex = 38;
            this.TBRePass.TextChanged += new System.EventHandler(this.TBRePass_IsiBerubah);
            this.TBRePass.Enter += new System.EventHandler(this.TBRePass_TerFokus);
            // 
            // CBOtoClose
            // 
            this.CBOtoClose.Checked = true;
            this.CBOtoClose.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBOtoClose.GlobalItem = false;
            this.CBOtoClose.Name = "CBOtoClose";
            this.CBOtoClose.Stretch = true;
            this.CBOtoClose.Text = "Tutup otomatis";
            // 
            // BSimpan
            // 
            this.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BSimpan.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BSimpan.Location = new System.Drawing.Point(494, 169);
            this.BSimpan.Name = "BSimpan";
            this.BSimpan.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BSimpan.Size = new System.Drawing.Size(126, 34);
            this.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BSimpan.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.CBOtoClose});
            this.BSimpan.SubItemsExpandWidth = 18;
            this.BSimpan.TabIndex = 39;
            this.BSimpan.Text = "Tambah";
            this.BSimpan.Click += new System.EventHandler(this.BSimpan_Click);
            // 
            // BTutup
            // 
            this.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BTutup.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.Location = new System.Drawing.Point(360, 169);
            this.BTutup.Name = "BTutup";
            this.BTutup.PopupSide = DevComponents.DotNetBar.ePopupSide.Bottom;
            this.BTutup.Size = new System.Drawing.Size(126, 34);
            this.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BTutup.TabIndex = 40;
            this.BTutup.Text = "Batal";
            // 
            // LabelX7
            // 
            this.LabelX7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX7.ForeColor = System.Drawing.Color.Black;
            this.LabelX7.Location = new System.Drawing.Point(336, 102);
            this.LabelX7.Name = "LabelX7";
            this.LabelX7.Size = new System.Drawing.Size(100, 23);
            this.LabelX7.TabIndex = 43;
            this.LabelX7.Text = "Password";
            // 
            // TBPass
            // 
            this.TBPass.AcceptsReturn = true;
            this.TBPass.AutoSelectAll = true;
            this.TBPass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBPass.Border.Class = "TextBoxBorder";
            this.TBPass.Border.CornerDiameter = 2;
            this.TBPass.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBPass.Border.TextShadowColor = System.Drawing.SystemColors.Highlight;
            this.TBPass.Border.TextShadowOffset = new System.Drawing.Point(2, 2);
            this.TBPass.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBPass, true);
            this.TBPass.Location = new System.Drawing.Point(436, 103);
            this.TBPass.MaxLength = 16;
            this.TBPass.Name = "TBPass";
            this.TBPass.PasswordChar = '●';
            this.TBPass.Size = new System.Drawing.Size(206, 22);
            this.TBPass.TabIndex = 36;
            // 
            // LabelX6
            // 
            this.LabelX6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX6.ForeColor = System.Drawing.Color.Black;
            this.LabelX6.Location = new System.Drawing.Point(336, 70);
            this.LabelX6.Name = "LabelX6";
            this.LabelX6.Size = new System.Drawing.Size(100, 23);
            this.LabelX6.TabIndex = 42;
            this.LabelX6.Text = "Password Lama";
            // 
            // TBPassLama
            // 
            this.TBPassLama.AutoSelectAll = true;
            this.TBPassLama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBPassLama.Border.Class = "TextBoxBorder";
            this.TBPassLama.Border.CornerDiameter = 2;
            this.TBPassLama.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBPassLama.Border.TextShadowColor = System.Drawing.SystemColors.Highlight;
            this.TBPassLama.Border.TextShadowOffset = new System.Drawing.Point(2, 2);
            this.TBPassLama.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBPassLama, true);
            this.TBPassLama.Location = new System.Drawing.Point(436, 71);
            this.TBPassLama.MaxLength = 16;
            this.TBPassLama.Name = "TBPassLama";
            this.TBPassLama.PasswordChar = '●';
            this.TBPassLama.Size = new System.Drawing.Size(206, 22);
            this.TBPassLama.TabIndex = 35;
            // 
            // TBNoTelp
            // 
            this.TBNoTelp.AllowPromptAsInput = false;
            this.TBNoTelp.AntiAlias = true;
            this.TBNoTelp.AsciiOnly = true;
            this.TBNoTelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBNoTelp.BackgroundStyle.Class = "TextBoxBorder";
            this.TBNoTelp.BackgroundStyle.CornerDiameter = 2;
            this.TBNoTelp.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBNoTelp.BeepOnError = true;
            this.TBNoTelp.ButtonClear.Visible = true;
            this.TBNoTelp.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TBNoTelp.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.TBNoTelp.ForeColor = System.Drawing.Color.Black;
            this.TBNoTelp.HidePromptOnLeave = true;
            this.highlighter1.SetHighlightOnFocus(this.TBNoTelp, true);
            this.TBNoTelp.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.TBNoTelp.Location = new System.Drawing.Point(436, 38);
            this.TBNoTelp.Mask = "000000000000";
            this.TBNoTelp.Name = "TBNoTelp";
            this.TBNoTelp.PromptChar = ' ';
            this.TBNoTelp.ResetOnPrompt = false;
            this.TBNoTelp.ResetOnSpace = false;
            this.TBNoTelp.Size = new System.Drawing.Size(206, 22);
            this.TBNoTelp.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TBNoTelp.TabIndex = 33;
            this.TBNoTelp.Text = "";
            this.TBNoTelp.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // LabelX5
            // 
            this.LabelX5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX5.ForeColor = System.Drawing.Color.Black;
            this.LabelX5.Location = new System.Drawing.Point(336, 36);
            this.LabelX5.Name = "LabelX5";
            this.LabelX5.Size = new System.Drawing.Size(100, 23);
            this.LabelX5.TabIndex = 41;
            this.LabelX5.Text = "No. Handphone";
            // 
            // LabelX4
            // 
            this.LabelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX4.ForeColor = System.Drawing.Color.Black;
            this.LabelX4.Location = new System.Drawing.Point(336, 8);
            this.LabelX4.Name = "LabelX4";
            this.LabelX4.Size = new System.Drawing.Size(100, 23);
            this.LabelX4.TabIndex = 37;
            this.LabelX4.Text = "Jenis Kelamin";
            // 
            // LabelX3
            // 
            this.LabelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX3.ForeColor = System.Drawing.Color.Black;
            this.LabelX3.Location = new System.Drawing.Point(8, 93);
            this.LabelX3.Name = "LabelX3";
            this.LabelX3.Size = new System.Drawing.Size(100, 23);
            this.LabelX3.TabIndex = 34;
            this.LabelX3.Text = "Alamat";
            // 
            // TBAlamat
            // 
            this.TBAlamat.AutoSelectAll = true;
            this.TBAlamat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBAlamat.Border.Class = "TextBoxBorder";
            this.TBAlamat.Border.CornerDiameter = 2;
            this.TBAlamat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBAlamat.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBAlamat, true);
            this.TBAlamat.Location = new System.Drawing.Point(108, 94);
            this.TBAlamat.Multiline = true;
            this.TBAlamat.Name = "TBAlamat";
            this.TBAlamat.Size = new System.Drawing.Size(206, 106);
            this.TBAlamat.TabIndex = 30;
            // 
            // LabelX2
            // 
            this.LabelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX2.ForeColor = System.Drawing.Color.Black;
            this.LabelX2.Location = new System.Drawing.Point(8, 65);
            this.LabelX2.Name = "LabelX2";
            this.LabelX2.Size = new System.Drawing.Size(100, 23);
            this.LabelX2.TabIndex = 32;
            this.LabelX2.Text = "Kelas";
            // 
            // LabelX1
            // 
            this.LabelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LabelX1.ForeColor = System.Drawing.Color.Black;
            this.LabelX1.Location = new System.Drawing.Point(8, 37);
            this.LabelX1.Name = "LabelX1";
            this.LabelX1.Size = new System.Drawing.Size(100, 23);
            this.LabelX1.TabIndex = 28;
            this.LabelX1.Text = "Nama";
            // 
            // TBNama
            // 
            this.TBNama.AutoSelectAll = true;
            this.TBNama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBNama.Border.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBNama.Border.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBNama.Border.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBNama.Border.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.TBNama.Border.Class = "TextBoxBorder";
            this.TBNama.Border.CornerDiameter = 2;
            this.TBNama.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBNama.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBNama, true);
            this.TBNama.Location = new System.Drawing.Point(108, 38);
            this.TBNama.MaxLength = 25;
            this.TBNama.Name = "TBNama";
            this.TBNama.Size = new System.Drawing.Size(206, 22);
            this.TBNama.TabIndex = 27;
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // CBGender
            // 
            this.CBGender.DisplayMember = "Text";
            this.CBGender.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CBGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBGender.ForeColor = System.Drawing.Color.Black;
            this.CBGender.FormattingEnabled = true;
            this.highlighter1.SetHighlightOnFocus(this.CBGender, true);
            this.CBGender.ItemHeight = 14;
            this.CBGender.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2});
            this.CBGender.Location = new System.Drawing.Point(436, 8);
            this.CBGender.Name = "CBGender";
            this.CBGender.Size = new System.Drawing.Size(206, 20);
            this.CBGender.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.CBGender.TabIndex = 32;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "L";
            this.comboItem1.Value = "L";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "P";
            this.comboItem2.Value = "P";
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242))))), System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204))))));
            // 
            // TBKelas
            // 
            this.TBKelas.AutoSelectAll = true;
            this.TBKelas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBKelas.Border.Class = "TextBoxBorder";
            this.TBKelas.Border.CornerDiameter = 2;
            this.TBKelas.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBKelas.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBKelas, true);
            this.TBKelas.Location = new System.Drawing.Point(108, 66);
            this.TBKelas.MaxLength = 7;
            this.TBKelas.Name = "TBKelas";
            this.TBKelas.Size = new System.Drawing.Size(206, 22);
            this.TBKelas.TabIndex = 29;
            // 
            // TBNim
            // 
            this.TBNim.AllowPromptAsInput = false;
            this.TBNim.AntiAlias = true;
            this.TBNim.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBNim.BackgroundStyle.Class = "TextBoxBorder";
            this.TBNim.BackgroundStyle.CornerDiameter = 2;
            this.TBNim.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.TBNim.ButtonClear.Visible = true;
            this.styleManagerAmbient1.SetEnableAmbientSettings(this.TBNim, DevComponents.DotNetBar.eAmbientSettings.None);
            this.TBNim.ForeColor = System.Drawing.Color.Black;
            this.TBNim.HidePromptOnLeave = true;
            this.highlighter1.SetHighlightOnFocus(this.TBNim, true);
            this.TBNim.Location = new System.Drawing.Point(108, 10);
            this.TBNim.Mask = "00.00.0000";
            this.TBNim.Name = "TBNim";
            this.TBNim.Size = new System.Drawing.Size(206, 22);
            this.TBNim.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.TBNim.TabIndex = 26;
            this.TBNim.Text = "";
            this.TBNim.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // LNamaAnggota
            // 
            this.LNamaAnggota.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNamaAnggota.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNamaAnggota.ForeColor = System.Drawing.Color.Black;
            this.LNamaAnggota.Location = new System.Drawing.Point(8, 8);
            this.LNamaAnggota.Name = "LNamaAnggota";
            this.LNamaAnggota.Size = new System.Drawing.Size(100, 23);
            this.LNamaAnggota.TabIndex = 25;
            this.LNamaAnggota.Text = "Nim";
            // 
            // Form_AturAnggota
            // 
            this.AcceptButton = this.BSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BTutup;
            this.ClientSize = new System.Drawing.Size(646, 206);
            this.ControlBox = false;
            this.Controls.Add(this.CBGender);
            this.Controls.Add(this.BPrev);
            this.Controls.Add(this.BNext);
            this.Controls.Add(this.TBNim);
            this.Controls.Add(this.LabelX8);
            this.Controls.Add(this.TBRePass);
            this.Controls.Add(this.BSimpan);
            this.Controls.Add(this.BTutup);
            this.Controls.Add(this.LabelX7);
            this.Controls.Add(this.TBPass);
            this.Controls.Add(this.LabelX6);
            this.Controls.Add(this.TBPassLama);
            this.Controls.Add(this.TBNoTelp);
            this.Controls.Add(this.LabelX5);
            this.Controls.Add(this.LabelX4);
            this.Controls.Add(this.LabelX3);
            this.Controls.Add(this.TBAlamat);
            this.Controls.Add(this.LabelX2);
            this.Controls.Add(this.TBKelas);
            this.Controls.Add(this.LabelX1);
            this.Controls.Add(this.TBNama);
            this.Controls.Add(this.LNamaAnggota);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form_AturAnggota";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ubah Data Anggota";
            this.Shown += new System.EventHandler(this.Ubah_Anggota_Tampil);
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.ButtonX BPrev;
        internal DevComponents.DotNetBar.ButtonX BNext;
        internal DevComponents.DotNetBar.Validator.RequiredFieldValidator RequiredFieldValidator1;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
        internal DevComponents.DotNetBar.LabelX LabelX8;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBRePass;
        internal DevComponents.DotNetBar.ButtonX BSimpan;
        internal DevComponents.DotNetBar.CheckBoxItem CBOtoClose;
        internal DevComponents.DotNetBar.ButtonX BTutup;
        internal DevComponents.DotNetBar.LabelX LabelX7;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBPass;
        internal DevComponents.DotNetBar.LabelX LabelX6;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBPassLama;
        internal DevComponents.DotNetBar.Controls.MaskedTextBoxAdv TBNoTelp;
        internal DevComponents.DotNetBar.LabelX LabelX5;
        internal DevComponents.DotNetBar.LabelX LabelX4;
        internal DevComponents.DotNetBar.LabelX LabelX3;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBAlamat;
        internal DevComponents.DotNetBar.LabelX LabelX2;
        internal DevComponents.DotNetBar.LabelX LabelX1;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBNama;
        public DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.StyleManagerAmbient styleManagerAmbient1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx CBGender;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        internal DevComponents.DotNetBar.Controls.MaskedTextBoxAdv TBNim;
        internal DevComponents.DotNetBar.Controls.TextBoxX TBKelas;
        internal DevComponents.DotNetBar.LabelX LNamaAnggota;
    }
}