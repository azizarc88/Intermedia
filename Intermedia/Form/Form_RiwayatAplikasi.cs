﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_RiwayatAplikasi : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();

        public Form_RiwayatAplikasi()
        {
            InitializeComponent();
        }

        private void Form_RiwayatAplikasi_Load(object sender, EventArgs e)
        {
            DGRiwayat.DataSource = MDA.IsiDG("select * from riwayat_aplikasi order by date_time desc");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DGRiwayat.DataSource = MDA.IsiDG("select * from riwayat_aplikasi order by date_time desc");
        }
    }
}
