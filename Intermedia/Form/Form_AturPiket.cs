﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_AturPiket : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();

        public delegate void DataGridSegarkanHandler(object sender, DataGridSegarkanEventArgs e);
        public event DataGridSegarkanHandler DataGridSegarkan;

        public List<string> DataUbah = new List<string>();
        public List<string> DataInfo = new List<string>();
        public List<string> Nim = new List<string>();

        public Form_AturPiket()
        {
            InitializeComponent();
        }

        internal void SetTampilan(string UbahATAUTambah)
        {
            if (UbahATAUTambah == "ubah")
            {
                BSimpan.Text = "Ubah";
                BPilih.Enabled = false;
                TBNim.ReadOnly = true;
                TBNim.ButtonClear.Enabled = false;
                TBNim.BackColor = Color.LightSteelBlue;
                this.Text = "Ubah Data Piket";

                LId.Text = DataUbah[0];
                TBNim.Text = DataUbah[1];
                CBHari.Text = DataUbah[2];
                CBJam.Text = DataUbah[3];

                LNama.Text = DataInfo[1];
                LKelas.Text = DataInfo[2];
                LAlamat.Text = DataInfo[3];
                LNoTelp.Text = DataInfo[5];
            }
            else
            {
                BSimpan.Text = "Tambah";
                BPilih.Enabled = true;
                TBNim.ReadOnly = false;
                TBNim.ButtonClear.Enabled = true;
                TBNim.BackColor = Color.FromArgb(239, 239, 242);
                LId.Text = MDA.TentID("PK", "piket", "id_piket");
                this.Text = "Tambah Data Piket";
            }
        }

        private bool CekKelengkapan()
        {
            if (TBNim.Text == "")
            {
                ToastNotification.Show(this, "Nim dari anggota harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBNim.Focus();
                return false;
            }
            else if (CBHari.SelectedItem.ToString() == "")
            {
                ToastNotification.Show(this, "Hari piket harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                CBHari.Focus();
                return false;
            }
            else if (CBJam.SelectedItem.ToString() == "")
            {
                ToastNotification.Show(this, "Jam piket harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                CBHari.Focus();
                return false;
            }
            return true;
        }

        private void Form_AturPiket_Tampil(object sender, EventArgs e)
        {
            if (BSimpan.Text == "Ubah")
            {
                CBHari.Focus();
            }
            else
            {
                TBNim.Focus();
            }
        }

        private void BSimpan_Click(object sender, EventArgs e)
        {
            if (BSimpan.Text == "Ubah")
            {
                if (CekKelengkapan() == false)
                {
                    return;
                }

                string nilai, nilaiC;
                nilai = "nim = '" + TBNim.Text + "', hari = '" + CBHari.SelectedItem.ToString() + "', waktu = '" + CBJam.SelectedItem.ToString() + "'";
                nilaiC = "(" + LId.Text + ", " + TBNim.Text + ", " + CBHari.SelectedItem.ToString() + ", " + CBJam.SelectedItem.ToString() + ")";
                MDA.UbahData("piket", nilai, "id_piket = '" + LId.Text + "'");
                MDA.Catat("Ubah Data Piket", nilaiC);
            }
            else if (BSimpan.Text == "Tambah")
            {
                if (CekKelengkapan() == false)
                {
                    return;
                }

                if (MDA.DataAda("nim", "piket", "nim = '" + TBNim.Text + "'") == true)
                {
                    ToastNotification.Show(this, "Data dengan nim \"" + TBNim.Text + "\" sudah ada", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                    TBNim.Focus();
                    return;
                }

                string nilai, nilaiC;
                nilai = "'" + LId.Text + "', '" + TBNim.Text + "', '" + CBHari.SelectedItem.ToString() + "', '" + CBJam.SelectedItem.ToString() + "'";
                nilaiC = "(" + LId.Text + ", " + TBNim.Text + ", " + CBHari.SelectedItem.ToString() + ", " + CBJam.SelectedItem.ToString() + ")";
                MDA.TambahData("piket", nilai);
                MDA.Catat("Tambah Data Piket", nilaiC);
            }

            DataGridSegarkanEventArgs args = new DataGridSegarkanEventArgs("Piket");
            DataGridSegarkan(this, args);

            Nim.Add(TBNim.Text);

            LId.Text = MDA.TentID("PK", "piket", "id_piket");

            if (CBOtoClose.Checked == true)
            {
                this.Close();
                this.DialogResult = System.Windows.Forms.DialogResult.Yes;
            }

            ToastNotification.Show(this, LNama.Text + " telah memiliki jadwal piket", eToastPosition.BottomCenter);
        }

        private void BPilih_Click(object sender, EventArgs e)
        {
            Form_Atur_Pilih FrmPilihItem = new Form_Atur_Pilih();
            FrmPilihItem.ItemTerpilih += new Form_Atur_Pilih.ItemTerpilihHandler(ItemTerpilih);
            FrmPilihItem.ID = Nim;
            FrmPilihItem.Query = "select nim, nama_anggota, kelas, alamat, j_kel, no_hp from anggota where nim > '1000' order by nama_anggota asc";
            FrmPilihItem.ShowDialog();
        }

        private void ItemTerpilih(object sender, ItemTerpilihEventArgs e)
        {
            CBHari.Focus();

            TBNim.Text = e.Daftar[0];
            LNama.Text = e.Daftar[1];
            LKelas.Text = e.Daftar[2];
            LAlamat.Text = e.Daftar[3];
            LNoTelp.Text = e.Daftar[5];
        }
    }
}
