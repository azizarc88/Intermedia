﻿namespace Intermedia.Form
{
    partial class Form_AturProker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LNoID = new DevComponents.DotNetBar.LabelX();
            this.LId = new DevComponents.DotNetBar.LabelX();
            this.styleManagerAmbient1 = new DevComponents.DotNetBar.StyleManagerAmbient(this.components);
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.TBNama = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.TBDana = new DevComponents.Editors.IntegerInput();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.TBKeterangan = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.BSimpan = new DevComponents.DotNetBar.ButtonX();
            this.BTutup = new DevComponents.DotNetBar.ButtonX();
            this.DTITanggal = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            ((System.ComponentModel.ISupportInitialize)(this.TBDana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTITanggal)).BeginInit();
            this.SuspendLayout();
            // 
            // LNoID
            // 
            this.LNoID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LNoID.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LNoID.ForeColor = System.Drawing.Color.Black;
            this.LNoID.Location = new System.Drawing.Point(12, 4);
            this.LNoID.Name = "LNoID";
            this.LNoID.Size = new System.Drawing.Size(100, 23);
            this.LNoID.TabIndex = 99;
            this.LNoID.Text = "ID";
            // 
            // LId
            // 
            this.LId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.LId.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.LId.ForeColor = System.Drawing.Color.Black;
            this.LId.Location = new System.Drawing.Point(112, 4);
            this.LId.Name = "LId";
            this.LId.Size = new System.Drawing.Size(168, 23);
            this.LId.TabIndex = 98;
            this.LId.Text = "PR001";
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.ForeColor = System.Drawing.Color.Black;
            this.labelX1.Location = new System.Drawing.Point(12, 33);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(100, 23);
            this.labelX1.TabIndex = 97;
            this.labelX1.Text = "Nama Proker";
            // 
            // TBNama
            // 
            this.TBNama.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBNama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBNama.Border.Class = "TextBoxBorder";
            this.TBNama.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TBNama.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBNama, true);
            this.TBNama.Location = new System.Drawing.Point(112, 34);
            this.TBNama.Name = "TBNama";
            this.TBNama.Size = new System.Drawing.Size(239, 20);
            this.TBNama.TabIndex = 72;
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.ForeColor = System.Drawing.Color.Black;
            this.labelX2.Location = new System.Drawing.Point(12, 62);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(100, 23);
            this.labelX2.TabIndex = 96;
            this.labelX2.Text = "Tanggal";
            // 
            // TBDana
            // 
            this.TBDana.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBDana.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBDana.BackgroundStyle.Class = "DateTimeInputBackground";
            this.TBDana.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TBDana.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.TBDana.DisplayFormat = "Rp .,00";
            this.TBDana.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBDana, true);
            this.TBDana.Increment = 1000;
            this.TBDana.Location = new System.Drawing.Point(112, 92);
            this.TBDana.Name = "TBDana";
            this.TBDana.ShowUpDown = true;
            this.TBDana.Size = new System.Drawing.Size(239, 20);
            this.TBDana.TabIndex = 74;
            // 
            // labelX3
            // 
            this.labelX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.ForeColor = System.Drawing.Color.Black;
            this.labelX3.Location = new System.Drawing.Point(12, 91);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(100, 23);
            this.labelX3.TabIndex = 95;
            this.labelX3.Text = "Dana Dibutuhkan";
            // 
            // labelX4
            // 
            this.labelX4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.ForeColor = System.Drawing.Color.Black;
            this.labelX4.Location = new System.Drawing.Point(12, 120);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(100, 23);
            this.labelX4.TabIndex = 94;
            this.labelX4.Text = "Keterangan";
            // 
            // TBKeterangan
            // 
            this.TBKeterangan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBKeterangan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.TBKeterangan.Border.Class = "TextBoxBorder";
            this.TBKeterangan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.TBKeterangan.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.TBKeterangan, true);
            this.TBKeterangan.Location = new System.Drawing.Point(112, 121);
            this.TBKeterangan.Multiline = true;
            this.TBKeterangan.Name = "TBKeterangan";
            this.TBKeterangan.Size = new System.Drawing.Size(239, 108);
            this.TBKeterangan.TabIndex = 75;
            // 
            // BSimpan
            // 
            this.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BSimpan.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.BSimpan.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BSimpan.Location = new System.Drawing.Point(191, 244);
            this.BSimpan.Name = "BSimpan";
            this.BSimpan.Size = new System.Drawing.Size(142, 34);
            this.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BSimpan.SubItemsExpandWidth = 18;
            this.BSimpan.TabIndex = 77;
            this.BSimpan.Text = "Simpan";
            this.BSimpan.Click += new System.EventHandler(this.BSimpan_Click);
            // 
            // BTutup
            // 
            this.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BTutup.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.BTutup.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueOrb;
            this.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTutup.Location = new System.Drawing.Point(27, 244);
            this.BTutup.Name = "BTutup";
            this.BTutup.Size = new System.Drawing.Size(142, 34);
            this.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.BTutup.TabIndex = 76;
            this.BTutup.Text = "Batal";
            // 
            // DTITanggal
            // 
            this.DTITanggal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DTITanggal.AntiAlias = true;
            this.DTITanggal.AutoAdvance = true;
            this.DTITanggal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(242)))));
            // 
            // 
            // 
            this.DTITanggal.BackgroundStyle.Class = "DateTimeInputBackground";
            this.DTITanggal.BackgroundStyle.CornerDiameter = 2;
            this.DTITanggal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.DTITanggal.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.DTITanggal.ButtonDropDown.Visible = true;
            this.DTITanggal.ForeColor = System.Drawing.Color.Black;
            this.highlighter1.SetHighlightOnFocus(this.DTITanggal, true);
            this.DTITanggal.IsPopupCalendarOpen = false;
            this.DTITanggal.Location = new System.Drawing.Point(112, 63);
            // 
            // 
            // 
            this.DTITanggal.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTITanggal.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTITanggal.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.DTITanggal.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.DTITanggal.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTITanggal.MonthCalendar.DisplayMonth = new System.DateTime(2014, 2, 1, 0, 0, 0, 0);
            this.DTITanggal.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.DTITanggal.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.DTITanggal.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.DTITanggal.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.DTITanggal.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.DTITanggal.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.DTITanggal.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.DTITanggal.MonthCalendar.TodayButtonVisible = true;
            this.DTITanggal.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.DTITanggal.Name = "DTITanggal";
            this.DTITanggal.Size = new System.Drawing.Size(239, 20);
            this.DTITanggal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.DTITanggal.TabIndex = 73;
            this.DTITanggal.TimeSelectorTimeFormat = DevComponents.Editors.DateTimeAdv.eTimeSelectorFormat.Time12H;
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // Form_AturProker
            // 
            this.AcceptButton = this.BSimpan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BTutup;
            this.ClientSize = new System.Drawing.Size(362, 287);
            this.ControlBox = false;
            this.Controls.Add(this.DTITanggal);
            this.Controls.Add(this.BSimpan);
            this.Controls.Add(this.BTutup);
            this.Controls.Add(this.TBKeterangan);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX3);
            this.Controls.Add(this.TBDana);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.TBNama);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.LNoID);
            this.Controls.Add(this.LId);
            this.DoubleBuffered = true;
            this.Name = "Form_AturProker";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tambah Data Proker";
            this.Load += new System.EventHandler(this.Form_AturProker_Tampil);
            ((System.ComponentModel.ISupportInitialize)(this.TBDana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTITanggal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevComponents.DotNetBar.LabelX LNoID;
        internal DevComponents.DotNetBar.LabelX LId;
        private DevComponents.DotNetBar.StyleManagerAmbient styleManagerAmbient1;
        internal DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.TextBoxX TBNama;
        internal DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.Editors.IntegerInput TBDana;
        internal DevComponents.DotNetBar.LabelX labelX3;
        internal DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.TextBoxX TBKeterangan;
        internal DevComponents.DotNetBar.ButtonX BSimpan;
        internal DevComponents.DotNetBar.ButtonX BTutup;
        internal DevComponents.Editors.DateTimeAdv.DateTimeInput DTITanggal;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
    }
}