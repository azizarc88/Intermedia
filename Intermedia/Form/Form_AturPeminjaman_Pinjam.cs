﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_AturPeminjaman_Pinjam : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();

        public delegate void UpdatePeminjamanHandler(object sender, UpdatePeminjamanEventArgs e);
        public event UpdatePeminjamanHandler UpdatePeminjaman;

        public List<string> IDBarang = new List<string>();

        public Form_AturPeminjaman_Pinjam()
        {
            InitializeComponent();
        }

        private bool CekKelengkapan()
        {
            if (TBIDBarang.Text == "")
            {
                ToastNotification.Show(this, "ID Barang harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBIDBarang.Focus();
                return false;
            }
            else if (TBNim.Text == "")
            {
                ToastNotification.Show(this, "Nim anggota harap diisi", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                TBNim.Focus();
                return false;
            }
            return true;
        }

        private void BSimpan_Click(object sender, EventArgs e)
        {
            if (CekKelengkapan() == false)
            {
                return;
            }

            string nilai, nilaiC;
            nilai = "'" + LId.Text + "', '" + TBIDBarang.Text + "', '" + TBNim.Text + "', '" + MDA.AturTanggal(DateTime.Now) + "', NULL, 'Belum Kembali'";
            nilaiC = "(" + LId.Text + ", " + TBIDBarang.Text + ", " + TBNim.Text + ", " + MDA.AturTanggal(DateTime.Now) + ", NULL, Belum Kembali";
            MDA.TambahData("tabel_peminjaman", nilai);
            MDA.Catat("Tambah Data Peminjaman", nilaiC);

            UpdatePeminjamanEventArgs args = new UpdatePeminjamanEventArgs(LId.Text);
            UpdatePeminjaman(this, args);

            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }

        private void Form_AturPeminjaman_Pinjam_Load_1(object sender, EventArgs e)
        {
            LId.Text = MDA.TentID("PM", "tabel_peminjaman", "id_peminjaman");
            TBNim.Focus();
        }

        private void BPilih_Click(object sender, EventArgs e)
        {
            Form_AturPeminjaman_Pinjam_PilihBarang FrmPilih = new Form_AturPeminjaman_Pinjam_PilihBarang();
            FrmPilih.BarangTerpilih += new Form_AturPeminjaman_Pinjam_PilihBarang.BarangTerpilihHandler(BarangTerpilih);
            FrmPilih.IDBarang = IDBarang;
            FrmPilih.ShowDialog();
        }

        private void BPilihNim_Click(object sender, EventArgs e)
        {
            Form_Atur_Pilih FrmPilih = new Form_Atur_Pilih();
            FrmPilih.ItemTerpilih += new Form_Atur_Pilih.ItemTerpilihHandler(ItemTerpilih);
            FrmPilih.Query = "select nim, nama_anggota, kelas, alamat, j_kel, no_hp from anggota where nim > '1000' order by nama_anggota asc";
            FrmPilih.ShowDialog();
        }

        private void ItemTerpilih(object sender, ItemTerpilihEventArgs e)
        {
            TBNim.Text = e.Daftar[0];
        }

        private void BarangTerpilih(object sender, BarangTerpilihEventArgs e)
        {
            TBIDBarang.Text = e.IDBarang;
        }
    }
}
