﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using DevComponents.DotNetBar;
using Intermedia.Class;

namespace Intermedia.Form
{
    public partial class Form_AturPeminjaman_Pinjam_PilihBarang : MetroForm
    {
        ManageDataAnggota MDA = new ManageDataAnggota();

        public delegate void BarangTerpilihHandler(object sender, BarangTerpilihEventArgs e);
        public event BarangTerpilihHandler BarangTerpilih;

        public List<string> IDBarang = new List<string>();

        public Form_AturPeminjaman_Pinjam_PilihBarang()
        {
            InitializeComponent();
        }

        private void CekKetersediaan()
        {
            foreach (DataGridViewRow baris in DGBarang.Rows)
            {
                foreach (string idBarang in IDBarang)
                {
                    if (baris.Cells[0].Value.ToString().Contains(idBarang))
                    {
                        foreach (DataGridViewCell cell in baris.Cells)
                        {
                            cell.Style.BackColor = Color.LightCoral;
                        }
                    }
                }
            }
        }

        private void Cari(string TeksCari)
        {
            foreach (DataGridViewRow baris in DGBarang.Rows)
            {
                foreach (DataGridViewCell cell in baris.Cells)
                {
                    cell.Style.BackColor = Color.WhiteSmoke;
                }
            }

            if (TeksCari != "")
            {
                foreach (DataGridViewRow baris in DGBarang.Rows)
                {
                    foreach (DataGridViewCell cell in baris.Cells)
                    {
                        if (cell.Value.ToString().ToLower().Contains(TeksCari.ToString().ToLower()))
                        {
                            cell.Style.BackColor = Color.LightBlue;
                            DGBarang.CurrentCell = cell;
                        }
                    }
                }
            }
        }

        private void Form_AturPeminjaman_Pinjam_PilihBarang_Load(object sender, EventArgs e)
        {
            DGBarang.DataSource = MDA.IsiDG("select * from v_pilih_barang");
            CekKetersediaan();
        }

        private void TBCari_TextChanged(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Cari(TBCari.Text);
            timer1.Enabled = false;
        }

        private void BPilih_Click(object sender, EventArgs e)
        {
            int y = DGBarang.CurrentCellAddress.Y;
            string idbarang, nama_barang;

            idbarang = DGBarang[0, y].Value.ToString();
            nama_barang = DGBarang[3, y].Value.ToString();

            if (IDBarang.Contains(LCNimDA.Text))
            {
                ToastNotification.Show(this, nama_barang + " sudah dipinjam oleh orang lain", null, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter);
                return;
            }

            BarangTerpilihEventArgs args = new BarangTerpilihEventArgs(idbarang);
            BarangTerpilih(this, args);

            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }

        private void DGBarang_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int y = DGBarang.CurrentCellAddress.Y;
            LCNimDA.Text = DGBarang[0, y].Value.ToString();
            LContentDA.Text = DGBarang.CurrentCell.Value.ToString();
        }


    }
}
