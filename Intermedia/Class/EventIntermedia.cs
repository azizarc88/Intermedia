﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Intermedia.Class
{
    public class DataGridSegarkanEventArgs : System.EventArgs
    {
        private string mNamaTabel;
        
        public DataGridSegarkanEventArgs(string NamaTabel)
        {
            this.mNamaTabel = NamaTabel;
        }

        public string NamaTabel
        {
            get
            {
                return mNamaTabel;
            }
        }
    }

    public class ItemTerpilihEventArgs : System.EventArgs
    {
        public List<string> mDaftar = new List<string>();

        public ItemTerpilihEventArgs(List<string> Daftar)
        {
            this.mDaftar = Daftar;
        }

        public List<string> Daftar
        {
            get
            {
                return mDaftar;
            }
        }
    }

    public class UpdatePeminjamanEventArgs : System.EventArgs
    {
        private string mNim;

        public UpdatePeminjamanEventArgs(string Nim)
        {
            this.mNim = Nim;
        }

        public string Nim
        {
            get
            {
                return mNim;
            }
        }
    }

    public class BarangTerpilihEventArgs : System.EventArgs
    {
        private string mIDBarang;

        public BarangTerpilihEventArgs(string IDBarang)
        {
            this.mIDBarang = IDBarang;
        }

        public string IDBarang
        {
            get
            {
                return mIDBarang;
            }
        }
    }
}
