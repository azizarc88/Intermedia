﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Intermedia.Class
{
    class Pengguna : Koneksi
    {
        public int LogID { get; set; }
        public string Nim { get; set; }
        public string Nama { get; set; }
        public string Kelas { get; set; }
        public string Alamat { get; set; }
        public string JKel { get; set; }
        public string NoHP { get; set; }
        public string Jabatan { get; set; }
        public string Pass { get; set; }

        public string CekKeanggotaan()
        {
            string _UserValid = "gagal";

            using (SqlCommand cmd = new SqlCommand())
            {
                if (!BukaKoneksi())
                {
                    return "galat";
                }
                SqlDataReader conReader;
                conReader = null;
                cmd.CommandText = "Select * from v_login where nim =@Nim and Password =@Pass";
                cmd.Connection = con;
                cmd.Transaction = trans;
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@Nim", SqlDbType.VarChar).Value = Nim;
                cmd.Parameters.Add("@Pass", SqlDbType.VarChar).Value = Pass;

                try
                {
                    conReader = cmd.ExecuteReader();
                    while (conReader.Read())
                    {
                        Nim = conReader["nim"].ToString();
                        Nama = conReader["nama_anggota"].ToString();
                        Jabatan = conReader["nama_jabatan"].ToString();
                        _UserValid = "sukses";
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Terjadi masalah ketika login :", ex);
                }
                finally
                {
                    conReader.Close();
                    TutupKoneksi();
                }
            }
            return _UserValid;
        }
    }
}
