﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace Intermedia.Class
{
    public abstract class Koneksi
    {
        public SqlConnection con;
        public SqlTransaction trans;
        private bool cobakembali;

        public Koneksi()
        {
            string strconn = "Data Source=localhost;Initial Catalog=dbIntermedia;Integrated Security=True;Connect Timeout=3";
            con = new SqlConnection(strconn);
        }
        
        public Boolean BukaKoneksi()
        {
            MessageBoxEx.UseSystemLocalizedString = true;
            try
            {
                con.Close();
                con.Open();
                trans = con.BeginTransaction();
                if (cobakembali)
                {
                    MessageBoxEx.Show("Koneksi ke database berhasil, silakan login kembali.", "Sudah terhubung", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cobakembali = false;
                }
                return true;
            }
            catch (Exception)
            {
                if (MessageBoxEx.Show("Tidak dapat melakukan koneksi ke database pada server " + con.DataSource.ToString() + ", dengan nama database " + con.Database.ToString()
                    + "\nSilakan cek apakah service SQL Server Agent sudah aktif dan pastikan terdapat database dengan nama tersebut pada SQL Server.", "Tidak dapat terhubung", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
                {
                    cobakembali = true;
                    BukaKoneksi();
                }
                return false;
            }
        }

        public void TutupKoneksi()
        {
            try
            {
                trans.Commit();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        public void TransaksiEror()
        {
            trans.Rollback();
            con.Close();
        }

        protected void ExecuteSQL(string sSQL)
        {
            BukaKoneksi();
            try
            {
                SqlCommand cmdDate = new SqlCommand(" SET DATEFORMAT dmy", con, trans);
                cmdDate.ExecuteNonQuery();
                SqlCommand cmd = new SqlCommand(sSQL, con, trans);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                pesan p = new pesan("Intermedia", eTaskDialogIcon.Stop, "Gagal Mengeksekusi Perintah", "Terjadi kegagalan ketika mengeksekusi perintah dengan query: " + sSQL + ".\n\nPesan Eror: " + ex.Message, eTaskDialogButton.Ok, eTaskDialogBackgroundColor.Red);
            }
            TutupKoneksi();
        }

        protected void OnlyExecuteSQL(string sSQL)
        {
            SqlCommand cmd = new SqlCommand(sSQL, con);
            cmd.ExecuteNonQuery();
        }

        protected DataSet FillDataSet(DataSet dset, string sSQL, string tbl)
        {
            SqlCommand cmd = new SqlCommand(sSQL, con);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);

            try
            {
                adapter.Fill(dset, tbl);
            }
            finally
            {
                con.Close();
            }
            return dset;
        }

        protected DataTable IsiData(string Query)
        {
            BukaKoneksi();
            SqlCommand cmd = new SqlCommand(Query, con, trans);
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            DataSet ds = new System.Data.DataSet();
            try
            {
                da.SelectCommand = cmd;
                da.Fill(ds);
                dt = ds.Tables[0];
                TutupKoneksi();
                return dt;
            }
            catch (Exception ex)
            {
                pesan p = new pesan("Intermedia", eTaskDialogIcon.Stop, "Gagal Mengeksekusi Perintah", "Terjadi kegagalan ketika mengeksekusi perintah dengan query: " + Query + ".\n\nPesan Eror: " + ex.Message, eTaskDialogButton.Ok, eTaskDialogBackgroundColor.Red);
                dt = null;
                TutupKoneksi();
                return dt;
            }
        }

        protected DataSet FillData(string sSQL, string sTable)
        {
            BukaKoneksi();
            SqlCommand cmd = new SqlCommand(sSQL, con, trans);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adapter.Fill(ds, sTable);
            TutupKoneksi();
            return ds;
        }

        protected SqlDataReader setDataReader(string sSQL)
        {
            BukaKoneksi();
            try
            {
                SqlCommand cmd = new SqlCommand(sSQL, con, trans);
                cmd.CommandTimeout = 300;
                SqlDataReader rtnReader;
                rtnReader = cmd.ExecuteReader();
                return rtnReader;
            }
            catch (Exception ex)
            {
                pesan p = new pesan("Intermedia", eTaskDialogIcon.Stop, "Gagal Mengeksekusi Perintah", "Terjadi kegagalan ketika mengeksekusi perintah dengan query: " + sSQL + ".\n\nPesan Eror: " + ex.Message, eTaskDialogButton.Ok, eTaskDialogBackgroundColor.Red);
                return null;
            }
        }
    }
}
