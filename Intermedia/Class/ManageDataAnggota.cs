﻿using System;
using System.Collections.Generic;
using System.Text;
using Intermedia.Class;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace Intermedia.Class
{
    class ManageDataAnggota : ModKoneksi
    {
        public List<string> BacaDataAnggota(string Nim)
        {
            List<string> d = new List<string>();
            BukaKoneksi();
            string sSql = "SELECT * FROM anggota WHERE nim =" + Nim;
            SqlCommand cmd = new SqlCommand(sSql, con, trans);
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                d.Add(Nim);
                d.Add(dr["nama_anggota"].ToString());
                d.Add(dr["kelas"].ToString());
                d.Add(dr["alamat"].ToString());
                d.Add(dr["j_kel"].ToString());
                d.Add(dr["no_hp"].ToString());
                d.Add(dr["password"].ToString());
            }
            dr.Close();
            TutupKoneksi();
            cmd.Dispose();
            return d;
        }

        public List<string> BacaDataStruktur(string Nim)
        {
            List<string> d = new List<string>();
            BukaKoneksi();
            string sSql = "SELECT * FROM struktur_anggota WHERE nim =" + Nim;
            SqlCommand cmd = new SqlCommand(sSql, con, trans);
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                d.Add(dr["id_jabatan"].ToString());
                d.Add(Nim);
                d.Add(dr["nama_jabatan"].ToString());
                d.Add(dr["jabatan_mulai"].ToString());
                d.Add(dr["jabatan_akhir"].ToString());
            }
            dr.Close();
            TutupKoneksi();
            cmd.Dispose();
            return d;
        }

        public List<string> BacaDataPiket(string Nim)
        {
            List<string> d = new List<string>();
            BukaKoneksi();
            string sSql = "SELECT * FROM piket WHERE nim =" + Nim;
            SqlCommand cmd = new SqlCommand(sSql, con, trans);
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                d.Add(dr["id_piket"].ToString());
                d.Add(Nim);
                d.Add(dr["hari"].ToString());
                d.Add(dr["waktu"].ToString());
            }
            dr.Close();
            TutupKoneksi();
            cmd.Dispose();
            return d;
        }

        public List<string> BacaDataRapat(string IDRapat)
        {
            List<string> d = new List<string>();
            BukaKoneksi();
            string sSql = "SELECT * FROM rapat WHERE id_rapat = '" + IDRapat + "'";
            SqlCommand cmd = new SqlCommand(sSql, con, trans);
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                d.Add(IDRapat);
                d.Add(dr["nama_rapat"].ToString());
                d.Add(dr["hasil_rapat"].ToString());
                d.Add(dr["tanggal"].ToString());
                d.Add(dr["waktu"].ToString());
            }
            dr.Close();
            TutupKoneksi();
            cmd.Dispose();
            return d;
        }

        public List<string> BacaDataProker(string IDProker)
        {
            List<string> d = new List<string>();
            BukaKoneksi();
            string sSql = "SELECT * FROM program_kerja WHERE id_proker = '" + IDProker + "'";
            SqlCommand cmd = new SqlCommand(sSql, con, trans);
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                d.Add(IDProker);
                d.Add(dr["nama"].ToString());
                d.Add(dr["tanggal"].ToString());
                d.Add(dr["dana"].ToString());
                d.Add(dr["keterangan"].ToString());
            }
            dr.Close();
            TutupKoneksi();
            cmd.Dispose();
            return d;
        }

        public List<string> BacaDataInvent(string IDBarang)
        {
            List<string> d = new List<string>();
            BukaKoneksi();
            string sSql = "SELECT * FROM inventaris WHERE id_barang = '" + IDBarang + "'";
            SqlCommand cmd = new SqlCommand(sSql, con, trans);
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                d.Add(IDBarang);
                d.Add(dr["nim"].ToString());
                d.Add(dr["nama_barang"].ToString());
            }
            dr.Close();
            TutupKoneksi();
            cmd.Dispose();
            return d;
        }

        public List<string> BacaDataKeuangan(string IDKeuangan)
        {
            List<string> d = new List<string>();
            BukaKoneksi();
            string sSql = "SELECT * FROM keuangan WHERE id_keuangan = '" + IDKeuangan + "'";
            SqlCommand cmd = new SqlCommand(sSql, con, trans);
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                d.Add(IDKeuangan);
                d.Add(dr["nim"].ToString());
                d.Add(dr["tipes"].ToString());
                d.Add(dr["keterangan"].ToString());
                d.Add(dr["nominal"].ToString());
                d.Add(dr["tanggal"].ToString());
            }
            dr.Close();
            TutupKoneksi();
            cmd.Dispose();
            return d;
        }
    }
}
