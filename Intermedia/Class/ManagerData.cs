﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Intermedia.Class;
using System.Windows.Forms;

namespace Intermedia.Class
{
    class ManagerData : Koneksi
    {
        public DataTable IsiDG(string NamaTabel)
        {
            if (NamaTabel == "Anggota")
            {
                if (Program.Jabatan == "Administrator")
                {
                    return IsiData("select nim, nama_anggota, kelas, alamat, j_kel, no_hp from anggota order by nama_anggota asc");
                }
                else
                {
                    return IsiData("select nim, nama_anggota, kelas, alamat, j_kel, no_hp from anggota where nim > '1000' order by nama_anggota asc");
                }
            }
            else if (NamaTabel == "Struktur Anggota")
            {
                if (Program.Jabatan == "Administrator")
                {
                    return IsiData("select nim, nama_anggota, kelas, no_hp, nama_jabatan, jabatan_mulai from v_struktur order by nama_anggota");                    
                }
                else
                {
                    return IsiData("select nim, nama_anggota, kelas, no_hp, nama_jabatan, jabatan_mulai from v_struktur where nim > '1000'");                    
                }
            }
            else if (NamaTabel == "Piket")
            {
                return IsiData("select nim, nama_anggota, kelas, no_hp, hari, waktu from v_piket order by nama_anggota asc");
            }
            else if (NamaTabel == "Rapat")
            {
                return IsiData("select id_rapat, nama_rapat, hasil_rapat, tanggal, waktu from rapat order by tanggal asc");
            }
            else if (NamaTabel == "Proker")
            {
                return IsiData("select id_proker, nama, tanggal, dana, keterangan from program_kerja order by tanggal asc");
            }
            else if (NamaTabel == "Inventaris")
            {
                return IsiData("select * from v_inventaris");
            }
            else if (NamaTabel == "Keuangan")
            {
                return IsiData("select * from v_keuangan order by id_keuangan asc");
            }
            else
            {
                return null;
            }
        }

        public List<string> SetStruktur()
        {
            List<string> d = new List<string>();
            string[] jabatan = { "Ketua", "Wakil Ketua", "Sekretaris", "Bendahara" };

            foreach (string item in jabatan)
            {
                BukaKoneksi();
                string sSql = "SELECT * FROM v_struktur WHERE nama_jabatan='" + item + "'";
                SqlCommand cmd = new SqlCommand(sSql, con, trans);
                SqlDataReader dr;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    d.Add(dr["nama_anggota"].ToString());
                }
                dr.Close();
                TutupKoneksi();
                cmd.Dispose();
            }
            return d;
        }
    }
}
