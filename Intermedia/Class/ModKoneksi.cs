﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using Intermedia.Class;
using System.Data;

namespace Intermedia.Class
{
    class ModKoneksi : Koneksi
    {
        public bool DataAda(string NamaKolom, string NamaTabel, string Kondisi)
        {
            string sSQL = "SELECT " + NamaKolom + " From " + NamaTabel + " WHERE " + Kondisi;
            SqlDataReader dr = setDataReader(sSQL);
            dr.Read();
            bool isExists = ((dr.HasRows == true) ? true : false);
            dr.Close();
            dr.Dispose();
            return isExists;
        }

        public SqlDataReader AmbilDataKolom(string NamaKolom, string NamaTabel, string Kondisi)
        {
            string sSQL = "SELECT " + NamaKolom + " FROM " + NamaTabel + " WHERE " + Kondisi;
            return setDataReader(sSQL);
        }

        public void TambahData(string NamaTabel, string Nilai, string optionalNamaKolom = " ")
        {
            string sSQL = "INSERT INTO " + NamaTabel + optionalNamaKolom + " VALUES(" + Nilai + ")";
            ExecuteSQL(sSQL);
        }

        public void UbahData(string NamaTabel, string Nilai, string Kondisi)
        {
            string sSQL = "UPDATE " + NamaTabel + " SET " + Nilai + " WHERE " + Kondisi;
            ExecuteSQL(sSQL);
        }

        public void HapusData(string NamaTabel, string Kondisi)
        {
            string sSQL = "DELETE FROM " + NamaTabel + " WHERE " + Kondisi;
            ExecuteSQL(sSQL);
        }

        public void EksekusiSP(string NamaSP, string Kondisi)
        {
            string sSQL = "EXEC " + NamaSP + " " + Kondisi;
            ExecuteSQL(sSQL);
        }

        public SqlDataReader AmbilDataSP(string NamaSP, string Kondisi)
        {
            string sSQL = "EXEC " + NamaSP + " " + Kondisi;
            return setDataReader(sSQL);
        }

        public void Simpan()
        {
            TutupKoneksi();
            pesan p = new pesan("Intermedia", DevComponents.DotNetBar.eTaskDialogIcon.Information2, "Tersimpan", "Data telah tersimpan.", DevComponents.DotNetBar.eTaskDialogButton.Ok);
        }

        public string AmbilTeratasDariOrder(string NamaTabel, string NamaKolom, string NamaKolomOrder, string OrderBy)
        {
            string Data;
            BukaKoneksi();
            string sSql = "Select " + NamaKolom + " From " + NamaTabel + " order by " + NamaKolomOrder + " " + OrderBy;
            SqlCommand cmd = new SqlCommand(sSql, con, trans);
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            try
            {
                dr.Read();
                Data = dr[0].ToString();
            }
            catch (Exception)
            {
                Data = null;
            }
            dr.Close();
            TutupKoneksi();
            cmd.Dispose();
            return Data;
        }

        public string TentID(string Awal, string NamaTabel, string Unique)
        {
            string id;
            string a;
            int a2;
            int b;
            BukaKoneksi();
            string sSql = "Select " + Unique + " From " + NamaTabel + " order by " + Unique + " desc";
            SqlCommand cmd = new SqlCommand(sSql, con, trans);
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            dr.Read();
            if (dr.HasRows == false)
            {
                id = Awal + "001";
            }
            else
            {
                id = dr[0].ToString();
                a = id.Replace(Awal, "");
                a2 = int.Parse(a);
                a = a2.ToString();
                b = int.Parse(a) + 1;
                a = b.ToString();
                switch (a.Length)
                {
                    case 1:
                        id = "00" + b + "";
                        break;
                    case 2:
                        id = "0" + b + "";
                        break;
                    case 3:
                        id = "" + b + "";
                        break;
                }
                id = Awal + id;
            }
            dr.Close();
            TutupKoneksi();
            cmd.Dispose();
            return id;
        }

        public string KonversiHari(string hari, string ENorID = "ID")
        {
            string d = null;
            if (ENorID == "ID")
            {
                switch (hari)
                {
                    case "Sunday":
                        d = "Minggu";
                        break;
                    case "Monday":
                        d = "Senin";
                        break;
                    case "Tuesday":
                        d = "Selasa";
                        break;
                    case "Wednesday":
                        d = "Rabu";
                        break;
                    case "Thursday":
                        d = "Kamis";
                        break;
                    case "Friday":
                        d = "Jumat";
                        break;
                    case "Saturday":
                        d = "Sabtu";
                        break;
                }
            }
            else
            {
                switch (hari)
                {
                    case "Minggu":
                        d = "Sunday";
                        break;
                    case "Senin":
                        d = "Monday";
                        break;
                    case "Selasa":
                        d = "Tuesday";
                        break;
                    case "Rabu":
                        d = "Wednesday";
                        break;
                    case "Kamis":
                        d = "Thursday";
                        break;
                    case "Jumat":
                        d = "Friday";
                        break;
                    case "Sabtu":
                        d = "Saturday";
                        break;
                }
            }
            return d;
        }

        public string AturWaktu()
        {
            string t;
            t = AturTanggal(DateTime.Now) + " " + DateTime.Now.ToShortTimeString();
            return t;
        }

        public string AturWaktuUang(DateTime Tanggal)
        {
            string t;
            t = AturTanggal(Tanggal) + " " + DateTime.Now.ToShortTimeString();
            return t;
        }

        public string AturTanggal(DateTime tanggal, string IDorEN = "ID")
        {
            if (IDorEN == "ID")
            {
                return tanggal.Day + "/" + tanggal.Month + "/" + tanggal.Year;
            }
            else
            {
                return tanggal.Year + "/" + tanggal.Month + "/" + tanggal.Day;
            }
        }

        public string AturJabatanMulai()
        {
            int t, b, ta;
            t = DateTime.Now.Day;
            b = DateTime.Now.Month;
            ta = DateTime.Now.Year;
            return t + "/" + b + "/" + ta;
        }

        public string AturJabatanAkhir()
        {
            int t, b, ta;
            t = DateTime.Now.Day;
            b = DateTime.Now.Month;
            ta = DateTime.Now.Year + 1;
            return t + "/" + b + "/" + ta;
        }

        public void Catat(String Tipe, string Keterangan)
        {
            TambahData("riwayat_aplikasi", "'" + TentID("RA", "riwayat_aplikasi", "id_riwayat") + "', '" + Program.Nama + "', '" + Tipe + "', '" + Keterangan + "', '" + AturWaktu() + "'");
        }

        public string AmbilNilai(string NamaKolom, string NamaTabel, string Kondisi)
        {
            string sSQL = "SELECT " + NamaKolom + " FROM " + NamaTabel + " WHERE " + Kondisi;
            SqlDataReader dr = setDataReader(sSQL);
            string sValue = "";
            dr.Read();
            if (dr.HasRows == true)
            {
                sValue = ((dr[0].ToString().Trim() == "Null" || dr[0].ToString().Trim() == "") ? "" : dr[0].ToString().Trim());
            }
            else sValue = "";
            dr.Close();
            dr.Dispose();
            return sValue;
        }

        public List<DateTime> AmbilVarian(string NamaKolom, string NamaTabel)
        {
            List<DateTime> d = new List<DateTime>();
            DateTime a;
            BukaKoneksi();
            string sSql = "SELECT DISTINCT " + NamaKolom + " FROM " + NamaTabel;
            SqlCommand cmd = new SqlCommand(sSql, con, trans);
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                a = DateTime.Parse(dr[NamaKolom].ToString());
                d.Add(a);
            }
            dr.Close();
            TutupKoneksi();
            cmd.Dispose();
            return d;
        }

        public List<string> ID(DataGridViewRowCollection DataGrid, int Kolom = 0)
        {
            List<string> d = new List<string>();
            foreach (DataGridViewRow id in DataGrid)
            {
                d.Add(id.Cells[Kolom].Value.ToString());
            }
            return d;
        }

        public DataTable IsiDG(string Query)
        {
            return IsiData(Query);
        }

        public DataSet IsiData(string sSQL, string sTable)
        {
            return FillData(sSQL, sTable);
        }
    }
}
