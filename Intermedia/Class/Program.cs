﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevComponents.DotNetBar.Metro;
using Intermedia.Form;

namespace Intermedia
{
    static class Program
    {
        public static string Nim = "";
        public static string Nama = "";
        public static string Jabatan = "";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form_Login());
        }
    }
}
