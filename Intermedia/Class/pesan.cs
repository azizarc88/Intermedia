﻿using System;
using System.Collections.Generic;
using System.Text;
using DevComponents.DotNetBar;


namespace Intermedia
{
    public class pesan
    {
        public pesan()
        {

        }

        public pesan(string judul, eTaskDialogIcon icon, string header, string isi, eTaskDialogButton tombol, eTaskDialogBackgroundColor optionalwarna_dialog = eTaskDialogBackgroundColor.Blue, Boolean optionalstatusperintah = false, string optionalteksyes1 = null, string optionalteksyes2 = null, string optionalteksno1 = null, string optionalteksno2 = null, string optionalfooter = null, Boolean optionalfooterimage = false)
        {
            pesanTampil(judul, icon, header, isi, tombol, optionalwarna_dialog);
        }

        public eTaskDialogResult pesanTampil(string judul, eTaskDialogIcon icon, string header, string isi, eTaskDialogButton tombol, eTaskDialogBackgroundColor optionalwarna_dialog = eTaskDialogBackgroundColor.Blue, Boolean optionalstatusperintah = false, string optionalteksyes1 = null, string optionalteksyes2 = null, string optionalteksno1 = null, string optionalteksno2 = null, string optionalfooter = null, Boolean optionalfooterimage = false)
        {
            TaskDialogInfo info = new TaskDialogInfo(judul, icon, header, isi, tombol, optionalwarna_dialog);
            eTaskDialogResult result;
            result = TaskDialog.Show(info);
            TaskDialog.EnableGlass = true;
            return result;
        }
    }
}
